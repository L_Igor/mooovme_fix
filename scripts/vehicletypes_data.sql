SET IDENTITY_INSERT [dbo].[VehicleTypes] ON 
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (1, N'Car', N'')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (2, N'Pickups', N'the pickup is available if you are moving small loads, for a partial move or if you want to move just a few specific items. It is going to be helpful for you if you are on to make any home improvements as well.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (3, N'Cargo Van', N'this type of vehicle is good for small living places like a studio or one-bedroom apartment.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (4, N'10 foot truck', N'this type of moving vehicles is good again for small apartments and you can move a larger studio with this truck. It can be very economic in its gas consumption and has amenities for easy loading. From this point onward you will read about moving trucks with extra space.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (5, N'14 foot truck', N'this truck is appropriate for a bigger apartment for example with two bedrooms and has also a low deck that makes loading and unloading easy. In this case it is good to think about hiring dollies as well – depending on the specifics of your move it is recommended to use a dolly even for your stacks of boxes rather than move them by hand and injure your back. Save your strength and use the amenities that are available anyway. Especially if you are going to move large pieces of furniture the dollies are a must. 12 to 15 feet trucks can carry up to about 3,000 pounds.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (6, N'17 foot truck', N'this kind of moving vehicles is good to be used for a 2-bedroom house and a larger apartment. Another moving truck with extra space – should you need it.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (7, N'20 foot truck', N'it is a vehicle that is appropriate for larger houses with 3 bedrooms.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (8, N'24 foot truck', N'this truck is very large and can take up the belongings of a 4-bedroom house.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (9, N'26 foot truck', N'and in case the 24 foot truck is still not large enough for your home you have one more option. This type of moving vehicles is again appropriate for a 4-bedroom house and larger homes. This one of all the types of moving trucks is the largest possible to rent.')
GO
SET IDENTITY_INSERT [dbo].[VehicleTypes] OFF
GO
