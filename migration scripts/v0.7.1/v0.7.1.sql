--USE [mooovme]
--GO


/******  ******/
PRINT N'Starting updating table [dbo].[PaymentDetails]...';
GO

UPDATE [dbo].[PaymentDetails]
SET [PaymentType] = 0,
    [PaymentDetails] = REPLACE([PaymentDetails], '"fixed"', 0)
WHERE LOWER(TRIM([PaymentType])) = 'fixed'
GO

UPDATE [dbo].[PaymentDetails]
SET [PaymentType] = 1,
    [PaymentDetails] = REPLACE([PaymentDetails], '"hourly"', 1)
WHERE LOWER(TRIM([PaymentType])) = 'hourly'
GO



PRINT N'Altering [dbo].[PaymentDetails]...';
GO

ALTER TABLE [dbo].[PaymentDetails] ALTER COLUMN [PaymentType] INT NOT NULL;
GO





PRINT N'Update complete.';
GO