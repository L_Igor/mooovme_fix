--USE [mooovme]
--GO


/******  ******/
PRINT N'Creating [dbo].[PaymentDetails]...';
GO

CREATE TABLE [dbo].[PaymentDetails] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [PaymentType]    VARCHAR (1000) NOT NULL,
    [PaymentDetails] VARCHAR (8000) NOT NULL,
    CONSTRAINT [PK_PaymentDetails] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

ALTER TABLE [dbo].[PaymentDetails]
    ADD [tmp_OfferId] BIGINT NOT NULL;
GO





/******  ******/
PRINT N'Starting rebuilding table [dbo].[Offers]...';
GO

ALTER TABLE [dbo].[Offers]
    ADD [PaymentDetailsId] BIGINT NULL;
GO

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Offers])
    BEGIN
        INSERT INTO [dbo].[PaymentDetails] ([PaymentType], [PaymentDetails], [tmp_OfferId])
        SELECT   [PaymentType],
                 [PaymentDetails],
                 [Id]
        FROM     [dbo].[Offers]
        ORDER BY [Id] ASC;

        UPDATE [dbo].[Offers]
            SET [PaymentDetailsId] = p.[Id]
            FROM (SELECT [Id], [tmp_OfferId]
                  FROM [dbo].[PaymentDetails]) AS p
            WHERE [dbo].[Offers].[Id] = p.[tmp_OfferId];
    END





/****** finalization ******/
ALTER TABLE [dbo].[PaymentDetails]
    DROP COLUMN [tmp_OfferId];
GO


ALTER TABLE [dbo].[Offers]
    ALTER COLUMN [PaymentDetailsId] BIGINT NOT NULL;
GO

ALTER TABLE [dbo].[Offers]
    DROP COLUMN [PaymentType],
                [PaymentDetails];
GO





PRINT N'Update complete.';
GO