--USE [mooovme]
--GO


/******  ******/
PRINT N'Creating [dbo].[PaymentDetails]...';
GO

CREATE TABLE [dbo].[PaymentDetails] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [PaymentType]    VARCHAR (1000) NOT NULL,
    [PaymentDetails] VARCHAR (8000) NOT NULL,
    CONSTRAINT [PK_PaymentDetails] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

ALTER TABLE [dbo].[PaymentDetails]
    ADD [tmp_OfferId] BIGINT NOT NULL;
GO





/******  ******/
PRINT N'Starting rebuilding table [dbo].[Offers]...';
GO

CREATE TABLE [dbo].[tmp_Offers] (
    [Id]               BIGINT          IDENTITY (1, 1) NOT NULL,
    [Description]      NVARCHAR (4000) NOT NULL,
    [Status]           INT             NOT NULL,
    [RequestId]        BIGINT          NOT NULL,
    [UserId]           BIGINT          NOT NULL,
    [PaymentDetailsId] BIGINT          NOT NULL,
    CONSTRAINT [tmp_PK_Offers] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Offers])
    BEGIN
        INSERT INTO [dbo].[PaymentDetails] ([PaymentType], [PaymentDetails], [tmp_OfferId])
        SELECT   [PaymentType],
                 [PaymentDetails],
                 [Id]
        FROM     [dbo].[Offers]
        ORDER BY [Id] ASC;

        SET IDENTITY_INSERT [dbo].[tmp_Offers] ON;
        INSERT INTO [dbo].[tmp_Offers] ([Id], [Description], [Status], [RequestId], [UserId], [PaymentDetailsId])
        SELECT   o.[Id],
                 o.[Description],
                 o.[Status],
                 o.[RequestId],
                 o.[UserId],
                 p.[Id]
        FROM     [dbo].[Offers] o
            INNER JOIN [dbo].[PaymentDetails] p ON o.[Id] = p.[tmp_OfferId]
        ORDER BY o.[Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_Offers] OFF;
    END

DROP TABLE [dbo].[Offers];

EXECUTE sp_rename N'[dbo].[tmp_Offers]', N'Offers';

EXECUTE sp_rename N'[dbo].[tmp_PK_Offers]', N'PK_Offers', N'OBJECT';





/****** finalization ******/
ALTER TABLE [dbo].[PaymentDetails]
    DROP COLUMN [tmp_OfferId];
GO





PRINT N'Update complete.';
GO