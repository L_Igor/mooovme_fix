/*    ==��������� ���������==

    ������ ��������� ���� ���� : ������ Microsoft Azure SQL Database Edition
    ��� ��������� ���� ���� : ���� ������ SQL Microsoft Azure

    ������ �������� ������� : SQL Server�2017
    ������ �������� ���� ���� : ������ Microsoft SQL Server Standard Edition
    ��� �������� ���� ���� : ������������� SQL Server
*/

USE [master]
GO
/****** Object:  Database [mooovme]    Script Date: 17.11.2017 19:28:43 ******/
CREATE DATABASE [mooovme]
GO
ALTER DATABASE [mooovme] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [mooovme].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [mooovme] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [mooovme] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [mooovme] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [mooovme] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [mooovme] SET ARITHABORT OFF 
GO
ALTER DATABASE [mooovme] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [mooovme] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [mooovme] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [mooovme] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [mooovme] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [mooovme] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [mooovme] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [mooovme] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [mooovme] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [mooovme] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [mooovme] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [mooovme] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [mooovme] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [mooovme] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [mooovme] SET  MULTI_USER 
GO
ALTER DATABASE [mooovme] SET DB_CHAINING OFF 
GO
ALTER DATABASE [mooovme] SET ENCRYPTION ON
GO
ALTER DATABASE [mooovme] SET QUERY_STORE = ON
GO
ALTER DATABASE [mooovme] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 100, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO)
GO
USE [mooovme]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [mooovme]
GO
/****** Object:  Table [dbo].[AffiliateDetails]    Script Date: 17.11.2017 19:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AffiliateDetails](
	[Id] [bigint] NOT NULL,
	[Sex] [int] NOT NULL,
	[MaxDistance] [float] NOT NULL,
	[PhoneNumber] [varchar](64) NULL,
	[DriverLicense] [varchar](64) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_AffiliateDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[Countries]    Script Date: 17.11.2017 19:28:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Countries](
	[Id] [char](2) NOT NULL,
	[Name] [varchar](256) NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[JobFeedbacks]    Script Date: 17.11.2017 19:28:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobFeedbacks](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Rating] [tinyint] NOT NULL,
	[Comment] [varchar](1000) NULL,
	[CreatedAt] [datetimeoffset](7) NOT NULL,
	[UserId] [bigint] NOT NULL,
 CONSTRAINT [PK_JobFeedbacks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[Jobs]    Script Date: 17.11.2017 19:28:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jobs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RequestId] [bigint] NOT NULL,
	[OfferId] [bigint] NOT NULL,
	[Status] [int] NOT NULL,
	[CustomerFeedbackId] [bigint] NULL,
	[AffiliateFeedbackId] [bigint] NULL,
	[CreatedAt] [datetimeoffset](7) NOT NULL,
	[ClosedAt] [datetimeoffset](7) NULL,
 CONSTRAINT [PK_Jobs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 17.11.2017 19:28:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Locations](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FullAddress] [varchar](1000) NOT NULL,
	[ZipCode] [bigint] NOT NULL,
	[Longitude] [decimal](18, 0) NULL,
	[Latitude] [decimal](18, 0) NULL,
	[CountryId] [char](2) NOT NULL,
 CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[Offers]    Script Date: 17.11.2017 19:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Offers](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RequestId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Description] [varchar](8000) NOT NULL,
	[PaymentType] [varchar](1000) NOT NULL,
	[PaymentDetails] [varchar](8000) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_Offers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[RefreshTokens]    Script Date: 17.11.2017 19:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RefreshTokens](
	[Id] [varchar](128) NOT NULL,
	[Expires] [datetimeoffset](7) NOT NULL,
	[CreatedAt] [datetimeoffset](7) NOT NULL,
	[UserId] [bigint] NOT NULL,
 CONSTRAINT [PK_RefreshTokens] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[Requests]    Script Date: 17.11.2017 19:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Requests](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ContactEmail] [nvarchar](256) NULL,
	[ContactNumber] [varchar](64) NULL,
	[Description] [varchar](8000) NOT NULL,
	[MoversCount] [bigint] NULL,
	[Status] [int] NOT NULL,
	[CreatedAt] [datetimeoffset](7) NOT NULL,
	[DepartureLocationId] [bigint] NOT NULL,
	[DestinationLocationId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[VehicleTypeId] [bigint] NOT NULL,
 CONSTRAINT [PK_Requests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[TemporaryTokens]    Script Date: 17.11.2017 19:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemporaryTokens](
	[Id] [varchar](128) NOT NULL,
	[Type] [int] NOT NULL,
	[Expires] [datetimeoffset](7) NOT NULL,
	[UserId] [bigint] NOT NULL,
 CONSTRAINT [PK_TemporaryTokens] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 17.11.2017 19:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDetails](
	[Id] [bigint] NOT NULL,
	[MaxDistance] [float] NOT NULL,
	[PhoneNumber] [varchar](64) NULL,
	[LocationId] [bigint] NULL,
 CONSTRAINT [PK_UserDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[Users]    Script Date: 17.11.2017 19:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](256) NOT NULL,
	[FullName] [nvarchar](1024) NOT NULL,
	[PasswordHash] [nvarchar](max) NOT NULL,
	[Salt] [nvarchar](max) NOT NULL,
	[IsEmailConfirmed] [bit] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
/****** Object:  Table [dbo].[VehicleTypes]    Script Date: 17.11.2017 19:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehicleTypes](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](1024) NOT NULL,
	[Description] [varchar](8000) NOT NULL,
 CONSTRAINT [PK_VehicleTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AD', N'Andorra')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AE', N'United Arab Emirates')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AF', N'Afghanistan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AG', N'Antigua and Barbuda')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AI', N'Anguilla')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AL', N'Albania')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AM', N'Armenia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AO', N'Angola')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AQ', N'Antarctica')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AR', N'Argentina')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AS', N'American Samoa')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AT', N'Austria')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AU', N'Australia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AW', N'Aruba')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AX', N'Aland Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'AZ', N'Azerbaijan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BA', N'Bosnia and Herzegovina')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BB', N'Barbados')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BD', N'Bangladesh')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BE', N'Belgium')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BF', N'Burkina Faso')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BG', N'Bulgaria')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BH', N'Bahrain')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BI', N'Burundi')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BJ', N'Benin')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BL', N'Saint Barthelemy')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BM', N'Bermuda')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BN', N'Brunei Darussalam')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BO', N'Bolivia (Plurinational State of)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BQ', N'Bonaire, Sint Eustatius and Saba')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BR', N'Brazil')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BS', N'Bahamas')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BT', N'Bhutan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BV', N'Bouvet Island')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BW', N'Botswana')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BY', N'Belarus')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'BZ', N'Belize')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CA', N'Canada')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CC', N'Cocos (Keeling) Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CD', N'Congo (Democratic Republic of the)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CF', N'Central African Republic')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CG', N'Congo')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CH', N'Switzerland')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CI', N'Cote d''Ivoire')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CK', N'Cook Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CL', N'Chile')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CM', N'Cameroon')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CN', N'China')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CO', N'Colombia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CR', N'Costa Rica')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CU', N'Cuba')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CV', N'Cabo Verde')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CW', N'Curacao')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CX', N'Christmas Island')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CY', N'Cyprus')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'CZ', N'Czechia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'DE', N'Germany')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'DJ', N'Djibouti')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'DK', N'Denmark')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'DM', N'Dominica')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'DO', N'Dominican Republic')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'DZ', N'Algeria')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'EC', N'Ecuador')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'EE', N'Estonia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'EG', N'Egypt')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'EH', N'Western Sahara')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'ER', N'Eritrea')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'ES', N'Spain')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'ET', N'Ethiopia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'FI', N'Finland')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'FJ', N'Fiji')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'FK', N'Falkland Islands (Malvinas)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'FM', N'Micronesia (Federated States of)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'FO', N'Faroe Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'FR', N'France')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GA', N'Gabon')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GB', N'United Kingdom of Great Britain and Northern Ireland')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GD', N'Grenada')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GE', N'Georgia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GF', N'French Guiana')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GG', N'Guernsey')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GH', N'Ghana')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GI', N'Gibraltar')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GL', N'Greenland')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GM', N'Gambia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GN', N'Guinea')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GP', N'Guadeloupe')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GQ', N'Equatorial Guinea')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GR', N'Greece')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GS', N'South Georgia and the South Sandwich Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GT', N'Guatemala')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GU', N'Guam')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GW', N'Guinea-Bissau')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'GY', N'Guyana')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'HK', N'Hong Kong')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'HM', N'Heard Island and McDonald Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'HN', N'Honduras')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'HR', N'Croatia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'HT', N'Haiti')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'HU', N'Hungary')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'ID', N'Indonesia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'IE', N'Ireland')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'IL', N'Israel')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'IM', N'Isle of Man')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'IN', N'India')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'IO', N'British Indian Ocean Territory')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'IQ', N'Iraq')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'IR', N'Iran (Islamic Republic of)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'IS', N'Iceland')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'IT', N'Italy')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'JE', N'Jersey')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'JM', N'Jamaica')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'JO', N'Jordan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'JP', N'Japan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'KE', N'Kenya')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'KG', N'Kyrgyzstan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'KH', N'Cambodia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'KI', N'Kiribati')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'KM', N'Comoros')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'KN', N'Saint Kitts and Nevis')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'KP', N'Korea (Democratic People''s Republic of)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'KR', N'Korea (Republic of)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'KW', N'Kuwait')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'KY', N'Cayman Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'KZ', N'Kazakhstan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'LA', N'Lao People''s Democratic Republic')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'LB', N'Lebanon')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'LC', N'Saint Lucia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'LI', N'Liechtenstein')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'LK', N'Sri Lanka')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'LR', N'Liberia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'LS', N'Lesotho')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'LT', N'Lithuania')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'LU', N'Luxembourg')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'LV', N'Latvia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'LY', N'Libya')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MA', N'Morocco')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MC', N'Monaco')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MD', N'Moldova (Republic of)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'ME', N'Montenegro')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MF', N'Saint Martin (French part)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MG', N'Madagascar')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MH', N'Marshall Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MK', N'Macedonia (the former Yugoslav Republic of)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'ML', N'Mali')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MM', N'Myanmar')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MN', N'Mongolia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MO', N'Macao')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MP', N'Northern Mariana Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MQ', N'Martinique')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MR', N'Mauritania')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MS', N'Montserrat')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MT', N'Malta')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MU', N'Mauritius')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MV', N'Maldives')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MW', N'Malawi')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MX', N'Mexico')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MY', N'Malaysia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'MZ', N'Mozambique')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NA', N'Namibia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NC', N'New Caledonia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NE', N'Niger')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NF', N'Norfolk Island')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NG', N'Nigeria')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NI', N'Nicaragua')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NL', N'Netherlands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NO', N'Norway')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NP', N'Nepal')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NR', N'Nauru')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NU', N'Niue')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'NZ', N'New Zealand')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'OM', N'Oman')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PA', N'Panama')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PE', N'Peru')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PF', N'French Polynesia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PG', N'Papua New Guinea')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PH', N'Philippines')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PK', N'Pakistan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PL', N'Poland')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PM', N'Saint Pierre and Miquelon')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PN', N'Pitcairn')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PR', N'Puerto Rico')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PS', N'Palestine, State of')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PT', N'Portugal')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PW', N'Palau')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'PY', N'Paraguay')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'QA', N'Qatar')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'RE', N'Reunion')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'RO', N'Romania')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'RS', N'Serbia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'RU', N'Russian Federation')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'RW', N'Rwanda')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SA', N'Saudi Arabia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SB', N'Solomon Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SC', N'Seychelles')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SD', N'Sudan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SE', N'Sweden')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SG', N'Singapore')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SH', N'Saint Helena, Ascension and Tristan da Cunha')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SI', N'Slovenia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SJ', N'Svalbard and Jan Mayen')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SK', N'Slovakia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SL', N'Sierra Leone')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SM', N'San Marino')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SN', N'Senegal')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SO', N'Somalia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SR', N'Suriname')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SS', N'South Sudan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'ST', N'Sao Tome and Principe')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SV', N'El Salvador')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SX', N'Sint Maarten (Dutch part)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SY', N'Syrian Arab Republic')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'SZ', N'Swaziland')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TC', N'Turks and Caicos Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TD', N'Chad')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TF', N'French Southern Territories')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TG', N'Togo')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TH', N'Thailand')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TJ', N'Tajikistan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TK', N'Tokelau')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TL', N'Timor-Leste')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TM', N'Turkmenistan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TN', N'Tunisia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TO', N'Tonga')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TR', N'Turkey')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TT', N'Trinidad and Tobago')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TV', N'Tuvalu')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TW', N'Taiwan, Province of China[a]')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'TZ', N'Tanzania, United Republic of')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'UA', N'Ukraine')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'UG', N'Uganda')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'UM', N'United States Minor Outlying Islands')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'US', N'United States of America')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'UY', N'Uruguay')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'UZ', N'Uzbekistan')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'VA', N'Holy See')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'VC', N'Saint Vincent and the Grenadines')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'VE', N'Venezuela (Bolivarian Republic of)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'VG', N'Virgin Islands (British)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'VI', N'Virgin Islands (U.S.)')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'VN', N'Viet Nam')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'VU', N'Vanuatu')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'WF', N'Wallis and Futuna')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'WS', N'Samoa')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'YE', N'Yemen')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'YT', N'Mayotte')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'ZA', N'South Africa')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'ZM', N'Zambia')
GO
INSERT [dbo].[Countries] ([Id], [Name]) VALUES (N'ZW', N'Zimbabwe')
GO
SET IDENTITY_INSERT [dbo].[Locations] ON 
GO
INSERT [dbo].[Locations] ([Id], [FullAddress], [ZipCode], [Longitude], [Latitude], [CountryId]) VALUES (1, N'Qer', 123457, CAST(-4 AS Decimal(18, 0)), CAST(55 AS Decimal(18, 0)), N'CA')
GO
INSERT [dbo].[Locations] ([Id], [FullAddress], [ZipCode], [Longitude], [Latitude], [CountryId]) VALUES (2, N'Qer', 123457, CAST(-4 AS Decimal(18, 0)), CAST(55 AS Decimal(18, 0)), N'CA')
GO
SET IDENTITY_INSERT [dbo].[Locations] OFF
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'0243d138f27b4fbf9ed6b4c6143c25e8', CAST(N'2018-01-11T11:40:44.3498893+00:00' AS DateTimeOffset), CAST(N'2017-11-12T11:40:44.3498893+00:00' AS DateTimeOffset), 4)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'0d2d5620df614340bad1f79a1268edc4', CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), CAST(N'2017-11-17T10:14:21.7480877+00:00' AS DateTimeOffset), 6)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'1371cee98b4d400cb91edaf929945a8d', CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), CAST(N'2017-11-12T12:37:06.3499393+00:00' AS DateTimeOffset), 3)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'2dde57225d21495b9e9629623dc9ba1d', CAST(N'2018-01-16T10:59:23.6531837+00:00' AS DateTimeOffset), CAST(N'2017-11-17T10:59:23.8094295+00:00' AS DateTimeOffset), 6)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'2e4f217e25244f88ac68ddb074571b9d', CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), CAST(N'2017-11-12T11:53:11.5954724+00:00' AS DateTimeOffset), 3)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'3a0bbdad6851476fa7bb4bc5c4d2f6b8', CAST(N'2018-01-11T08:39:53.5325542+00:00' AS DateTimeOffset), CAST(N'2017-11-12T08:39:53.6848497+00:00' AS DateTimeOffset), 1)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'3cc7ef6e86fa42c2a0eef65fcbf413fe', CAST(N'2018-01-14T16:37:38.0877008+00:00' AS DateTimeOffset), CAST(N'2017-11-15T16:37:38.2238946+00:00' AS DateTimeOffset), 2)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'50ec57513e114c1cac5157598bf74c8a', CAST(N'2018-01-11T12:57:25.9899800+00:00' AS DateTimeOffset), CAST(N'2017-11-12T12:57:25.9899800+00:00' AS DateTimeOffset), 4)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'5c4e01e1663c48c1ab278542ff918c62', CAST(N'2018-01-11T09:17:30.3788159+00:00' AS DateTimeOffset), CAST(N'2017-11-12T09:17:30.4413358+00:00' AS DateTimeOffset), 3)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'61c01e086e414cbbbe37169e6df6544d', CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), CAST(N'2017-11-12T11:57:46.9268432+00:00' AS DateTimeOffset), 4)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'a0fd608a54a7478e9dd1006745953184', CAST(N'2018-01-13T00:13:45.8210921+00:00' AS DateTimeOffset), CAST(N'2017-11-14T00:13:45.9671705+00:00' AS DateTimeOffset), 5)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'a23f500c836543ff8d3377e117cd11fb', CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), CAST(N'2017-11-13T19:34:33.8944979+00:00' AS DateTimeOffset), 5)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'a4f6587a80544f5793642c57ca3741a8', CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), CAST(N'2017-11-12T08:45:58.6999702+00:00' AS DateTimeOffset), 2)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'a616bd2b386c458c92be4a93c1bccb30', CAST(N'2018-01-11T13:10:25.6622722+00:00' AS DateTimeOffset), CAST(N'2017-11-12T13:10:25.6622722+00:00' AS DateTimeOffset), 3)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'b3a3abe06f794a3eaf6d522c45e43577', CAST(N'2018-01-11T11:45:29.0248317+00:00' AS DateTimeOffset), CAST(N'2017-11-12T11:45:29.0248317+00:00' AS DateTimeOffset), 4)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'bc35e137124f4d22ada0592067c8aef6', CAST(N'2018-01-13T08:58:44.3245283+00:00' AS DateTimeOffset), CAST(N'2017-11-14T08:58:44.3732255+00:00' AS DateTimeOffset), 6)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'c22fce57a1894a32b4c6b2fe53c0bb3e', CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), CAST(N'2017-11-12T11:17:38.1779890+00:00' AS DateTimeOffset), 3)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'd7ef9924dffb442ab0a8ae45a0b0ed06', CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), CAST(N'2017-11-11T19:41:18.1662081+00:00' AS DateTimeOffset), 1)
GO
INSERT [dbo].[RefreshTokens] ([Id], [Expires], [CreatedAt], [UserId]) VALUES (N'ef573540db38409094212712c7f4acb1', CAST(N'2018-01-11T11:38:25.5790356+00:00' AS DateTimeOffset), CAST(N'2017-11-12T11:38:25.5790356+00:00' AS DateTimeOffset), 4)
GO
SET IDENTITY_INSERT [dbo].[Requests] ON 
GO
INSERT [dbo].[Requests] ([Id], [ContactEmail], [ContactNumber], [Description], [MoversCount], [Status], [CreatedAt], [DepartureLocationId], [DestinationLocationId], [UserId], [VehicleTypeId]) VALUES (1, N'Alexander.drt@gmail.com', N'1346', N'Test', 1, 0, CAST(N'2017-11-11T19:44:37.8914907+00:00' AS DateTimeOffset), 1, 2, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Requests] OFF
GO
INSERT [dbo].[TemporaryTokens] ([Id], [Type], [Expires], [UserId]) VALUES (N'89trmtdlfnpnblp6loj7y79yudfkrymi', 1, CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), 6)
GO
INSERT [dbo].[TemporaryTokens] ([Id], [Type], [Expires], [UserId]) VALUES (N'l1ewvkbndja08ij2we8fezpxsjwow95x', 1, CAST(N'2017-11-13T09:17:28.3787839+00:00' AS DateTimeOffset), 3)
GO
INSERT [dbo].[TemporaryTokens] ([Id], [Type], [Expires], [UserId]) VALUES (N'lgm4noskbwqpsf2jea7f6qydsx7fs8jf', 1, CAST(N'0001-01-01T00:00:00.0000000+00:00' AS DateTimeOffset), 5)
GO
INSERT [dbo].[TemporaryTokens] ([Id], [Type], [Expires], [UserId]) VALUES (N'mguwsjpa22wnxto9upcptr2c3vozslyl', 1, CAST(N'2017-11-13T11:38:23.4988351+00:00' AS DateTimeOffset), 4)
GO
INSERT [dbo].[TemporaryTokens] ([Id], [Type], [Expires], [UserId]) VALUES (N'tlbk2zm34lf7apytupj4op934cghch3b', 1, CAST(N'2017-11-12T19:41:14.7730763+00:00' AS DateTimeOffset), 1)
GO
INSERT [dbo].[TemporaryTokens] ([Id], [Type], [Expires], [UserId]) VALUES (N'xi9pm4ou2f06ggz9v6amg80lhv4wmyub', 1, CAST(N'2017-11-13T08:45:56.4187217+00:00' AS DateTimeOffset), 2)
GO
INSERT [dbo].[UserDetails] ([Id], [MaxDistance], [PhoneNumber], [LocationId]) VALUES (1, 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Users] ON 
GO
INSERT [dbo].[Users] ([Id], [Email], [FullName], [PasswordHash], [Salt], [IsEmailConfirmed]) VALUES (1, N'alexander.detkov@gmail.com', N'Alexander Detkov', N'5387f24564b241075f48bbdfdddc12640015e92a', N'yY/To?Z4kE)MQcQa;\J0uhy%H(9*mQfV', 0)
GO
INSERT [dbo].[Users] ([Id], [Email], [FullName], [PasswordHash], [Salt], [IsEmailConfirmed]) VALUES (2, N'test@test.com', N'Alex detkov', N'ef0e51f5225536e511303f0328b7b923316ca194', N'bsy8+xe*ZE1ZTMQ\Ehz\uXx1:PfP$AhK', 0)
GO
INSERT [dbo].[Users] ([Id], [Email], [FullName], [PasswordHash], [Salt], [IsEmailConfirmed]) VALUES (3, N'annavareca+5@gmail.com', N'Indira Gandi ', N'f51ba78b4c436b792ba8178fb8ac8a1ab776fd46', N'_oD_6/\p?o28gX,CtbBdC.iMDgb4KwB:', 0)
GO
INSERT [dbo].[Users] ([Id], [Email], [FullName], [PasswordHash], [Salt], [IsEmailConfirmed]) VALUES (4, N'annavareca@gmail.com', N'Leo di Caprio', N'f51ba78b4c436b792ba8178fb8ac8a1ab776fd46', N'CnQ\HBiXa4r!6o_+B\3D/%2gtR/T.0qg', 0)
GO
INSERT [dbo].[Users] ([Id], [Email], [FullName], [PasswordHash], [Salt], [IsEmailConfirmed]) VALUES (5, N'jaypersanchez@gmail.com', N'Jay Sanchez', N'eb1b4aa76126161199d3120a47cceee8186dbc1c', N'bKhFX\b&Krn!z@wKYG7NsQ6NjuTbC7EG', 1)
GO
INSERT [dbo].[Users] ([Id], [Email], [FullName], [PasswordHash], [Salt], [IsEmailConfirmed]) VALUES (6, N'jnjinc3@gmail.com', N'J sanchez', N'eb1b4aa76126161199d3120a47cceee8186dbc1c', N'by#KnFG)SPUPPVj9SfoKlv7bFYQfibEe', 1)
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
SET IDENTITY_INSERT [dbo].[VehicleTypes] ON 
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (1, N'Car', N'')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (2, N'Pickups', N'the pickup is available if you are moving small loads, for a partial move or if you want to move just a few specific items. It is going to be helpful for you if you are on to make any home improvements as well.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (3, N'Cargo Van', N'this type of vehicle is good for small living places like a studio or one-bedroom apartment.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (4, N'10 foot truck', N'this type of moving vehicles is good again for small apartments and you can move a larger studio with this truck. It can be very economic in its gas consumption and has amenities for easy loading. From this point onward you will read about moving trucks with extra space.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (5, N'14 foot truck', N'this truck is appropriate for a bigger apartment for example with two bedrooms and has also a low deck that makes loading and unloading easy. In this case it is good to think about hiring dollies as well � depending on the specifics of your move it is recommended to use a dolly even for your stacks of boxes rather than move them by hand and injure your back. Save your strength and use the amenities that are available anyway. Especially if you are going to move large pieces of furniture the dollies are a must. 12 to 15 feet trucks can carry up to about 3,000 pounds.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (6, N'17 foot truck', N'this kind of moving vehicles is good to be used for a 2-bedroom house and a larger apartment. Another moving truck with extra space � should you need it.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (7, N'20 foot truck', N'it is a vehicle that is appropriate for larger houses with 3 bedrooms.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (8, N'24 foot truck', N'this truck is very large and can take up the belongings of a 4-bedroom house.')
GO
INSERT [dbo].[VehicleTypes] ([Id], [Name], [Description]) VALUES (9, N'26 foot truck', N'and in case the 24 foot truck is still not large enough for your home you have one more option. This type of moving vehicles is again appropriate for a 4-bedroom house and larger homes. This one of all the types of moving trucks is the largest possible to rent.')
GO
SET IDENTITY_INSERT [dbo].[VehicleTypes] OFF
GO
USE [master]
GO
ALTER DATABASE [mooovme] SET  READ_WRITE 
GO
