--USE [mooovme]
--GO


/******  ******/
PRINT N'Altering [dbo].[Locations]...';
GO

ALTER TABLE [dbo].[Locations] ALTER COLUMN [ZipCode] NVARCHAR (100)  NULL;
GO





PRINT N'Update complete.';
GO