--USE [mooovme]
--GO


/******  ******/
PRINT N'Altering [dbo].[Users]...';
GO

ALTER TABLE [dbo].[Users]
    ADD	[DefaultUserRole] INT NOT NULL
        CONSTRAINT tmp_DF_Users_DefaultUserRole DEFAULT 1
GO

ALTER TABLE [dbo].[Users]
    DROP CONSTRAINT tmp_DF_Users_DefaultUserRole
GO





PRINT N'Update complete.';
GO