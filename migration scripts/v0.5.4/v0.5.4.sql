--USE [mooovme]
--GO


/******  ******/
PRINT N'Creating [dbo].[Invoices]...';
GO

CREATE TABLE [dbo].[Invoices] (
    [Id]               BIGINT         IDENTITY (1, 1) NOT NULL,
    [BankInfo]         VARCHAR (8000) NULL,
    [Status]           INT            NOT NULL,
    [JobId]            BIGINT         NOT NULL,
    [PaymentDetailsId] BIGINT         NOT NULL,
    CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO





PRINT N'Update complete.';
GO
