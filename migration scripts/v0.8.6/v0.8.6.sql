--USE [mooovme]
--GO


/******  ******/
PRINT N'Altering [dbo].[Locations]...';
GO

ALTER TABLE [dbo].[Locations] ALTER COLUMN [Latitude] FLOAT (53) NULL;
GO
ALTER TABLE [dbo].[Locations] ALTER COLUMN [Longitude] FLOAT (53) NULL;
GO





PRINT N'Update complete.';
GO