/*    ==Параметры сценариев==

    Версия исходного сервера : SQL Server 2017 (14.0.1000)
    Выпуск исходного ядра СУБД : Выпуск Microsoft SQL Server Express Edition
    Тип исходного ядра СУБД : Изолированный SQL Server

    Версия целевого сервера : SQL Server 2017
    Выпуск целевого ядра СУБД : Выпуск Microsoft SQL Server Standard Edition
    Тип целевого ядра СУБД : Изолированный SQL Server
*/


--USE [mooovme]
--GO


/****** Object:  Table [dbo].[Countries]    Script Date: 20.11.2017 18:25:08 ******/
PRINT N'Starting rebuilding table [dbo].[Countries]...';
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmp_Countries](
	[Id] [char](2) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [tmp_PK_Countries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Countries])
    BEGIN
        --SET IDENTITY_INSERT [dbo].[tmp_Countries] ON;
        INSERT INTO [dbo].[tmp_Countries] ([Id], [Name])
        SELECT   [Id],
                 [Name]
        FROM     [dbo].[Countries]
        ORDER BY [Id] ASC;
        --SET IDENTITY_INSERT [dbo].[tmp_Countries] OFF;
    END

DROP TABLE [dbo].[Countries];

EXECUTE sp_rename N'[dbo].[tmp_Countries]', N'Countries';

EXECUTE sp_rename N'[dbo].[tmp_PK_Countries]', N'PK_Countries', N'OBJECT';





/****** Object:  Table [dbo].[JobFeedbacks]    Script Date: 20.11.2017 18:25:08 ******/
PRINT N'Starting rebuilding table [dbo].[JobFeedbacks]...';
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmp_JobFeedbacks](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Rating] [tinyint] NOT NULL,
	[Comment] [nvarchar](1000) NULL,
	[CreatedAt] [datetimeoffset](7) NOT NULL,
	[UserId] [bigint] NOT NULL,
 CONSTRAINT [tmp_PK_JobFeedbacks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[JobFeedbacks])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_JobFeedbacks] ON;
        INSERT INTO [dbo].[tmp_JobFeedbacks] ([Id], [Rating], [Comment], [CreatedAt], [UserId])
        SELECT   [Id],
                 [Rating],
				 [Comment],
				 [CreatedAt],
				 [UserId]
        FROM     [dbo].[JobFeedbacks]
        ORDER BY [Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_JobFeedbacks] OFF;
    END

DROP TABLE [dbo].[JobFeedbacks];

EXECUTE sp_rename N'[dbo].[tmp_JobFeedbacks]', N'JobFeedbacks';

EXECUTE sp_rename N'[dbo].[tmp_PK_JobFeedbacks]', N'PK_JobFeedbacks', N'OBJECT';





/****** Object:  Table [dbo].[Locations]    Script Date: 20.11.2017 18:25:08 ******/
PRINT N'Starting rebuilding table [dbo].[Locations]...';
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmp_Locations](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FullAddress] [nvarchar](1000) NOT NULL,
	[ZipCode] [bigint] NOT NULL,
	[Longitude] [decimal](18, 0) NULL,
	[Latitude] [decimal](18, 0) NULL,
	[CountryId] [char](2) NOT NULL,
 CONSTRAINT [tmp_PK_Locations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Locations])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_Locations] ON;
        INSERT INTO [dbo].[tmp_Locations] ([Id], [FullAddress], [ZipCode], [Longitude], [Latitude], [CountryId])
        SELECT   [Id],
                 [FullAddress],
				 [ZipCode],
				 [Longitude],
				 [Latitude],
				 [CountryId]
        FROM     [dbo].[Locations]
        ORDER BY [Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_Locations] OFF;
    END

DROP TABLE [dbo].[Locations];

EXECUTE sp_rename N'[dbo].[tmp_Locations]', N'Locations';

EXECUTE sp_rename N'[dbo].[tmp_PK_Locations]', N'PK_Locations', N'OBJECT';





/****** Object:  Table [dbo].[Offers]    Script Date: 20.11.2017 18:25:08 ******/
PRINT N'Starting rebuilding table [dbo].[Offers]...';
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmp_Offers](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RequestId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Description] [nvarchar](4000) NOT NULL,
	[PaymentType] [varchar](1000) NOT NULL,
	[PaymentDetails] [varchar](8000) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [tmp_PK_Offers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Offers])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_Offers] ON;
        INSERT INTO [dbo].[tmp_Offers] ([Id], [RequestId], [UserId], [Description], [PaymentType], [PaymentDetails], [Status])
        SELECT   [Id],
                 [RequestId],
				 [UserId],
				 [Description],
				 [PaymentType],
				 [PaymentDetails],
				 [Status]
        FROM     [dbo].[Offers]
        ORDER BY [Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_Offers] OFF;
    END

DROP TABLE [dbo].[Offers];

EXECUTE sp_rename N'[dbo].[tmp_Offers]', N'Offers';

EXECUTE sp_rename N'[dbo].[tmp_PK_Offers]', N'PK_Offers', N'OBJECT';





/****** Object:  Table [dbo].[Requests]    Script Date: 20.11.2017 18:25:08 ******/
PRINT N'Starting rebuilding table [dbo].[Requests]...';
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmp_Requests](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ContactEmail] [nvarchar](256) NULL,
	[ContactNumber] [varchar](64) NULL,
	[Description] [nvarchar](4000) NOT NULL,
	[MoversCount] [bigint] NULL,
	[Status] [int] NOT NULL,
	[CreatedAt] [datetimeoffset](7) NOT NULL,
	[DepartureLocationId] [bigint] NOT NULL,
	[DestinationLocationId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[VehicleTypeId] [bigint] NOT NULL,
 CONSTRAINT [tmp_PK_Requests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Requests])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_Requests] ON;
        INSERT INTO [dbo].[tmp_Requests] ([Id], [ContactEmail], [ContactNumber], [Description], [MoversCount], [Status], [CreatedAt], [DepartureLocationId], [DestinationLocationId], [UserId], [VehicleTypeId])
        SELECT   [Id],
                 [ContactEmail],
				 [ContactNumber],
				 [Description],
				 [MoversCount],
				 [Status],
				 [CreatedAt],
				 [DepartureLocationId],
				 [DestinationLocationId],
				 [UserId],
				 [VehicleTypeId]
		FROM     [dbo].[Requests]
        ORDER BY [Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_Requests] OFF;
    END

DROP TABLE [dbo].[Requests];

EXECUTE sp_rename N'[dbo].[tmp_Requests]', N'Requests';

EXECUTE sp_rename N'[dbo].[tmp_PK_Requests]', N'PK_Requests', N'OBJECT';





/****** Object:  Table [dbo].[Users]    Script Date: 20.11.2017 18:25:08 ******/
PRINT N'Starting rebuilding table [dbo].[Users]...';
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmp_Users](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](256) NOT NULL,
	[FullName] [nvarchar](1024) NOT NULL,
	[PasswordHash] [varchar](512) NOT NULL,
	[Salt] [varchar](128) NOT NULL,
	[IsEmailConfirmed] [bit] NOT NULL,
 CONSTRAINT [tmp_PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Users])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_Users] ON;
        INSERT INTO [dbo].[tmp_Users] ([Id], [Email], [FullName], [PasswordHash], [Salt], [IsEmailConfirmed])
        SELECT   [Id],
                 [Email],
                 [FullName],
                 [PasswordHash],
                 [Salt],
                 [IsEmailConfirmed]
        FROM     [dbo].[Users]
        ORDER BY [Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_Users] OFF;
    END

DROP TABLE [dbo].[Users];

EXECUTE sp_rename N'[dbo].[tmp_Users]', N'Users';

EXECUTE sp_rename N'[dbo].[tmp_PK_Users]', N'PK_Users', N'OBJECT';





/****** Object:  Table [dbo].[VehicleTypes]    Script Date: 20.11.2017 18:25:08 ******/
PRINT N'Starting rebuilding table [dbo].[VehicleTypes]...';
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmp_VehicleTypes](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](1024) NOT NULL,
	[Description] [nvarchar](4000) NOT NULL,
 CONSTRAINT [tmp_PK_VehicleTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[VehicleTypes])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_VehicleTypes] ON;
        INSERT INTO [dbo].[tmp_VehicleTypes] ([Id], [Name], [Description])
        SELECT   [Id],
                 [Name],
                 [Description]
        FROM     [dbo].[VehicleTypes]
        ORDER BY [Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_VehicleTypes] OFF;
    END

DROP TABLE [dbo].[VehicleTypes];

EXECUTE sp_rename N'[dbo].[tmp_VehicleTypes]', N'VehicleTypes';

EXECUTE sp_rename N'[dbo].[tmp_PK_VehicleTypes]', N'PK_VehicleTypes', N'OBJECT';





PRINT N'Update complete.';
GO
