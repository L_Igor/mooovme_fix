﻿namespace MoooveMe.Api
{
    public class MerchantAuthSettings
    {
        public string ApiEndpoint { get; set; }

        public string ApiLoginId { get; set; }

        public string TransactionKey { get; set; }
    }
}
