using AutoMapper;
using Newtonsoft.Json;
using MoooveMe.Api.Mailing.Models;
using MoooveMe.Api.Models.BindingModel;
using MoooveMe.Api.Models.BindingModel.Invoice;
using MoooveMe.Api.Models.BindingModel.Job;
using MoooveMe.Api.Models.BindingModel.JobFeedback;
using MoooveMe.Api.Models.BindingModel.PaymentDetails;
using MoooveMe.Api.Models.BindingModel.Offer;
using MoooveMe.Api.Models.BindingModel.Request;
using MoooveMe.Api.Models.BindingModel.User;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.View;

namespace MoooveMe.Api
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<User, UserDto>(); //todo: delete

            CreateMap<User, UserInfo>();

            CreateMap<UserDetails, UserDetailsDto>();   //todo: delete

            CreateMap<AffiliateDetails, AffiliateDetailsDto>(); //todo: delete

            CreateMap<Request, RequestDto>();   //todo: delete

            CreateMap<Offer, OfferDto>();   //todo: delete

            CreateMap<PaymentDetails, PaymentDetailsDto>()
                .ForMember(o => o.PaymentType, opt => opt.MapFrom(o => o.Type))
                .ForMember(o => o.PaymentDetails, opt => opt.MapFrom(o => JsonConvert.SerializeObject(o))); //todo: delete

            CreateMap<VehicleType, VehicleTypeDto>();

            CreateMap<Job, JobDto>();

            CreateMap<JobFeedback, JobFeedbackDto>();

            CreateMap<Location, LocationDto>(); //todo: delete


            BindingModelToDtoMappingProfile();
            DtoToViewModelMappingProfile();

            CreateMap<User, RecipientInfo>();
        }

        private void BindingModelToDtoMappingProfile()
        {
            CreateMap<CreateRequest, RequestDto>();
            CreateMap<UpdateRequest, RequestDto>();

            CreateMap<CreateOffer, OfferDto>();

            CreateMap<CreatePaymentDetails, PaymentDetailsDto>()
                .ForMember(o => o.PaymentType, opt => opt.MapFrom(o => o.Type))
                .ForMember(o => o.PaymentDetails, opt => opt.MapFrom(o => JsonConvert.SerializeObject(o)));

            CreateMap<CreateJob, JobDto>();

            CreateMap<CreateJobFeedback, JobFeedbackDto>();

            CreateMap<CreateInvoice, InvoiceDto>();

            CreateMap<RegisterUser, UserDto>();
            CreateMap<UpdateUser, UserDto>();

            CreateMap<UpdateUserDetails, UserDetailsDto>();

            CreateMap<UpdateAffiliateDetails, AffiliateDetailsDto>();

            CreateMap<UpdateLocation, LocationDto>();
        }

        private void DtoToViewModelMappingProfile()
        {
            CreateMap<UserDto, User>();

            CreateMap<UserDto, UserInfo>();

            CreateMap<UserDetailsDto, UserDetails>();

            CreateMap<AffiliateDetailsDto, AffiliateDetails>();

            CreateMap<RequestDto, Request>();

            CreateMap<OfferDto, Offer>();

            CreateMap<PaymentDetailsDto, PaymentDetails>()
                .ConstructUsing(src => ConvertDetails(src.PaymentType, src.PaymentDetails));

            CreateMap<InvoiceDto, Invoice>();

            CreateMap<VehicleTypeDto, VehicleType>();

            CreateMap<JobDto, Job>();

            CreateMap<JobFeedbackDto, JobFeedback>();

            CreateMap<LocationDto, Location>();
        }

        private static PaymentDetails ConvertDetails(PaymentType type, string value)
        {
            switch (type)
            {
                case PaymentType.Fixed:
                    return JsonConvert.DeserializeObject<FixedPaymentDetails>(value);

                case PaymentType.Hourly:
                    return JsonConvert.DeserializeObject<HourlyPaymentDetails>(value);

                default:
                    return new PaymentDetails();
            }
        }
    }
}