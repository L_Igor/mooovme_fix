﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using MoooveMe.Api;
using MoooveMe.Api.Data;
using MoooveMe.Api.Mailing;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Repositories;
using MoooveMe.Api.Repositories.Db;
using MoooveMe.Api.Services;
using MoooveMe.Api.Services.Impl;

public static class ServiceProviderExtensions
{
    public static void RegisterDependencies(this IServiceCollection services)
    {
        services.AddSingleton<IMapper>(s => new Mapper(new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<MapperProfile>();
        })));

        services.AddTransient<IUserRepository, UserRepository>();
        services.AddTransient<IUserDetailsRepository, UserDetailsRepository>();
        services.AddTransient<IAffiliateDetailsRepository, AffiliateDetailsRepository>();
        services.AddTransient<ITemporaryTokenRepository, TemporaryTokenRepository>();
        services.AddTransient<IRefreshTokenRepository, RefreshTokenRepository>();
        services.AddTransient<IVehicleTypeRepository, VehicleTypeRepository>();
        services.AddTransient<IOfferRepository, OfferRepository>();
        services.AddTransient<IRequestRepository, RequestRepository>();
        services.AddTransient<IJobRepository, JobRepository>();
        services.AddTransient<IJobFeedbackRepository, JobFeedbackRepository>();
        services.AddTransient<ILocationRepository, LocationRepository>();
        services.AddTransient<ICountyRepository, CountyRepository>();
        services.AddTransient<IPaymentDetailsRepository, PaymentDetailsRepository>();
        services.AddTransient<IInvoiceRepository, InvoiceRepository>();
        services.AddTransient<IDisposableRepository<RequestDto>, RequestDisposableRepository>();

        services.AddTransient<IMailingService, MailingService>();
        services.AddTransient<IMailTemplateService, MailTemplateService>();
        services.AddTransient<IGeocodingService, GeocodingService>();
        services.AddTransient<IRequestService, RequestService>();
        services.AddTransient<IOfferService, OfferService>();
        services.AddTransient<IJobService, JobService>();
        services.AddTransient<IJobFeedbackService, JobFeedbackService>();
        services.AddTransient<IUserService, UserService>();
        services.AddTransient<IInvoiceService, InvoiceService>();
    }

    public static void RegisterOptions(this IServiceCollection services, IConfigurationRoot Configuration)
    {
        services.AddOptions();
        services.Configure<AppSettings>(Configuration.GetSection("App"));
        services.Configure<AuthSettings>(Configuration.GetSection("Auth"));
        services.Configure<MailingSettings>(Configuration.GetSection("Mailing"));
        services.Configure<DatabaseSettings>(Configuration.GetSection("Database"));
        services.Configure<MerchantAuthSettings>(Configuration.GetSection("MerchantAuth"));
    }
}