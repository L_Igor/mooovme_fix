﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MoooveMe.Api.Helper
{
    public class AuthHelper
    {
        private static readonly Random Random = new Random();

        public static string[] Admins => new [] { "admin@mooovme.com" };


        public static string CreateSalt(int length = 32)
        {
            const string chars = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+/\;:,.?";
            return new string(Enumerable.Range(0, length)
                .Select(i => chars[Random.Next(chars.Length)])
                .ToArray());
        }

        public static string GetPassword(string password, string salt)
        {
            using (var sha1 = SHA1.Create())
            {
                var bytes = sha1.ComputeHash(Encoding.Unicode.GetBytes(password));
                return BitConverter.ToString(bytes).Replace("-", "").ToLower();
            }
        }

        public static string GetPassword(string password, out string salt)
        {
            salt = CreateSalt();
            return GetPassword(password, salt);
        }
    }
}
