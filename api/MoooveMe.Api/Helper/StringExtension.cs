﻿using System;

namespace MoooveMe.Api.Helper
{
    public static class StringExtension
    {
        public static T ToEnum<T>(this string value, T defaultValue = default(T))
            where T : struct
        {
            return Enum.TryParse(value, out T v) ? v : defaultValue;
        }
    }
}
