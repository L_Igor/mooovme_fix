﻿using System;
using System.Linq;
using System.Reflection;
using LinqToDB;
using LinqToDB.Data;
using LinqToDB.Extensions;
using LinqToDB.Mapping;

namespace MoooveMe.Api.Helper
{
    public static class DataConnectionExtensions
    {
        public static void CreateTableIfNotExists<T>(this DataConnection con, bool force = false)
            where T : class
        {
            ITable<T> table = con.GetTable<T>();
            bool isAvailable = true; //TODO: Find something better to determine table is exists
            try
            {
                var item = table.Take(1).FirstOrDefault();
            }
            catch
            {
                isAvailable = false;
            }
            if (isAvailable)
            {
                if (!force) return;
                table.DropTable();
            }

            con.CreateTable<T>();
        }

        public static void Initialize(this DataConnection con, bool force, params Assembly[] assemblies)
        {
            if (assemblies.Length == 0)
            {
                assemblies = new[] { Assembly.GetEntryAssembly(), };
            }
            var createTableMethod = typeof(DataConnectionExtensions)
                .GetMethod(nameof(CreateTableIfNotExists), BindingFlags.Public | BindingFlags.Static);

            foreach (var type in assemblies
                .SelectMany(a => a.DefinedTypes)
                .Select(t => t.AsType())
                .Where(t => t.GetFirstAttribute<TableAttribute>() != null))
            {
                var generic = createTableMethod.MakeGenericMethod(type);
                generic.Invoke(null, new object[] { con, force });
            }
        }
    }
}
