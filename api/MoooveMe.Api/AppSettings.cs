﻿using MoooveMe.Api.Data;
using MoooveMe.Api.Mailing;

namespace MoooveMe.Api
{
    public class AppSettings
    {
        public string ApiUrl { get; set; }

        public string GoogleMapsApiKey { get; set; }
    }
}
