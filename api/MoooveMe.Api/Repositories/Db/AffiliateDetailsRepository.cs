﻿using LinqToDB;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class AffiliateDetailsRepository : Repository<AffiliateDetailsDto>, IAffiliateDetailsRepository
    {
        public AffiliateDetailsRepository()
            : base(db => db.AffiliateDetails)
        {
        }

        public override AffiliateDetailsDto Add(AffiliateDetailsDto item)
        {
            using (var db = new Data.Db())
            {
                db.Insert(item);
            }

            return Get(item.Id);
        }
    }
}
