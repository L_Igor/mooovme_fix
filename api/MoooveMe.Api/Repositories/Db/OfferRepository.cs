﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqToDB;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Repositories.Db
{
    public class OfferRepository : Repository<OfferDto>, IOfferRepository
    {
        public OfferRepository() 
            : base(db => db.Offers)
        {
        }

        public void SetStatus(long id, OfferStatus status)
        {
            using (var db = new Data.Db())
            {
                var offer = db.Offers.FirstOrDefault(o => o.Id == id);
                if (offer == null) return;

                db.Offers.Where(o => o.Id == id)
                    .Set(o => o.Status, status)
                    .Update();
            }
        }

        public OfferDto[] GetUserOffers(long userId)
        {
            using (var db = new Data.Db())
            {
                return db.Offers.Where(o => o.UserId == userId).ToArray();
            }
        }

        public OfferDto[] GetRequestOffers(long requestId)
        {
            using (var db = new Data.Db())
            {
                return db.Offers
                    .Where(o =>
                        o.RequestId == requestId &&
                        o.Status != OfferStatus.Declined)
                    .ToArray();
            }
        }

        public void SetRequestOffersStatus(long requestId, OfferStatus status)
        {
            using (var db = new Data.Db())
            {
                db.Offers.Where(o => o.RequestId == requestId)
                    .Set(o => o.Status, status)
                    .Update();
            }
        }

        public bool IsExistOffer(long requestId, long userId)
        {
            using (var db = new Data.Db())
            {
                return db.Offers
                    .Any(o =>
                        o.RequestId == requestId &&
                        o.UserId == userId);
            }
        }
    }
}
