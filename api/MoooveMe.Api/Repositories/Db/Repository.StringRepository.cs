﻿using System;
using System.Linq;
using LinqToDB;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class StringRepository<T> : IRepository<T, string>
        where T : IdEntity<string>
    {
        protected Func<Data.Db, ITable<T>> TableSelector { get; }

        public StringRepository(Func<Data.Db, ITable<T>> tableSelector)
        {
            TableSelector = tableSelector;
        }

        public virtual T[] GetAll()
        {
            using (var db = new Data.Db())
            {
                return TableSelector(db).ToArray();
            }
        }

        public virtual T Get(string id)
        {
            using (var db = new Data.Db())
            {
                return TableSelector(db).FirstOrDefault(i => i.Id == id);
            }
        }

        public virtual T Add(T item)
        {
            using (var db = new Data.Db())
            {
                db.Insert(item);
            }

            return Get(item.Id);
        }

        public virtual void Delete(string id)
        {
            using (var db = new Data.Db())
            {
                TableSelector(db)
                    .Delete(i => i.Id == id);
            }
        }

        public virtual void Update(string id, T item)
        {
            item.Id = id;
            using (var db = new Data.Db())
            {
                db.Update(item);
            }
        }

        //public virtual IQueryable<T> Query()
        //{
        //    return TableSelector(new Data.Db());
        //}

        public virtual TOut[] Query<TOut>(Func<IQueryable<T>, IQueryable<TOut>> query)
        {
            using (var db = new Data.Db())
            {
                return query(TableSelector(db)).ToArray();
            }
        }

        public virtual T[] Query(Func<ITable<T>, ITable<T>> selector)
        {
            using (var db = new Data.Db())
            {
                return selector(TableSelector(db)).ToArray();
            }
        }

        public virtual TOut[] Query<TOut>(Func<IQueryable<T>, IQueryable<TOut>> query, Func<ITable<T>, ITable<T>> selector)
        {
            using (var db = new Data.Db())
            {
                return query(selector(TableSelector(db))).ToArray();
            }
        }
    }
}