﻿using System.Linq;
using AutoMapper;
using LinqToDB;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Services;

namespace MoooveMe.Api.Repositories.Db
{
    public class UserRepository : Repository<UserDto>, IUserRepository
    {
        private readonly IMapper _mapper;
        private readonly ILocationRepository _locationRepository;
        private readonly IUserDetailsRepository _userDetailsRepository;
        private readonly IAffiliateDetailsRepository _affiliateDetailsRepository;
        private readonly IGeocodingService _geoService;

        public UserRepository(IMapper mapper,
            ILocationRepository locationRepository,
            IUserDetailsRepository userDetailsRepository,
            IAffiliateDetailsRepository affiliateDetailsRepository,
            IGeocodingService geoService)
            : base(db => db.Users)
        {
            _mapper = mapper;
            _locationRepository = locationRepository;
            _userDetailsRepository = userDetailsRepository;
            _affiliateDetailsRepository = affiliateDetailsRepository;
            _geoService = geoService;
        }

        public UserDto GetByEmail(string email)
        {
            using (var db = new Data.Db())
            {
                var res = from u in db.Users
                          where u.Email == email
                          select u;

                return res.FirstOrDefault();
            }
        }

        public SaltedUserCredentials GetCredentials(string email)
        {
            using (var db = new Data.Db())
            {
                var res = from u in db.Users
                          where u.Email == email
                          select new SaltedUserCredentials
                          {
                              Id = u.Id,
                              Email = u.Email,
                              FullName = u.FullName,
                              Password = u.PasswordHash,
                              Salt = u.Salt,
                          };

                return res.FirstOrDefault();
            }
        }

        public void Update(long id, Models.BindingModel.User.UpdateUser user)
        {
            using (var db = new Data.Db())
            {
                db.Users.Where(u => u.Id == id)
                    .Set(u => u.FullName, user.FullName)
                    .Update();
            }
        }

        public void SetEmailConfirmed(long id, bool isConfirmed)
        {
            using (var db = new Data.Db())
            {
                db.Users
                    .Where(u => u.Id == id)
                    .Set(u => u.IsEmailConfirmed, isConfirmed)
                    .Update();
            }
        }

        public void SetPasswordHash(long id, string passwordHash)
        {
            using (var db = new Data.Db())
            {
                db.Users
                    .Where(u => u.Id == id)
                    .Set(u => u.PasswordHash, passwordHash)
                    .Update();
            }
        }
    }
}
