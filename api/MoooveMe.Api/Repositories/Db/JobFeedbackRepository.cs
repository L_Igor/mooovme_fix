﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class JobFeedbackRepository : Repository<JobFeedbackDto>, IJobFeedbackRepository
    {
        public JobFeedbackRepository()
            : base(db => db.JobFeedbacks)
        {
        }
    }
}


