﻿using System.Linq;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class InvoiceRepository : Repository<InvoiceDto>, IInvoiceRepository
    {
        public InvoiceRepository()
            :base(db => db.Invoices)
        {
        }

        public bool IsExistInvoice(long jobId)
        {
            using (var db = new Data.Db())
            {
                return db.Invoices
                    .Any(o => o.JobId == jobId);
            }
        }
    }
}
