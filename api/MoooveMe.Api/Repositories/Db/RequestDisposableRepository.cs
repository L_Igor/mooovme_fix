using System.Linq;
using LinqToDB;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class RequestDisposableRepository : DisposableRepository<RequestDto>
    {
        public RequestDisposableRepository()
            : base(db => db.Requests)
        {
        }

        public override RequestDto[] GetAll()
        {
            return GetAllEntries().ToArray();
        }

        public override RequestDto Get(long id)
        {
            return GetAllEntries().FirstOrDefault(r => r.Id == id);
        }

        public override IQueryable<RequestDto> GetAllEntries()
        {
            return Table.LoadWith(s => s.DepartureLocation)
                .LoadWith(s => s.DestinationLocation)
                .LoadWith(s => s.User)
                .LoadWith(s => s.User.UserDetails)
                .LoadWith(s => s.User.UserDetails.Location)
                .LoadWith(s => s.User.AffiliateDetails)
                .LoadWith(s => s.VehicleType)
                .LoadWith(s => s.Offers);
            // .LoadWith(s => s.Offers[0].PaymentDetails)
            // .LoadWith(s => s.Offers[0].User)
            // .LoadWith(s => s.Offers[0].User.AffiliateDetails);
        }
    }
}