﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class PaymentDetailsRepository : Repository<PaymentDetailsDto>, IPaymentDetailsRepository
    {
        public PaymentDetailsRepository()
            : base(db => db.PaymentDetails)
        {
        }
    }
}
