﻿using System.Linq;
using LinqToDB;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class JobRepository : Repository<JobDto>, IJobRepository
    {
        public JobRepository()
            : base(db => db.Jobs)
        {
        }

        public JobDto GetByRequestId(long requestId)
        {
            using (var db = new Data.Db())
            {
                return
                    db.Jobs
                        .LoadWith(s => s.Request)
                        .LoadWith(s => s.Offer)
                    .Where(p => p.RequestId == requestId)
                    .FirstOrDefault();
            }
        }

        public JobDto[] GetUserJobs(long userId)
        {
            using (var db = new Data.Db())
            {
                var userRequests = db.Jobs
                        .LoadWith(s => s.Request)
                        .LoadWith(s => s.Offer)
                    .Where(p => p.Request.UserId == userId || p.Offer.UserId == userId)
                    .ToArray();

                return userRequests;
            }
        }

        //JobDto[] GetUserJobsAsCustomer(long userId)
        //{
        //    return GetUserJobs(userId, false);
        //}

        //JobDto[] GetUserJobsAsAffiliate(long userId)
        //{
        //    return GetUserJobs(userId, true);
        //}

        //JobDto[] GetUserJobs(long userId, bool? isAffiliate)
        //{
        //    using (var db = new Data.Db())
        //    {
        //        var userJobs = !isAffiliate.HasValue || !isAffiliate.Value
        //            ? db.Jobs.LoadWith(s => s.Request).Where(p => p.Request.UserId == userId)
        //            : new System.Collections.Generic.List<JobDto>().AsQueryable();

        //        var affiliateJobs = !isAffiliate.HasValue || isAffiliate.Value
        //            ? db.Jobs.LoadWith(s => s.Offer).Where(p => p.Offer.UserId == userId)
        //            : new System.Collections.Generic.List<JobDto>().AsQueryable();

        //        var allJobs = !isAffiliate.HasValue
        //            ? Queryable.Union(userJobs, affiliateJobs)
        //            : !isAffiliate.Value
        //            ? userJobs
        //            : affiliateJobs;

        //        return allJobs.ToArray();
        //    }
        //}

        //todo: Vlad "Вот это хотелось бы разобрать"
        //private bool CheckOwn<T>(long? idProposal, string userId, T repo)    // обучение дженерикам на хомячках
        //{         // пример надуманный, но вопрос в том будет ли такая "косорукция" работать правильно?
        //          // в тестовом примере в консольке работало нормально, но я не уверен в этом
        //    return ((IRepository<UserIdEntity>)repo).Get((long)idProposal).UserId == userId; // Long? to long - possible Exception
        //}
        //public bool IsProvider(long id, string userId) // не перепутаны ли названия?
        //{
        //    using (var db = new Data.Db())
        //    {
        //        var job = db.Job.FirstOrDefault(f => f.Id == id);
        //        return CheckOwn(job.OfferId, userId, _repoOffer);
        //    }
        //}
        //public bool IsCustomer(long id, string userId) // не перепутаны ли названия?
        //{
        //    using (var db = new Data.Db())
        //    {
        //        var job = db.Job.FirstOrDefault(f => f.Id == id);
        //        return CheckOwn(job.RequestId, userId, _repoOffer);
        //    }
        //}


        //todo: Vlad "FeedbackRepo наследуется на прямую, логику изменения статуса работы, мне кажется, лучше разместить тут"
        //public JobDto[] GetUserJobs(long userId)
        //{
        //    using (var db = new Data.Db())
        //    {
        //        var offers = _repoOffer.GetUserOffers(userId);
        //        var requests =
        //            _repoRequest.Query(q =>
        //                q.Where(r => r.UserId == userId)
        //                .Select(s => new
        //                {
        //                    Id = s.Id,
        //                }));
        //        var rez = new List<JobDto>();
        //        foreach (var offer in offers)
        //        {
        //            rez.Add(db.Jobs.FirstOrDefault(j => j.Id == offer.Id));
        //        }
        //        foreach (var request in requests)
        //        {
        //            rez.Add(db.Jobs.FirstOrDefault(j => j.Id == request.Id));
        //        }
        //        return rez.ToArray();
        //    }
        //}

        //public string Сomplete(long id, long userId, JobFeedback feedback)
        //{
        //    var job = Get(id);
        //    var feedbackPath = GetUserFeedback(job, userId);
        //    if (Enum.IsDefined(typeof(JobFeedbackStatus), feedback.Status))
        //    {
        //        _repoFeedback.Update(feedbackPath.Id, _mapper.Map<JobFeedbackDto>(feedback));


        //        if (feedback.Status != JobFeedbackStatus.Closed)
        //        {
        //            return Close(job, ((JobStatus)feedback.Status));
        //        }
        //        else
        //        {
        //            var statuses = EnumToDict(JobStatus.Open);
        //            var rez = Close(job,
        //                (JobStatus)statuses["ClosedBy" + feedback.Status]); // по традиции чутка около-варварства
        //            var request = _repoRequest.Get((long)job.RequestId);
        //            request.Status = RequestStatus.Closed;
        //            _repoRequest.Update(request.Id, request);
        //            return rez;
        //        }
        //    }
        //    return feedback.Status.ToString();
        //}

        ////todo: Vlad "этому методы тут не место, но пускай тут пока полежит и вообще странный способ юзать дженерики"
        //public Dictionary<string, int> EnumToDict<T>(T enm)
        //{
        //    var rez = new Dictionary<string, int>();
        //    foreach (var st in Enum.GetValues(typeof(T)))
        //    {
        //        rez[st.ToString()] = (int)st;
        //    }
        //    return rez;
        //}



        //public long IsVendor(JobDto job, long userId) //todo: Vlad "не перепутаны ли названия?"
        //{
        //    //using (var db = new Data.Db())
        //    //{
        //    // var job = db.Job.FirstOrDefault(f => f.Id == id);
        //    // db.Job.FirstOrDefault(f => f.Id == id) <=> Get(id)??


        //    return (_repoOffer.Get((long)job.OfferId).UserId == userId)
        //        ? (long)job.VendorFeedbackId
        //        : Int64.MinValue; // Long? to long - possible Exception
        //    //}
        //}

        //public long IsCustomer(JobDto job, long userId) //todo: Vlad "не перепутаны ли названия?"
        //{
        //    return (_repoRequest.Get((long)job.RequestId).UserId == userId)
        //        ? (long)job.CustomerFeedbackId
        //        : Int64.MinValue; // Long? to long - possible Exception
        //}

        //todo: Vlad "вот тут немного магии с динамиками, возможно лучше юзать структуры но нужны они будут сугубо в одном месте, надо ли?"
        //public dynamic GetUserFeedback(JobDto job, long userId)
        //{
        //    dynamic rez = new ExpandoObject();
        //    rez.Add = (Action<long, string>)((i, t) =>
        //    {
        //        rez.Id = i; // храним ID и строку с типом юзера
        //        rez.Type = t; // возможо стоило создать Enum, но создавать Enum для ExpandoObject.. - хзхз
        //    });
        //    var c = IsCustomer(job, userId);
        //    var v = IsVendor(job, userId);
        //    if (c > v)
        //        rez.Add(c, "Customer");
        //    else if (c < v)
        //        rez.Add(v, "Vendor");
        //    else if (c == v)
        //        if (c > -1)   // не факт, что такая проверка нужна, достаточно фильтра на вход, не плохо было бы сделать её через аспект
        //            rez.Add(c, "InternalServerError");  //?? может ли быть заказчик сам себе исполнителем? попахиваем отмыванием бабла
        //        else
        //            rez.Add(c, "Forbidden");
        //    else
        //        rez.Add(-1, "NotFound");
        //    return rez;
        //}

        //public string Close(JobDto job, JobStatus status)
        //{
        //    if (job == null)
        //        return "NotFound";
        //    if (JobStatus.Completed >= job.Status && job.Status >= JobStatus.Open) //todo: Vlad "могут быть внедрены промежуточные статусы"
        //    {
        //        job.Status = status;
        //        job.ClosedAt = DateTimeOffset.Now;
        //        Update(job.Id, job);
        //        return "OK";
        //    }
        //    return "Conflict";
        //}
    }
}
