using System;
using System.Linq;
using LinqToDB;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class Repository<T> : IRepository<T, long>
        where T : IdEntity<long>
    {
        protected Func<Data.Db, ITable<T>> TableSelector { get; }

        public Repository(Func<Data.Db, ITable<T>> tableSelector)
        {
            TableSelector = tableSelector;
        }

        public virtual T[] GetAll()
        {
            using (var db = new Data.Db())
            {
                return TableSelector(db).ToArray();
            }
        }

        public virtual T Get(long id)
        {
            using (var db = new Data.Db())
            {
                return TableSelector(db).FirstOrDefault(i => i.Id == id);
            }
        }

        public virtual T Add(T item)
        {
            long id;
            using (var db = new Data.Db())
            {
                id = (long)(decimal)db.InsertWithIdentity(item);
            }

            return Get(id);
        }

        public virtual void Delete(long id)
        {
            using (var db = new Data.Db())
            {
                TableSelector(db)
                    .Delete(i => i.Id == id);
            }
        }

        public virtual void Update(long id, T item)
        {
            item.Id = id;
            using (var db = new Data.Db())
            {
                db.Update(item);
            }
        }

        //public virtual IQueryable<T> Query()
        //{
        //    return TableSelector(new Data.Db());
        //}

        public virtual TOut[] Query<TOut>(Func<IQueryable<T>, IQueryable<TOut>> query)
        {
            using (var db = new Data.Db())
            {
                return query(TableSelector(db)).ToArray();
            }
        }

        public virtual T[] Query(Func<ITable<T>, ITable<T>> selector)
        {
            using (var db = new Data.Db())
            {
                return selector(TableSelector(db)).ToArray();
            }
        }

        public virtual TOut[] Query<TOut>(Func<IQueryable<T>, IQueryable<TOut>> query, Func<ITable<T>, ITable<T>> selector)
        {
            using (var db = new Data.Db())
            {
                return query(selector(TableSelector(db))).ToArray();
            }
        }
    }
}