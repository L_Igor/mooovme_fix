﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqToDB;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class RefreshTokenRepository : StringRepository<RefreshTokenDto>, IRefreshTokenRepository
    {
        public RefreshTokenRepository()
            : base(db => db.RefreshTokens)
        {
        }

        //public RefreshTokenDto Get(string id, string clientId = null)
        //{
        //    using (var db = new Data.Db())
        //    {
        //        return db.RefreshTokens.FirstOrDefault(t => t.Id == id && t.ClientId == clientId);
        //    }
        //}
    }
}
