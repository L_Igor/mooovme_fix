﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class LocationRepository : Repository<LocationDto>, ILocationRepository
    {
        public LocationRepository()
            : base(db => db.Locations)
        {
        }
    }
}
