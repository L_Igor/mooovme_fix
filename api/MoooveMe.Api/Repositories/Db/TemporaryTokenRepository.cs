﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqToDB;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Repositories.Db
{
    public class TemporaryTokenRepository : StringRepository<TemporaryTokenDto>, ITemporaryTokenRepository
    {
        public TemporaryTokenRepository()
            : base(db => db.TemporaryTokens)
        {
        }

        public TemporaryTokenDto Get(string id, TokenType type)
        {   
            using (var db = new Data.Db())
            {
                return db.TemporaryTokens.FirstOrDefault(t => t.Id == id && t.Type == type);
            }
        }

        public TemporaryTokenDto GetByUserId(long userId, TokenType type)
        {
            using (var db = new Data.Db())
            {
                return
                    db.TemporaryTokens.FirstOrDefault(t =>
                        t.UserId == userId &&
                        t.Type == type &&
                        t.Expires > DateTimeOffset.UtcNow);
            }
        }
    }
}
