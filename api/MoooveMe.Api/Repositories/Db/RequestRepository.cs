﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class RequestRepository : Repository<RequestDto>, IRequestRepository
    {
        public RequestRepository() 
            : base(db => db.Requests)
        {
        }
    }
}
