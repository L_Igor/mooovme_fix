﻿using LinqToDB;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class UserDetailsRepository : Repository<UserDetailsDto>, IUserDetailsRepository
    {
        public UserDetailsRepository()
            : base(db => db.UserDetails)
        {
        }

        public override UserDetailsDto Add(UserDetailsDto item)
        {
            using (var db = new Data.Db())
            {
                db.Insert(item);
            }

            return Get(item.Id);
        }
    }
}
