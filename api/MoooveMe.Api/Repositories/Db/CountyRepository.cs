﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class CountyRepository : StringRepository<CountryDto>, ICountyRepository
    {
        public CountyRepository()
            : base(db => db.Countries)
        {
        }
    }
}
