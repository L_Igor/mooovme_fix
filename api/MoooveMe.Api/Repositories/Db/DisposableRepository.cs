using System;
using System.Linq;
using LinqToDB;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class DisposableRepository<T> : IDisposableRepository<T>
        where T : IdEntity<long>
    {
        public DisposableRepository(Func<Data.Db, ITable<T>> tableSelector)
        {
            Db = new Data.Db();
            Table = tableSelector(Db);
        }

        protected Data.Db Db { get; }

        protected ITable<T> Table { get; }

        public virtual T Add(T item)
        {
            var id = (long) (decimal) Db.InsertWithIdentity(item);

            return Get(id);
        }

        public virtual void Delete(long id)
        {
            Table.Delete(i => i.Id == id);
        }

        public virtual void Dispose()
        {
            Db?.Dispose();
        }

        public virtual T Get(long id)
        {
            return Table.FirstOrDefault(i => i.Id == id);
        }

        public virtual T[] GetAll()
        {
            return Table.ToArray();
        }

        public virtual IQueryable<T> GetAllEntries()
        {
            return Table;
        }

        public virtual TOut[] Query<TOut>(Func<IQueryable<T>, IQueryable<TOut>> query)
        {
            throw new NotImplementedException();
        }

        public virtual T[] Query(Func<ITable<T>, ITable<T>> selector)
        {
            throw new NotImplementedException();
        }

        public virtual TOut[] Query<TOut>(Func<IQueryable<T>, IQueryable<TOut>> query,
            Func<ITable<T>, ITable<T>> selector)
        {
            throw new NotImplementedException();
        }

        public virtual void Update(long id, T item)
        {
            item.Id = id;
            Db.Update(item);
        }
    }
}