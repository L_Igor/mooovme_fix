﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqToDB;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories.Db
{
    public class VehicleTypeRepository : Repository<VehicleTypeDto>, IVehicleTypeRepository
    {
        public VehicleTypeRepository()
            : base(db => db.VehicleTypes)
        {
        }
    }
}
