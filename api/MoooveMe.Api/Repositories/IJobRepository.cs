﻿using System.Collections.Generic;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.View;

namespace MoooveMe.Api.Repositories
{
    public interface IJobRepository : IRepository<JobDto, long>
    {
        JobDto GetByRequestId(long requestId);

        JobDto[] GetUserJobs(long userId);

        //string Сomplete(long id, long userId, JobFeedback feedback);

        //Dictionary<string, int> EnumToDict<T>(T enm);
    }
}
