﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface IAffiliateDetailsRepository : IRepository<AffiliateDetailsDto, long>
    {
    }
}
