﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface IJobFeedbackRepository : IRepository<JobFeedbackDto, long>
    {
    }
}
