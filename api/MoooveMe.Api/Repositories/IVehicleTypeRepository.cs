﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface IVehicleTypeRepository : IRepository<VehicleTypeDto, long>
    {
    }
}
