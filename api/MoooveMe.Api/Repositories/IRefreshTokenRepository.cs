﻿using System;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface IRefreshTokenRepository : IRepository<RefreshTokenDto, string>
    {
        //RefreshTokenDto Get(string id, string clientId);
    }
}
