﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface IInvoiceRepository : IRepository<InvoiceDto, long>
    {
        bool IsExistInvoice(long jobId);
    }
}
