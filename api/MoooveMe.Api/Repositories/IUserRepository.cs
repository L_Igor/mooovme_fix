﻿using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.View;

namespace MoooveMe.Api.Repositories
{
    public interface IUserRepository : IRepository<UserDto, long>
    {
        UserDto GetByEmail(string email);

        SaltedUserCredentials GetCredentials(string email);

        void Update(long id, Models.BindingModel.User.UpdateUser user);

        void SetEmailConfirmed(long id, bool isConfirmed);

        void SetPasswordHash(long id, string passwordHash);
    }
}
