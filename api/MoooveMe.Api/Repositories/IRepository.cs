﻿using System;
using System.Linq;
using LinqToDB;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface IRepository<T, TId>
        where T : IdEntity<TId>
    {
        T[] GetAll();

        T Get(TId id);

        T Add(T item);

        void Delete(TId id);

        void Update(TId id, T item);

        //IQueryable<T> Query();

        TOut[] Query<TOut>(Func<IQueryable<T>, IQueryable<TOut>> query);

        T[] Query(Func<ITable<T>, ITable<T>> selector);

        TOut[] Query<TOut>(Func<IQueryable<T>, IQueryable<TOut>> query, Func<ITable<T>, ITable<T>> selector);
    }
}
