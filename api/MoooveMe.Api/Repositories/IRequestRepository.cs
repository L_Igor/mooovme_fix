﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface IRequestRepository : IRepository<RequestDto, long>
    {
    }
}
