﻿using System;
using System.Linq;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface IDisposableRepository<T> 
        : IDisposable, IRepository<T, long> where T : IdEntity<long>
    {
        IQueryable<T> GetAllEntries();
    }
}
