﻿using System;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Repositories
{
    public interface ITemporaryTokenRepository : IRepository<TemporaryTokenDto, string>
    {
        TemporaryTokenDto Get(string id, TokenType type);

        TemporaryTokenDto GetByUserId(long userId, TokenType type);
    }
}
