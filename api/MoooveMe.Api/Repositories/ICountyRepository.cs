﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface ICountyRepository : IRepository<CountryDto, string>
    {
    }
}
