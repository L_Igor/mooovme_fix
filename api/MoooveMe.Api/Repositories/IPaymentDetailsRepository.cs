﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface IPaymentDetailsRepository : IRepository<PaymentDetailsDto, long>
    {
    }
}
