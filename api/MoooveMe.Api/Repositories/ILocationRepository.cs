﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface ILocationRepository : IRepository<LocationDto, long>
    {
    }
}
