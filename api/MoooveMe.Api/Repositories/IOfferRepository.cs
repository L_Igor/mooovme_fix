﻿using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Repositories
{
    public interface IOfferRepository : IRepository<OfferDto, long>
    {
        void SetStatus(long id, OfferStatus status);

        OfferDto[] GetUserOffers(long userId);

        OfferDto[] GetRequestOffers(long requestId);

        void SetRequestOffersStatus(long requestId, OfferStatus status);

        bool IsExistOffer(long requestId, long userId);
    }
}
