﻿using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Repositories
{
    public interface IUserDetailsRepository : IRepository<UserDetailsDto, long>
    {
    }
}
