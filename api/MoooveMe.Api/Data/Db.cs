﻿using LinqToDB;
using LinqToDB.Data;
using MoooveMe.Api.Models.Dto;

namespace MoooveMe.Api.Data
{
    public class Db : DataConnection
    {
        public Db()
            : base("SqlServer")
        {
        }

        public ITable<AffiliateDetailsDto> AffiliateDetails => GetTable<AffiliateDetailsDto>();

        public ITable<OfferDto> Offers => GetTable<OfferDto>();

        public ITable<RequestDto> Requests => GetTable<RequestDto>();

        public ITable<JobDto> Jobs => GetTable<JobDto>();

        public ITable<JobFeedbackDto> JobFeedbacks => GetTable<JobFeedbackDto>();

        public ITable<PaymentDetailsDto> PaymentDetails => GetTable<PaymentDetailsDto>();

        public ITable<InvoiceDto> Invoices => GetTable<InvoiceDto>();

        public ITable<TemporaryTokenDto> TemporaryTokens => GetTable<TemporaryTokenDto>();

        public ITable<RefreshTokenDto> RefreshTokens => GetTable<RefreshTokenDto>();

        public ITable<UserDetailsDto> UserDetails => GetTable<UserDetailsDto>();

        public ITable<UserDto> Users => GetTable<UserDto>();

        public ITable<VehicleTypeDto> VehicleTypes => GetTable<VehicleTypeDto>();

        public ITable<LocationDto> Locations => GetTable<LocationDto>();

        public ITable<CountryDto> Countries => GetTable<CountryDto>();
    }
}