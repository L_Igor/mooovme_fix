﻿using System.Collections.Generic;
using LinqToDB.Configuration;

namespace MoooveMe.Api.Data
{
    public class DatabaseSettings : ILinqToDBSettings
    {
        public IEnumerable<IDataProviderSettings> DataProviders
        {
            get { yield break; }
        }

        public string DefaultConfiguration { get; set; }

        public string DefaultDataProvider { get; set; }

        public ConnectionStringSettings[] ConnectionStrings { get; set; }

        IEnumerable<IConnectionStringSettings> ILinqToDBSettings.ConnectionStrings => ConnectionStrings;
    }
}