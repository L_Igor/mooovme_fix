using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace MoooveMe.Api
{
    public class MarkSecuredMethodsOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {

            var filterPipeline = context.ApiDescription.ActionDescriptor.FilterDescriptors;
            // check if authorization is required
            var isAuthorized = filterPipeline
                .Select(filterInfo => filterInfo.Filter)
                .Any(filter => filter is IAuthorizationFilter);

            if (!isAuthorized) return;
            if (operation.Security == null)
                operation.Security = new List<IDictionary<string, IEnumerable<string>>>();

            var auth = new Dictionary<string, IEnumerable<string>>
            {
                {"basic", Enumerable.Empty<string>()}
            };
            operation.Security.Add(auth);
        }
    }
}