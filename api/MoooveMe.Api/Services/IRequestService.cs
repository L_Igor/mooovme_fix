﻿using System;
using MoooveMe.Api.Models.BindingModel.Request;
using MoooveMe.Api.Models.View;

namespace MoooveMe.Api.Services
{
    public interface IRequestService : IDisposable
    {
        Request Get(long id, long userId);

        /// <summary>
        /// Get all available requests to the specified user.
        /// </summary>
        /// <param name="userId">User who get requests</param>
        /// <returns></returns>
        Request[] GetAll(long userId);

        Request[] GetUserRequests(long userId);

        Request Create(CreateRequest request, long userId);
    }
}
