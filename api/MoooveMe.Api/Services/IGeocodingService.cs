﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoooveMe.Api.Services
{
    public interface IGeocodingService
    {
        Coordinates GetCoordinates(string address);
    }
}
