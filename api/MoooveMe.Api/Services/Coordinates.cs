﻿using System;

namespace MoooveMe.Api.Services
{
    public class Coordinates
    {
        public double Longitude { get; set; }

        public double Latitude { get; set; }


        public Coordinates(double lat = 0, double lon = 0)
        {
            Longitude = lon;
            Latitude = lat;
        }

        public Coordinates ConvertToCoordinatesInRad()
        {
            return new Coordinates
            {
                Latitude = ToRad(Latitude),
                Longitude = ToRad(Latitude),
            };
        }


        private static double ToRad(double llDeg)
        {
            return llDeg != 0
                ? llDeg * Math.PI / 180  // signed decimal degrees without NSEW
                : 0;
        }
    }
}
