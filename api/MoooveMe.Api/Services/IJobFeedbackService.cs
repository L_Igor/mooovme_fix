﻿using MoooveMe.Api.Models.BindingModel.JobFeedback;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;

namespace MoooveMe.Api.Services
{
    public interface IJobFeedbackService
    {
        JobFeedback Get(long id);

        JobFeedback Create(CreateJobFeedback jobFeedback, long jobId, long userId);
    }
}
