﻿using MoooveMe.Api.Models.BindingModel.User;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;

namespace MoooveMe.Api.Services
{
    public interface IUserService
    {
        User GetById(long id);

        User GetByEmail(string email);

        UserInfo[] GetAll();

        SaltedUserCredentials GetCredentials(string email);

        AffiliateDetails GetAffiliateDetails(long id);

        User Create(RegisterUser user);

        void UpdateUser(long id, UpdateUser userView);

        void UpdateUserDetails(long id, UpdateUserDetails userDetails);

        void UpdateAffiliateDetails(long id, UpdateAffiliateDetails affiliateDetails);

        float GetUserRating(long id, UserRoles role);

        void AddRating(long id, byte rating, UserRoles role);

        void SetEmailConfirmed(long id, bool isConfirmed);

        bool SetPassword(long userId, string password);

        bool ChangePassword(long userId, string currentPassword, string newPassword);
    }
}
