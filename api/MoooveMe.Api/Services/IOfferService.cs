﻿using MoooveMe.Api.Models.View;

namespace MoooveMe.Api.Services
{
    public interface IOfferService
    {
        Offer Get(long id);

        Offer[] GetUserOffers(long userId);

        Offer[] GetRequestOffers(long requestId);
    }
}
