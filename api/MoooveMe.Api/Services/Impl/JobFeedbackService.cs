﻿using System;
using System.Linq;
using AutoMapper;
using LinqToDB;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.BindingModel.JobFeedback;

namespace MoooveMe.Api.Services.Impl
{
    public class JobFeedbackService : IJobFeedbackService
    {
        private readonly IMapper _mapper;
        private readonly IJobFeedbackRepository _jobFeedbackRepository;
        private readonly IJobRepository _jobRepository;
        private readonly IUserRepository _userRepository;
        private readonly IRequestRepository _requestRepository;
        private readonly IOfferRepository _offerRepository;
        private readonly IUserService _userService;

        public JobFeedbackService(IMapper mapper,
            IJobFeedbackRepository jobFeedbackRepository,
            IJobRepository jobRepository,
            IUserRepository userRepository,
            IRequestRepository requestRepository,
            IOfferRepository offerRepository,
            IUserService userService)
        {
            _mapper = mapper;
            _jobFeedbackRepository = jobFeedbackRepository;
            _jobRepository = jobRepository;
            _userRepository = userRepository;
            _requestRepository = requestRepository;
            _offerRepository = offerRepository;
            _userService = userService;
        }

        public JobFeedback Get(long id)
        {
            return GetAllWith(q => q.Where(p => p.Id == id)).FirstOrDefault();
        }

        public JobFeedback Create(CreateJobFeedback jobFeedback, long jobId, long userId)
        {
            if (jobFeedback == null)
                throw new ArgumentNullException(nameof(jobFeedback));

            var minRating = 1;
            var maxRating = 5;
            if (jobFeedback.Rating < minRating || jobFeedback.Rating > maxRating)
                throw new ArgumentException($"Rating must be between {minRating} and {maxRating}.", nameof(jobFeedback.Rating));

            var jobDto = _jobRepository.Get(jobId);
            if (jobDto == null)
                throw new ArgumentException($"The job '{jobId}' does not exist.", nameof(jobId));

            if (!jobDto.ClosedAt.HasValue)
                throw new InvalidOperationException($"The job '{jobId}' must be closed.");

            var userDto = _userRepository.Get(userId);
            if (userDto == null)
                throw new ArgumentException($"The user '{userId}' does not exist.", nameof(userId));

            var requestDto = _requestRepository.Get(jobDto.RequestId);
            var offerDto = _offerRepository.Get(jobDto.OfferId);

            var customerId = requestDto.UserId;
            var affiliateId = offerDto.UserId;
            if (customerId != userId && affiliateId != userId)
            {
                throw new ArgumentException($"You do not have permission for this action.");
            }

            if (customerId == userId && jobDto.CustomerFeedbackId.HasValue ||
                affiliateId == userId && jobDto.AffiliateFeedbackId.HasValue)
            {
                throw new ArgumentException($"You already left feedback.");
            }

            var jobFeedbackDto = _mapper.Map<JobFeedbackDto>(jobFeedback);
            jobFeedbackDto.CreatedAt = DateTimeOffset.UtcNow;
            jobFeedbackDto.UserId = userId;
            var createdJobFeedbackDto = _jobFeedbackRepository.Add(jobFeedbackDto);

            if (customerId == userId)
            {
                jobDto.CustomerFeedbackId = createdJobFeedbackDto.Id;
                _userService.AddRating(affiliateId, jobFeedback.Rating, UserRoles.Affiliate);
            }
            else if (affiliateId == userId)
            {
                jobDto.AffiliateFeedbackId = createdJobFeedbackDto.Id;
                _userService.AddRating(customerId, jobFeedback.Rating, UserRoles.Customer);
            }
            _jobRepository.Update(jobDto.Id, jobDto);

            return Get(createdJobFeedbackDto.Id);
        }

        private JobFeedback[] GetAllWith(Func<IQueryable<JobFeedbackDto>, IQueryable<JobFeedbackDto>> query = null)
        {
            JobFeedbackDto[] jobFeedbacksDto = null;

            if (query != null)
            {
                jobFeedbacksDto =
                    _jobFeedbackRepository.Query(
                       query,
                       selector => selector
                           .LoadWith(s => s.User)
                           .LoadWith(s => s.User.UserDetails)
                           .LoadWith(s => s.User.UserDetails.Location)
                           //.LoadWith(s => s.User.UserDetails.Location.Country)
                           .LoadWith(s => s.User.AffiliateDetails)
                   );
            }
            else
            {
                jobFeedbacksDto =
                    _jobFeedbackRepository.Query(
                       selector => selector
                           .LoadWith(s => s.User)
                           .LoadWith(s => s.User.UserDetails)
                           .LoadWith(s => s.User.UserDetails.Location)
                           //.LoadWith(s => s.User.UserDetails.Location.Country)
                           .LoadWith(s => s.User.AffiliateDetails)
                   );
            }

            var jobFeedbacks = _mapper.Map<JobFeedback[]>(jobFeedbacksDto);

            return jobFeedbacks;
        }
    }
}
