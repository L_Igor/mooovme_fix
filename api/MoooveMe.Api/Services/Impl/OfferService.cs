﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LinqToDB;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;

namespace MoooveMe.Api.Services.Impl
{
    public class OfferService : IOfferService
    {
        private readonly IMapper _mapper;
        private readonly IOfferRepository _offerRepository;

        public OfferService(IMapper mapper,
            IOfferRepository offerRepository)
        {
            _mapper = mapper;
            _offerRepository = offerRepository;
        }

        public Offer Get(long id)
        {
            return GetAllWith(q => q.Where(p => p.Id == id)).FirstOrDefault();
        }

        public Offer[] GetRequestOffers(long requestId)
        {
            return
                GetAllWith(q =>
                    q.Where(p => p.RequestId == requestId)
                );
        }

        public Offer[] GetUserOffers(long userId)
        {
            return
                GetAllWith(q =>
                    q.Where(p => p.UserId == userId)
                );
        }

        private Offer[] GetAllWith(Func<IQueryable<OfferDto>, IQueryable<OfferDto>> query = null)
        {
            OfferDto[] offersDto = null;

            if (query != null)
            {
                offersDto =
                    _offerRepository.Query(
                       query,
                       selector => selector
                           .LoadWith(s => s.PaymentDetails)

                           .LoadWith(s => s.Request)
                           .LoadWith(s => s.Request.DepartureLocation)
                           //.LoadWith(s => s.Request.DepartureLocation.Country)
                           .LoadWith(s => s.Request.DestinationLocation)
                           //.LoadWith(s => s.Request.DestinationLocation.Country)
                           .LoadWith(s => s.Request.User)
                           .LoadWith(s => s.Request.User.UserDetails)
                           .LoadWith(s => s.Request.User.UserDetails.Location)
                           //.LoadWith(s => s.Request.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Request.User.AffiliateDetails)
                           .LoadWith(s => s.Request.VehicleType)

                           .LoadWith(s => s.User)
                           .LoadWith(s => s.User.UserDetails)
                           .LoadWith(s => s.User.UserDetails.Location)
                           //.LoadWith(s => s.User.UserDetails.Location.Country)
                           .LoadWith(s => s.User.AffiliateDetails)
                   );
            }
            else
            {
                offersDto =
                    _offerRepository.Query(
                       selector => selector
                           .LoadWith(s => s.PaymentDetails)

                           .LoadWith(s => s.Request)
                           .LoadWith(s => s.Request.DepartureLocation)
                           .LoadWith(s => s.Request.DestinationLocation)
                           .LoadWith(s => s.Request.User)
                           .LoadWith(s => s.Request.User.UserDetails)
                           .LoadWith(s => s.Request.User.UserDetails.Location)
                           //.LoadWith(s => s.Request.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Request.User.AffiliateDetails)
                           .LoadWith(s => s.Request.VehicleType)

                           .LoadWith(s => s.User)
                           .LoadWith(s => s.User.UserDetails)
                           .LoadWith(s => s.User.UserDetails.Location)
                           //.LoadWith(s => s.User.UserDetails.Location.Country)
                           .LoadWith(s => s.User.AffiliateDetails)
                   );
            }

            var offers = _mapper.Map<Offer[]>(offersDto);

            return offers;
        }
    }
}
