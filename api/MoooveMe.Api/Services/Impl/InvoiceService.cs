﻿using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using AuthorizeNetCore.Models;
using AutoMapper;
using LinqToDB;
using MoooveMe.Api.Models.BindingModel.Invoice;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;

namespace MoooveMe.Api.Services.Impl
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IMapper _mapper;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IPaymentDetailsRepository _paymentDetailsRepository;
        private readonly IJobService _jobService;
        private readonly MerchantAuthSettings _merchantAuthSettings;

        public InvoiceService(IMapper mapper,
            IInvoiceRepository invoiceRepository,
            IPaymentDetailsRepository paymentDetailsRepository,
            IJobService jobService,
            IOptions<MerchantAuthSettings> merchantAuthSettings)
        {
            _mapper = mapper;
            _invoiceRepository = invoiceRepository;
            _paymentDetailsRepository = paymentDetailsRepository;
            _jobService = jobService;
            _merchantAuthSettings = merchantAuthSettings.Value;
        }

        public Invoice Get(long id)
        {
            return GetAllWith(q => q.Where(p => p.Id == id)).FirstOrDefault();
        }

        public Invoice[] GetUserInvoices(long userId, UserRoles roles)
        {
            if (roles == UserRoles.Customer)
            {
                return
                    GetAllWith(q =>
                        q.Where(p => p.Job.Request.UserId == userId)
                    );
            }
            else if (roles == UserRoles.Affiliate)
            {
                return
                    GetAllWith(q =>
                        q.Where(p => p.Job.Offer.UserId == userId)
                    );
            }

            return
                GetAllWith(q =>
                    q.Where(p => p.Job.Request.UserId == userId || p.Job.Offer.UserId == userId)
                );
        }

        public Invoice Create(CreateInvoice invoice, long jobId, long userId)
        {
            if (invoice == null)
                throw new ArgumentNullException(nameof(invoice));

            if (_invoiceRepository.IsExistInvoice(jobId))
                throw new ArgumentException($"The invoice for this job '{jobId}' already exists.");

            var job = _jobService.Get(jobId);
            if (job == null)
                throw new ArgumentException($"The job '{jobId}' does not exist.");

            if (!job.ClosedAt.HasValue)
                throw new InvalidOperationException($"Job '{jobId}' must be closed.");

            if (job.Offer.User.Id != userId)
                throw new InvalidOperationException($"This action can only be performed by the affiliate.");

            var paymentDetailsDto = _mapper.Map<PaymentDetailsDto>(invoice.PaymentDetails);
            paymentDetailsDto = _paymentDetailsRepository.Add(paymentDetailsDto);

            var invoiceDto = new InvoiceDto
            {
                BankInfo = null,
                Status = InvoiceStatus.Unpaid,
                JobId = job.Id,
                PaymentDetailsId = paymentDetailsDto.Id,
            };
            invoiceDto = _invoiceRepository.Add(invoiceDto);

            return Get(invoiceDto.Id);
        }

        public void Pay(long id, BankCard card, long userId)
        {
            if (!Regex.IsMatch(card.CardNumber, "^[0-9]{13,19}$"))
                throw new ArgumentException($"The {nameof(card.CardNumber)} must consist of digits and have a length of between 13 and 19 characters.");

            if (!Regex.IsMatch(card.ExpirationYear, "^[0-9]{4}$"))
                throw new ArgumentException($"The {nameof(card.ExpirationYear)} must consist of 4 digits.");

            if (!Regex.IsMatch(card.ExpirationMonth, "^[0-9]{2}$"))
                throw new ArgumentException($"The {nameof(card.ExpirationMonth)} must consist of 2 digits.");

            if (!Regex.IsMatch(card.CardCode, "^[0-9]{3,4}$"))
                throw new ArgumentException($"The {nameof(card.CardCode)} must consist of 3 or 4 digits.");

            var invoice = Get(id);

            if (invoice == null)
                throw new ArgumentException($"The invoice '{id}' does not exist.");

            if (invoice.Status == InvoiceStatus.Paid)
                throw new InvalidOperationException($"The invoice '{id}' already been paid.");

            if (invoice.Job.Request.User.Id != userId)
                throw new InvalidOperationException($"This action can only be performed by the customer.");

            var response = MakePayment(invoice, card);

            if (response.IsSuccessful)
            {
                var invoiceDto = _invoiceRepository.Get(id);

                invoiceDto.Status = InvoiceStatus.Paid;
                invoiceDto.BankInfo = Newtonsoft.Json.JsonConvert.SerializeObject(response);
                _invoiceRepository.Update(id, invoiceDto);
            }
            else
            {
                var message = new
                {
                    messages = response.Results,
                    transactionResponse = new
                    {
                        messages = response.TransactionResponse.Messages,
                        errors = response.TransactionResponse.Errors,
                    },
                };

                throw new InvalidOperationException(Newtonsoft.Json.JsonConvert.SerializeObject(message));
            }
        }

        private ChargeCreditCardResponse MakePayment(Invoice invoice, BankCard card)
        {
            var creditCard = new AuthorizeNetCore.CreditCard(_merchantAuthSettings.ApiEndpoint, _merchantAuthSettings.ApiLoginId, _merchantAuthSettings.TransactionKey);

            var chargeCreditCardRequest = new ChargeCreditCardRequest
            {
                CreateTransactionRequest = new CreateTransactionRequest
                {
                    MerchantAuthentication = new MerchantAuthentication
                    {
                        LoginId = _merchantAuthSettings.ApiLoginId,
                        TransactionKey = _merchantAuthSettings.TransactionKey
                    },
                    TransactionRequest = new TransactionRequest
                    {
                        Amount = invoice.PaymentDetails.TotalAmount,
                        Payment = new Payment
                        {
                            CreditCard = new CreditCard
                            {
                                CardNumber = card.CardNumber,
                                ExpirationDate = $"{card.ExpirationYear}-{card.ExpirationMonth}",
                                CardCode = card.CardCode,
                            },
                        },
                    },
                },
            };

            var task = creditCard.ChargeAsync(chargeCreditCardRequest);
            task.Wait();
            var response = task.Result;

            return response;
        }

        private Invoice[] GetAllWith(Func<IQueryable<InvoiceDto>, IQueryable<InvoiceDto>> query = null)
        {
            InvoiceDto[] invoicesDto = null;

            if (query != null)
            {
                invoicesDto =
                    _invoiceRepository.Query(
                       query,
                       selector => selector
                           .LoadWith(s => s.PaymentDetails)

                           .LoadWith(s => s.Job)
                           .LoadWith(s => s.Job.Request)
                           .LoadWith(s => s.Job.Request.DepartureLocation)
                           .LoadWith(s => s.Job.Request.DestinationLocation)
                           .LoadWith(s => s.Job.Request.User)
                           .LoadWith(s => s.Job.Request.User.UserDetails)
                           .LoadWith(s => s.Job.Request.User.UserDetails.Location)
                           //.LoadWith(s => s.Job.Request.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.Request.User.AffiliateDetails)
                           .LoadWith(s => s.Job.Request.VehicleType)
                           .LoadWith(s => s.Job.Offer)
                           .LoadWith(s => s.Job.Offer.Request.DepartureLocation)
                           .LoadWith(s => s.Job.Offer.Request.DestinationLocation)
                           .LoadWith(s => s.Job.Offer.Request.User)
                           .LoadWith(s => s.Job.Offer.Request.User.UserDetails)
                           .LoadWith(s => s.Job.Offer.Request.User.UserDetails.Location)
                           //.LoadWith(s => s.Job.Offer.Request.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.Offer.Request.User.AffiliateDetails)
                           .LoadWith(s => s.Job.Offer.Request.VehicleType)
                           .LoadWith(s => s.Job.Offer.User)
                           .LoadWith(s => s.Job.Offer.User.UserDetails)
                           .LoadWith(s => s.Job.Offer.User.UserDetails.Location)
                           //.LoadWith(s => s.Job.Offer.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.Offer.User.AffiliateDetails)
                           .LoadWith(s => s.Job.Offer.PaymentDetails)
                           .LoadWith(s => s.Job.CustomerFeedback)
                           .LoadWith(s => s.Job.CustomerFeedback.User)
                           .LoadWith(s => s.Job.CustomerFeedback.User.UserDetails)
                           .LoadWith(s => s.Job.CustomerFeedback.User.UserDetails.Location)
                           //.LoadWith(s => s.Job.CustomerFeedback.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.CustomerFeedback.User.AffiliateDetails)
                           .LoadWith(s => s.Job.AffiliateFeedback)
                           .LoadWith(s => s.Job.AffiliateFeedback.User)
                           .LoadWith(s => s.Job.AffiliateFeedback.User.UserDetails)
                           .LoadWith(s => s.Job.AffiliateFeedback.User.UserDetails.Location)
                           //.LoadWith(s => s.Job.AffiliateFeedback.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.AffiliateFeedback.User.AffiliateDetails)

                           .LoadWith(s => s.Job.UserClosedJob)
                           .LoadWith(s => s.Job.UserClosedJob.UserDetails)
                           .LoadWith(s => s.Job.UserClosedJob.UserDetails.Location)
                           //.LoadWith(s => s.Job.UserClosedJob.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.UserClosedJob.AffiliateDetails)
                   );
            }
            else
            {
                invoicesDto =
                    _invoiceRepository.Query(
                       selector => selector
                           .LoadWith(s => s.PaymentDetails)

                           .LoadWith(s => s.Job)
                           .LoadWith(s => s.Job.Request)
                           .LoadWith(s => s.Job.Request.DepartureLocation)
                           .LoadWith(s => s.Job.Request.DestinationLocation)
                           .LoadWith(s => s.Job.Request.User)
                           .LoadWith(s => s.Job.Request.User.UserDetails)
                           .LoadWith(s => s.Job.Request.User.UserDetails.Location)
                           //.LoadWith(s => s.Job.Request.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.Request.User.AffiliateDetails)
                           .LoadWith(s => s.Job.Request.VehicleType)
                           .LoadWith(s => s.Job.Offer)
                           .LoadWith(s => s.Job.Offer.Request.DepartureLocation)
                           .LoadWith(s => s.Job.Offer.Request.DestinationLocation)
                           .LoadWith(s => s.Job.Offer.Request.User)
                           .LoadWith(s => s.Job.Offer.Request.User.UserDetails)
                           .LoadWith(s => s.Job.Offer.Request.User.UserDetails.Location)
                           //.LoadWith(s => s.Job.Offer.Request.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.Offer.Request.User.AffiliateDetails)
                           .LoadWith(s => s.Job.Offer.Request.VehicleType)
                           .LoadWith(s => s.Job.Offer.User)
                           .LoadWith(s => s.Job.Offer.User.UserDetails)
                           .LoadWith(s => s.Job.Offer.User.UserDetails.Location)
                           //.LoadWith(s => s.Job.Offer.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.Offer.User.AffiliateDetails)
                           .LoadWith(s => s.Job.Offer.PaymentDetails)
                           .LoadWith(s => s.Job.CustomerFeedback)
                           .LoadWith(s => s.Job.CustomerFeedback.User)
                           .LoadWith(s => s.Job.CustomerFeedback.User.UserDetails)
                           .LoadWith(s => s.Job.CustomerFeedback.User.UserDetails.Location)
                           //.LoadWith(s => s.Job.CustomerFeedback.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.CustomerFeedback.User.AffiliateDetails)
                           .LoadWith(s => s.Job.AffiliateFeedback)
                           .LoadWith(s => s.Job.AffiliateFeedback.User)
                           .LoadWith(s => s.Job.AffiliateFeedback.User.UserDetails)
                           .LoadWith(s => s.Job.AffiliateFeedback.User.UserDetails.Location)
                           //.LoadWith(s => s.Job.AffiliateFeedback.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.AffiliateFeedback.User.AffiliateDetails)

                           .LoadWith(s => s.Job.UserClosedJob)
                           .LoadWith(s => s.Job.UserClosedJob.UserDetails)
                           .LoadWith(s => s.Job.UserClosedJob.UserDetails.Location)
                           //.LoadWith(s => s.Job.UserClosedJob.UserDetails.Location.Country)
                           .LoadWith(s => s.Job.UserClosedJob.AffiliateDetails)
                   );
            }

            var invoices = _mapper.Map<Invoice[]>(invoicesDto);

            return invoices;
        }
    }
}
