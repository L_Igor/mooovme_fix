﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LinqToDB;
using MoooveMe.Api.Models.BindingModel.User;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;
using MoooveMe.Api.Helper;

namespace MoooveMe.Api.Services.Impl
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        private readonly IUserDetailsRepository _userDetailsRepository;
        private readonly IAffiliateDetailsRepository _affiliateDetailsRepository;
        private readonly ILocationRepository _locationRepository;
        private readonly IGeocodingService _geoService;
        private readonly IHostingEnvironment _env;

        public UserService(IMapper mapper,
            IUserRepository userRepository,
            IUserDetailsRepository userDetailsRepository,
            IAffiliateDetailsRepository affiliateDetailsRepository,
            ILocationRepository locationRepository,
            IGeocodingService geoService,
            IHostingEnvironment env)
        {
            _mapper = mapper;
            _userRepository = userRepository;
            _userDetailsRepository = userDetailsRepository;
            _affiliateDetailsRepository = affiliateDetailsRepository;
            _locationRepository = locationRepository;
            _geoService = geoService;
            _env = env;
        }

        public User GetById(long id)
        {
            var userDto = GetAllWith(q => q.Where(r => r.Id == id)).FirstOrDefault();

            var user = _mapper.Map<User>(userDto);

            return user;
        }

        public User GetByEmail(string email)
        {
            var userDto = GetAllWith(q => q.Where(r => r.Email == email)).FirstOrDefault();

            var user = _mapper.Map<User>(userDto);

            return user;
        }

        public UserInfo[] GetAll()
        {
            var usersDto = GetAllWith();

            var users = _mapper.Map<UserInfo[]>(usersDto);

            return users;
        }

        public SaltedUserCredentials GetCredentials(string email)
        {
            return _userRepository.GetCredentials(email);
        }

        public AffiliateDetails GetAffiliateDetails(long id)
        {
            var affiliateDetailsDto = _affiliateDetailsRepository.Get(id);

            var affiliateDetails = _mapper.Map<AffiliateDetails>(affiliateDetailsDto);

            return affiliateDetails;
        }

        public User Create(RegisterUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (user.DefaultUserRole == UserRoles.All)
                throw new ArgumentException($"This role '{user.DefaultUserRole}' is not supported.", nameof(user.DefaultUserRole));

            if (!user.DefaultUserRole.HasValue)
                user.DefaultUserRole = UserRoles.Customer;

            string salt;
            string passwordHash = AuthHelper.GetPassword(user.Password, out salt);

            var userDto = _mapper.Map<UserDto>(user);
            userDto.Email = userDto.Email.ToLowerInvariant();   //hack:
            userDto.PasswordHash = passwordHash;
            userDto.Salt = salt;
            userDto.IsEmailConfirmed = false;

            userDto = _userRepository.Add(userDto);

            _userDetailsRepository.Add(new UserDetailsDto
            {
                Id = userDto.Id,
            });

            return GetById(userDto.Id);
        }

        public void UpdateUser(long id, UpdateUser userView)
        {
            if (userView == null)
                throw new ArgumentNullException(nameof(userView));

            _userRepository.Update(id, userView);
        }

        public void UpdateUserDetails(long id, UpdateUserDetails userDetailsView)
        {
            if (userDetailsView == null)
                throw new ArgumentNullException(nameof(userDetailsView));

            var exsistingUserDetails = _userDetailsRepository.Get(id);
            var locationId = exsistingUserDetails?.LocationId;

            if (userDetailsView?.Location != null)
            {
                var coords = _geoService.GetCoordinates(userDetailsView?.Location?.FullAddress);

                var locationDto = _mapper.Map<LocationDto>(userDetailsView.Location);
                locationDto.Id = locationId.GetValueOrDefault();
                locationDto.Longitude = coords?.Longitude;
                locationDto.Latitude = coords?.Latitude;

                if (locationId == null)
                {
                    var location = _locationRepository.Add(locationDto);
                    locationId = location.Id;
                }
                else
                {
                    _locationRepository.Update(locationId.Value, locationDto);
                }
            }

            var userDetailsDto = _mapper.Map<UserDetailsDto>(userDetailsView);
            userDetailsDto.Id = id;
            userDetailsDto.LocationId = locationId;

            if (exsistingUserDetails == null)
                _userDetailsRepository.Add(userDetailsDto);
            else
                _userDetailsRepository.Update(id, userDetailsDto);
        }

        public void UpdateAffiliateDetails(long id, UpdateAffiliateDetails affiliateDetailsView)
        {
            if (affiliateDetailsView == null)
                throw new ArgumentNullException(nameof(affiliateDetailsView));

            var affiliateDetailsDto = _mapper.Map<AffiliateDetailsDto>(affiliateDetailsView);
            affiliateDetailsDto.Id = id;

            var existingAffiliateDetails = _affiliateDetailsRepository.Get(id);

            if (existingAffiliateDetails == null)
            {
                affiliateDetailsDto.Status = IsTestEnvironment()
                    ? AffiliateStatus.Activated
                    : AffiliateStatus.Checking;
                _affiliateDetailsRepository.Add(affiliateDetailsDto);
            }
            else
            {
                _affiliateDetailsRepository.Update(id, affiliateDetailsDto);
            }
        }

        public float GetUserRating(long id, UserRoles role)
        {
            var rating = 0.0f;

            if (role == UserRoles.Customer)
            {
                var userDetails = _userDetailsRepository.Get(id);
                if (userDetails == null)
                    throw new ArgumentException($"The user '{id}' does not exist.", nameof(id));

                rating = userDetails.Rating;
            }
            else if (role == UserRoles.Affiliate)
            {
                var affiliateDetails = _affiliateDetailsRepository.Get(id);
                if (affiliateDetails == null)
                    throw new ArgumentException($"The user '{id}' does not exist.", nameof(id));

                rating = affiliateDetails.Rating;
            }
            else
            {
                throw new ArgumentException($"This role '{role}' is not supported.", nameof(role));
            }

            return rating;
        }

        public void AddRating(long id, byte rating, UserRoles role)
        {
            var minRating = 1;
            var maxRating = 5;
            if (rating < minRating || rating > maxRating)
                throw new ArgumentException($"Rating must be between {minRating} and {maxRating}.", nameof(rating));

            if (role == UserRoles.Customer)
            {
                var userDetails = _userDetailsRepository.Get(id);
                if (userDetails == null)
                    throw new ArgumentException($"The user '{id}' does not exist.", nameof(id));

                userDetails.SumRating += rating;
                ++userDetails.CountRating;
                userDetails.Rating = (float)userDetails.SumRating / userDetails.CountRating;

                _userDetailsRepository.Update(id, userDetails);
            }
            else if (role == UserRoles.Affiliate)
            {
                var affiliateDetails = _affiliateDetailsRepository.Get(id);
                if (affiliateDetails == null)
                    throw new ArgumentException($"The user '{id}' does not exist.", nameof(id));

                affiliateDetails.SumRating += rating;
                ++affiliateDetails.CountRating;
                affiliateDetails.Rating = (float)affiliateDetails.SumRating / affiliateDetails.CountRating;

                _affiliateDetailsRepository.Update(id, affiliateDetails);
            }
            else
            {
                throw new ArgumentException($"This role '{role}' is not supported.", nameof(role));
            }
        }

        public void SetEmailConfirmed(long id, bool isConfirmed)
        {
            _userRepository.SetEmailConfirmed(id, isConfirmed);
        }

        public bool SetPassword(long userId, string password)
        {
            var user = _userRepository.Get(userId);
            if (user == null)
                return false;

            var passwordHash = AuthHelper.GetPassword(password, user.Salt);

            _userRepository.SetPasswordHash(userId, passwordHash);

            return true;
        }

        public bool ChangePassword(long userId, string currentPassword, string newPassword)
        {
            var user = _userRepository.Get(userId);
            if (user == null)
                return false;

            if (user.PasswordHash != AuthHelper.GetPassword(currentPassword, user.Salt))
                return false;

            var passwordHash = AuthHelper.GetPassword(newPassword, user.Salt);

            _userRepository.SetPasswordHash(userId, passwordHash);

            return true;
        }

        private UserDto[] GetAllWith()
        {
            var users =
                    _userRepository.Query(
                       selector => selector
                           .LoadWith(s => s.UserDetails)
                           .LoadWith(s => s.UserDetails.Location)
                           //.LoadWith(s => s.UserDetails.Location.Country)
                           .LoadWith(s => s.AffiliateDetails)
                   );

            return users;
        }

        private TOut[] GetAllWith<TOut>(Func<IQueryable<UserDto>, IQueryable<TOut>> query)
        {
            var users =
                    _userRepository.Query(
                       query,
                       selector => selector
                           .LoadWith(s => s.UserDetails)
                           .LoadWith(s => s.UserDetails.Location)
                           //.LoadWith(s => s.UserDetails.Location.Country)
                           .LoadWith(s => s.AffiliateDetails)
                   );

            return users;
        }

        private bool IsTestEnvironment()
        {
            return _env.IsDevelopment() || _env.IsEnvironment("Local");
        }
    }
}
