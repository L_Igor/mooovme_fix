﻿using System;
using System.Linq;
using AutoMapper;
using LinqToDB;
using MoooveMe.Api.Models.BindingModel.Job;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Services.Impl
{
    public class JobService : IJobService
    {
        private readonly IMapper _mapper;
        private readonly IJobRepository _jobRepository;
        private readonly IRequestRepository _requestRepository;
        private readonly IOfferRepository _offerRepository;

        public JobService(IMapper mapper,
            IJobRepository jobRepository,
            IRequestRepository requestRepository,
            IOfferRepository offerRepository)
        {
            _mapper = mapper;
            _jobRepository = jobRepository;
            _requestRepository = requestRepository;
            _offerRepository = offerRepository;
        }

        public Job Get(long id)
        {
            return GetAllWith(q => q.Where(p => p.Id == id)).FirstOrDefault();
        }

        public Job[] GetAll()
        {
            return GetAllWith();
        }

        public Job[] GetUserJobs(long userId, UserRoles roles)
        {
            if (roles == UserRoles.Customer)
            {
                return
                    GetAllWith(q =>
                        q.Where(p => p.Request.UserId == userId)
                    );
            }
            else if (roles == UserRoles.Affiliate)
            {
                return
                    GetAllWith(q =>
                        q.Where(p => p.Offer.UserId == userId)
                    );
            }

            return
                GetAllWith(q =>
                    q.Where(p => p.Request.UserId == userId || p.Offer.UserId == userId)
                );
        }

        public Job Create(CreateJob job, long userId)
        {
            if (job == null)
                throw new ArgumentNullException(nameof(job));

            var offer = _offerRepository.Get(job.OfferId);
            if (offer == null)
                throw new ArgumentException($"The offer '{job.OfferId}' does not exist.");

            if (offer.Status != OfferStatus.NotDecided)
                throw new ArgumentException($"The offer '{offer.Id}' already has a status.");

            var request = _requestRepository.Get(offer.RequestId);
            if (request == null)
                throw new ArgumentException($"The request '{offer.RequestId}' does not exist.");

            if (request.UserId != userId)
                throw new ArgumentException($"You do not have permission for this action.");

            var existingJob = _jobRepository.GetByRequestId(request.Id);
            if (existingJob != null)
                throw new ArgumentException($"The job already exist.");

            //todo: add distance check

            if (offer.RequestId != request.Id)
                throw new ArgumentException($"Offer '{offer.Id}' does not belong to the specified request '{request.Id}'.");

            request.Status = RequestStatus.InProgress;
            _requestRepository.Update(request.Id, request);

            _offerRepository.SetRequestOffersStatus(request.Id, OfferStatus.Declined);

            offer.Status = OfferStatus.Accepted;
            _offerRepository.Update(offer.Id, offer);

            var jobDto = new JobDto
            {
                RequestId = request.Id,
                OfferId = offer.Id,
                Status = JobStatus.Open,
                CreatedAt = DateTimeOffset.UtcNow,
            };
            var createdJobDto = _jobRepository.Add(jobDto);

            return Get(createdJobDto.Id);
        }

        public void Close(long id, JobStatus status, long userId)
        {
            if (status == JobStatus.Open)
                throw new ArgumentException($"Invalid status '{status}' for this action.");

            var job = _jobRepository.Get(id);
            if (job == null)
                throw new ArgumentException($"The job '{id}' is not found.");

            if (job.ClosedAt.HasValue)
                throw new InvalidOperationException($"The job '{id}' is already closed.");

            var request = _requestRepository.Get(job.RequestId);
            var offer = _offerRepository.Get(job.OfferId);
            if (request.UserId != userId && offer.UserId != userId)
                throw new ArgumentException($"You do not have permission for this action.");

            job.Status = status;
            job.UserIdClosedJob = userId;
            job.ClosedAt = DateTimeOffset.UtcNow;
            _jobRepository.Update(id, job);
        }

        private Job[] GetAllWith(Func<IQueryable<JobDto>, IQueryable<JobDto>> query = null)
        {
            JobDto[] jobsDto = null;

            if (query != null)
            {
                jobsDto =
                    _jobRepository.Query(
                       query,
                       selector => selector
                           .LoadWith(s => s.Request)
                           .LoadWith(s => s.Request.DepartureLocation)
                           .LoadWith(s => s.Request.DestinationLocation)
                           .LoadWith(s => s.Request.User)
                           .LoadWith(s => s.Request.User.UserDetails)
                           .LoadWith(s => s.Request.User.UserDetails.Location)
                           //.LoadWith(s => s.Request.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Request.User.AffiliateDetails)
                           .LoadWith(s => s.Request.VehicleType)

                           .LoadWith(s => s.Offer)
                           .LoadWith(s => s.Offer.PaymentDetails)
                           .LoadWith(s => s.Offer.Request.DepartureLocation)
                           .LoadWith(s => s.Offer.Request.DestinationLocation)
                           .LoadWith(s => s.Offer.Request.User)
                           .LoadWith(s => s.Offer.Request.User.UserDetails)
                           .LoadWith(s => s.Offer.Request.User.UserDetails.Location)
                           //.LoadWith(s => s.Offer.Request.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Offer.Request.User.AffiliateDetails)
                           .LoadWith(s => s.Offer.Request.VehicleType)
                           .LoadWith(s => s.Offer.User)
                           .LoadWith(s => s.Offer.User.UserDetails)
                           .LoadWith(s => s.Offer.User.UserDetails.Location)
                           //.LoadWith(s => s.Offer.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Offer.User.AffiliateDetails)

                           .LoadWith(s => s.CustomerFeedback)
                           .LoadWith(s => s.CustomerFeedback.User)
                           .LoadWith(s => s.CustomerFeedback.User.UserDetails)
                           .LoadWith(s => s.CustomerFeedback.User.UserDetails.Location)
                           //.LoadWith(s => s.CustomerFeedback.User.UserDetails.Location.Country)
                           .LoadWith(s => s.CustomerFeedback.User.AffiliateDetails)

                           .LoadWith(s => s.AffiliateFeedback)
                           .LoadWith(s => s.AffiliateFeedback.User)
                           .LoadWith(s => s.AffiliateFeedback.User.UserDetails)
                           .LoadWith(s => s.AffiliateFeedback.User.UserDetails.Location)
                           //.LoadWith(s => s.AffiliateFeedback.User.UserDetails.Location.Country)
                           .LoadWith(s => s.AffiliateFeedback.User.AffiliateDetails)

                           .LoadWith(s => s.UserClosedJob)
                           .LoadWith(s => s.UserClosedJob.UserDetails)
                           .LoadWith(s => s.UserClosedJob.UserDetails.Location)
                           //.LoadWith(s => s.UserClosedJob.UserDetails.Location.Country)
                           .LoadWith(s => s.UserClosedJob.AffiliateDetails)
                   );
            }
            else
            {
                jobsDto =
                    _jobRepository.Query(
                       selector => selector
                           .LoadWith(s => s.Request)
                           .LoadWith(s => s.Request.DepartureLocation)
                           .LoadWith(s => s.Request.DestinationLocation)
                           .LoadWith(s => s.Request.User)
                           .LoadWith(s => s.Request.User.UserDetails)
                           .LoadWith(s => s.Request.User.UserDetails.Location)
                           //.LoadWith(s => s.Request.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Request.User.AffiliateDetails)
                           .LoadWith(s => s.Request.VehicleType)

                           .LoadWith(s => s.Offer)
                           .LoadWith(s => s.Offer.PaymentDetails)
                           .LoadWith(s => s.Offer.Request.DepartureLocation)
                           .LoadWith(s => s.Offer.Request.DestinationLocation)
                           .LoadWith(s => s.Offer.Request.User)
                           .LoadWith(s => s.Offer.Request.User.UserDetails)
                           .LoadWith(s => s.Offer.Request.User.UserDetails.Location)
                           //.LoadWith(s => s.Offer.Request.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Offer.Request.User.AffiliateDetails)
                           .LoadWith(s => s.Offer.Request.VehicleType)
                           .LoadWith(s => s.Offer.User)
                           .LoadWith(s => s.Offer.User.UserDetails)
                           .LoadWith(s => s.Offer.User.UserDetails.Location)
                           //.LoadWith(s => s.Offer.User.UserDetails.Location.Country)
                           .LoadWith(s => s.Offer.User.AffiliateDetails)

                           .LoadWith(s => s.CustomerFeedback)
                           .LoadWith(s => s.CustomerFeedback.User)
                           .LoadWith(s => s.CustomerFeedback.User.UserDetails)
                           .LoadWith(s => s.CustomerFeedback.User.UserDetails.Location)
                           //.LoadWith(s => s.CustomerFeedback.User.UserDetails.Location.Country)
                           .LoadWith(s => s.CustomerFeedback.User.AffiliateDetails)

                           .LoadWith(s => s.AffiliateFeedback)
                           .LoadWith(s => s.AffiliateFeedback.User)
                           .LoadWith(s => s.AffiliateFeedback.User.UserDetails)
                           .LoadWith(s => s.AffiliateFeedback.User.UserDetails.Location)
                           //.LoadWith(s => s.AffiliateFeedback.User.UserDetails.Location.Country)
                           .LoadWith(s => s.AffiliateFeedback.User.AffiliateDetails)

                           .LoadWith(s => s.UserClosedJob)
                           .LoadWith(s => s.UserClosedJob.UserDetails)
                           .LoadWith(s => s.UserClosedJob.UserDetails.Location)
                           //.LoadWith(s => s.UserClosedJob.UserDetails.Location.Country)
                           .LoadWith(s => s.UserClosedJob.AffiliateDetails)
                   );
            }

            var jobs = _mapper.Map<Job[]>(jobsDto);

            return jobs;
        }
    }
}
