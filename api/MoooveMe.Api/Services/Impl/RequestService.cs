﻿using System;
using System.Linq;
using AutoMapper;
using MoooveMe.Api.Models.BindingModel.Request;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;

namespace MoooveMe.Api.Services.Impl
{
    public class RequestService : IRequestService
    {
        private readonly IMapper _mapper;
        private readonly IDisposableRepository<RequestDto> _requestRepository;
        private readonly ILocationRepository _locationRepository;
        private readonly IUserRepository _userRepository;
        private readonly IVehicleTypeRepository _vehicleTypeRepository;
        private readonly IGeocodingService _geoService;

        public RequestService(IMapper mapper,
            IDisposableRepository<RequestDto> requestRepository,
            ILocationRepository locationRepository,
            IUserRepository userRepository,
            IVehicleTypeRepository vehicleTypeRepository,
            IGeocodingService geoService)
        {
            _mapper = mapper;
            _requestRepository = requestRepository;
            _locationRepository = locationRepository;
            _userRepository = userRepository;
            _vehicleTypeRepository = vehicleTypeRepository;
            _geoService = geoService;
        }

        public Request Get(long id, long userId)
        {
            var requestDto = _requestRepository.Get(id);
            if (requestDto.UserId != userId)
            {
                requestDto.Offers = requestDto.Offers
                    .Where(o => o.UserId == userId)
                    .ToArray();
            }
            return _mapper.Map<Request>(requestDto);
        }

        public Request[] GetAll(long userId)
        {
            var requestDtos = _requestRepository.GetAll();
            foreach (var requestDto in requestDtos.Where(r => r.UserId != userId))
            {
                requestDto.Offers = requestDto.Offers
                    .Where(o => o.UserId == userId)
                    .ToArray();
            }

            return _mapper.Map<Request[]>(requestDtos);
        }

        public Request[] GetUserRequests(long userId)
        {
            var requestsDto = _requestRepository
                .GetAllEntries()
                .Where(p => p.UserId == userId)
                .ToArray();

            return _mapper.Map<Request[]>(requestsDto);
        }

        public Request Create(CreateRequest request, long userId)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var coords = _geoService.GetCoordinates(request.DepartureLocation.FullAddress);
            var departureLocationDto = _mapper.Map<LocationDto>(request.DepartureLocation);
            departureLocationDto.Longitude = coords?.Longitude;
            departureLocationDto.Latitude = coords?.Latitude;
            departureLocationDto = _locationRepository.Add(departureLocationDto);

            coords = _geoService.GetCoordinates(request.DestinationLocation.FullAddress);
            var destinationLocationDto = _mapper.Map<LocationDto>(request.DestinationLocation);
            destinationLocationDto.Longitude = coords?.Longitude;
            destinationLocationDto.Latitude = coords?.Latitude;
            destinationLocationDto = _locationRepository.Add(destinationLocationDto);

            var userDto = _userRepository.Get(userId);
            var vehicleTypeDto = _vehicleTypeRepository.Get(request.VehicleTypeId);

            //todo: add checking existence of user and vehicleType

            var requestDto = _mapper.Map<RequestDto>(request);
            requestDto.Status = RequestStatus.Open;
            requestDto.CreatedAt = DateTimeOffset.UtcNow;
            requestDto.DepartureLocationId = departureLocationDto.Id;
            requestDto.DestinationLocationId = destinationLocationDto.Id;
            requestDto.UserId = userDto.Id;
            requestDto.VehicleTypeId = vehicleTypeDto.Id;
            requestDto = _requestRepository.Add(requestDto);

            return Get(requestDto.Id, userId);
        }

        public void Dispose()
        {
            _requestRepository?.Dispose();
        }
    }
}
