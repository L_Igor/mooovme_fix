﻿using Microsoft.Extensions.Options;
using System;
using System.Linq;
using Google.Maps;
using Google.Maps.Geocoding;

namespace MoooveMe.Api.Services.Impl
{
    public class GeocodingService : IGeocodingService
    {
        private static string _geoApiKey;

        public GeocodingService(IOptions<AppSettings> settings)
        {
            _geoApiKey = settings.Value.GoogleMapsApiKey;
            GoogleSigned.AssignAllServices(new GoogleSigned(_geoApiKey));
        }

        public Coordinates GetCoordinates(string address)
        {
            var request = new GeocodingRequest {Address = address};
            var response = new Google.Maps.Geocoding.GeocodingService().GetResponse(request);

            if (response.Status != ServiceResponseStatus.Ok)
                return null;

            var result = response.Results.First();
            return new Coordinates(result.Geometry.Location.Latitude,
                result.Geometry.Location.Longitude);
        }

        public double Range(Coordinates point1, Coordinates point2)
        {
            // vincenty
            var p1 = point1.ConvertToCoordinatesInRad();
            var p2 = point2.ConvertToCoordinatesInRad();
            double a = 6378137, b = 6356752.3142, f = 1 / 298.257223563;  // WGS-84 ellipsiod
            double L = p2.Longitude - p1.Longitude;
            var U1 = Math.Atan((1 - f) * Math.Tan(p1.Latitude));
            var U2 = Math.Atan((1 - f) * Math.Tan(p2.Latitude));
            double SinU1 = Math.Sin(U1), CosU1 = Math.Cos(U1);
            double SinU2 = Math.Sin(U2), CosU2 = Math.Cos(U2);

            double lambda = L, lambdaP = 2 * Math.PI;
            var iterLimit = 20;
            double CosSqAlpha = 0, Cos2SigmaM = 0, SinSigma = 0, CosSigma = 0, sigma = 0;
            while (Math.Abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0)
            {
                double SinLambda = Math.Sin(lambda), CosLambda = Math.Cos(lambda);
                SinSigma = Math.Sqrt((CosU2 * SinLambda) * (CosU2 * SinLambda) +
                    (CosU1 * SinU2 - SinU1 * CosU2 * CosLambda) * (CosU1 * SinU2 - SinU1 * CosU2 * CosLambda));
                if (SinSigma == 0) return 0;  // co-incident points
                CosSigma = SinU1 * SinU2 + CosU1 * CosU2 * CosLambda;
                sigma = Math.Atan2(SinSigma, CosSigma);
                var SinAlpha = CosU1 * CosU2 * SinLambda / SinSigma;
                CosSqAlpha = 1 - SinAlpha * SinAlpha;
                Cos2SigmaM = CosSigma - 2 * SinU1 * SinU2 / CosSqAlpha;
                if (Cos2SigmaM == 0) Cos2SigmaM = 0;  // equatorial line: CosSqAlpha=0 (ยง6)
                var C = f / 16 * CosSqAlpha * (4 + f * (4 - 3 * CosSqAlpha));
                lambdaP = lambda;
                lambda = L + (1 - C) * f * SinAlpha *
                    (sigma + C * SinSigma * (Cos2SigmaM + C * CosSigma *
                    (-1 + 2 * Cos2SigmaM * Cos2SigmaM)));
            }
            // if (iterLimit==0) return NaN  
            if (iterLimit == 0) return double.MinValue;  // formula failed to converge

            var uSq = CosSqAlpha * (a * a - b * b) / (b * b);
            var A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
            var B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
            var deltaSigma = B * SinSigma * (Cos2SigmaM + B / 4 * (CosSigma * (-1 + 2 * Cos2SigmaM * Cos2SigmaM) -
                B / 6 * Cos2SigmaM * (-3 + 4 * SinSigma * SinSigma) * (-3 + 4 * Cos2SigmaM * Cos2SigmaM)));
            var s = b * A * (sigma - deltaSigma);

            return Math.Round(s, 3); // round to 1mm precision
        }
    }
}
