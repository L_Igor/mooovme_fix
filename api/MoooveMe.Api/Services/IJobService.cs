﻿using MoooveMe.Api.Models.BindingModel.Job;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;

namespace MoooveMe.Api.Services
{
    public interface IJobService
    {
        Job Get(long id);

        Job[] GetAll();

        Job[] GetUserJobs(long userId, UserRoles roles);

        Job Create(CreateJob job, long userId);

        void Close(long id, JobStatus status, long userId);
    }
}
