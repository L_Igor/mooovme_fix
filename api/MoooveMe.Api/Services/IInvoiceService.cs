﻿using MoooveMe.Api.Models.BindingModel.Invoice;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;

namespace MoooveMe.Api.Services
{
    public interface IInvoiceService
    {
        Invoice Get(long id);

        Invoice[] GetUserInvoices(long userId, UserRoles roles);

        Invoice Create(CreateInvoice invoice, long jobId, long userId);

        void Pay(long id, BankCard card, long userId);
    }
}
