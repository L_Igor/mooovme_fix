using System;

namespace MoooveMe.Api.Http
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ImplementationAttribute : Attribute
    {
        public string Value { get; set; }

        public Type MappingType { get; set; }

        public string PropertyName { get; set; }
    }
}