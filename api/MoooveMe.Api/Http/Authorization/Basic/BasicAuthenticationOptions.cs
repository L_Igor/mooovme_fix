﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;
using MoooveMe.Api.Repositories;

namespace MoooveMe.Api.Http.Authorization.Basic
{
    /// <summary>
    /// The Basic authentication options
    /// </summary>
    public class BasicAuthenticationOptions
        : AuthenticationOptions, IOptions<BasicAuthenticationOptions>
    {
        // private string _realm;

        /// <summary>
        /// Create an instance of the options initialized with the default values
        /// </summary>
        public BasicAuthenticationOptions()
        {
            AuthenticationScheme = "Basic";
            AutomaticAuthenticate = true;
            AutomaticChallenge = true;
        }

        BasicAuthenticationOptions IOptions<BasicAuthenticationOptions>.Value => this;

        public IUserRepository UserRepository { get; set; }
    }
}