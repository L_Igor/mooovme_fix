using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Http.Features.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using MoooveMe.Api.Helper;
using MoooveMe.Api.Repositories;

namespace MoooveMe.Api.Http.Authorization.Basic
{
    internal class BasicAuthenticationHandler : AuthenticationHandler<BasicAuthenticationOptions>
    {
        private readonly IUserRepository _usersRepo;
        private const string Scheme = "Basic";

        public BasicAuthenticationHandler(IUserRepository usersRepo)
        {
            _usersRepo = usersRepo;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            string authorizationHeader = Request.Headers["Authorization"];

            if (Request.Method.ToUpperInvariant() == "OPTIONS")
                return AuthenticateResult.Skip();

            if (string.IsNullOrEmpty(authorizationHeader))
                return AuthenticateResult.Fail("No authorization header.");

            if (!authorizationHeader.StartsWith(Scheme + ' ', StringComparison.OrdinalIgnoreCase))
                return AuthenticateResult.Skip();


            string encodedCredentials = authorizationHeader.Substring(Scheme.Length).Trim();

            if (string.IsNullOrEmpty(encodedCredentials))
            {
                LoggerExtensions.LogInformation(Logger, "No credentials");

                return AuthenticateResult.Fail("No credentials");
            }

            string decodedCredentials;
            try
            {
                decodedCredentials = Encoding.UTF8.GetString(Convert.FromBase64String(encodedCredentials));
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to decode credentials : {encodedCredentials}", ex);
            }

            var delimiterIndex = decodedCredentials.IndexOf(':');
            if (delimiterIndex == -1)
            {
                Logger.LogInformation("Invalid credentials, missing delimiter.");

                return AuthenticateResult.Fail("Invalid credentials, missing delimiter.");
            }

            var username = decodedCredentials.Substring(0, delimiterIndex);
            var password = decodedCredentials.Substring(delimiterIndex + 1);

            var ticket = GetAuthenticationTicket(username, password);
            if (ticket != null)
                return AuthenticateResult.Success(ticket);

            Logger.LogInformation($"Credential validation failed for {username}");

            return AuthenticateResult.Fail("Invalid credentials.");
        }

        private AuthenticationTicket GetAuthenticationTicket(string username, string password)
        {
            var user = _usersRepo.GetCredentials(username);

            if (user == null || user.Password != AuthHelper.GetPassword(password, user.Salt)) return null;


            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, "user"),
            };
            if (AuthHelper.Admins.Contains(user.Email))
                claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, "admin"));

            var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, "Basic", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType));

            return new AuthenticationTicket(principal, new AuthenticationProperties(), Options.AuthenticationScheme);
        }

        protected override Task<bool> HandleUnauthorizedAsync(ChallengeContext context)
        {
            Response.StatusCode = 401;
            HeaderDictionaryExtensions.Append(Response.Headers, HeaderNames.WWWAuthenticate, Scheme);

            return Task.FromResult(true);
        }

        protected override Task<bool> HandleForbiddenAsync(ChallengeContext context)
        {
            Response.StatusCode = 403;
            return Task.FromResult(true);
        }

        protected override Task HandleSignOutAsync(SignOutContext context)
        {
            throw new NotSupportedException();
        }

        protected override Task HandleSignInAsync(SignInContext context)
        {
            throw new NotSupportedException();
        }
    }
}