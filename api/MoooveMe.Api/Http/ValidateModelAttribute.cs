﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace MoooveMe.Api.Http
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.ModelState.IsValid) return;
            var error = filterContext.ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();

            if (error == null) return;
            var content = new
            {
                Error = true,
                ErrorMessage = error.ErrorMessage,
            };

            filterContext.Result = new JsonResult(content)
            {
                StatusCode = (int)HttpStatusCode.BadRequest,
            };
        }
    }


}