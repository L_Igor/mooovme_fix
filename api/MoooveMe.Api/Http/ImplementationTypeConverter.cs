using System;
using System.Linq;
using System.Reflection;
using LinqToDB.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace MoooveMe.Api.Http
{
    public class ImplementationTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {

            var attribs = objectType.GetAttributes<ImplementationAttribute>();
            if (!attribs.Any()) return false;

            var properties = objectType
                .GetProperties(BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Instance)
                .Select(a => a.Name);

            return attribs.All(a => properties.Contains(a.PropertyName));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // Load JObject from stream
            JObject jObject = JObject.Load(reader);
            // Displaying output. 
            foreach (ImplementationAttribute attr in objectType.GetAttributes<ImplementationAttribute>())
            {
                var jObjectValue = jObject.GetValue(attr.PropertyName, StringComparison.OrdinalIgnoreCase)?.Value<string>();
                if (!String.Equals(jObjectValue, attr.Value, StringComparison.OrdinalIgnoreCase))
                    continue;

                var target = Activator.CreateInstance(attr.MappingType);
                serializer.Populate(jObject.CreateReader(), target);

                return target;
            }

            var result = Activator.CreateInstance(objectType);
            serializer.Populate(jObject.CreateReader(), result);

            return result;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JObject jo = new JObject();
            Type type = value.GetType();

            DefaultContractResolver resolver = serializer.ContractResolver as DefaultContractResolver;

            foreach (PropertyInfo prop in type.GetProperties())
            {
                if (prop.CanRead)
                {
                    object propVal = prop.GetValue(value, null);
                    if (propVal != null)
                    {
                        string propName = (resolver != null)
                            ? resolver.GetResolvedPropertyName(prop.Name)
                            : prop.Name;
                        jo.Add(propName, JToken.FromObject(propVal, serializer));
                    }
                }
            }
            jo.WriteTo(writer);
        }
    }
}