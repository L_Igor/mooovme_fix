﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using LinqToDB.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using MoooveMe.Api.Data;
using MoooveMe.Api.Helper;
using MoooveMe.Api.Http.Authorization.Basic;
using MoooveMe.Api.Repositories;
using LinqToDBConfiguration = LinqToDB.Common.Configuration;

namespace MoooveMe.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                //.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            
            var dbSettings = new DatabaseSettings();
            var configurationSection = Configuration.GetSection("Database");
            configurationSection.Bind(dbSettings);
            DataConnection.DefaultSettings = dbSettings;
            LinqToDBConfiguration.Linq.AllowMultipleQuery = true;

            /*using (var db = new Db())
                db.Initialize(false);*/
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.RegisterOptions(Configuration);

            services.AddCors();

            // Add framework services.
            services
                .AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Converters.Add(new StringEnumConverter
                    {
                        CamelCaseText = true,
                    });
                    options.SerializerSettings.Formatting = Formatting.Indented;
                });

            services.AddSwaggerGen(c =>
            {
                c.DescribeAllEnumsAsStrings();
                c.SwaggerDoc("v1", new Info { Title = "MooovMe API", Version = "v1" });
                c.OperationFilter<MarkSecuredMethodsOperationFilter>();
                //Set the comments path for the swagger json and ui.
                var xmlPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "MoooveMe.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            // hack: fixed display of type byte as an integer
            services.AddSwaggerGen(c =>
            {
                c.MapType<byte>(() => new Schema
                {
                    Type = "integer",
                });
            });

            services.RegisterDependencies();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment() || env.IsStaging() || env.IsEnvironment("Local"))
            {
                app.UseDeveloperExceptionPage();
            }

            var authSettings = app.ApplicationServices.GetService<IOptions<AuthSettings>>().Value;
            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                RequireHttpsMetadata = false,
                TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = AuthSettings.GetSymmetricSecurityKey(),
                    ValidAudience = authSettings.Audience,
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    ValidIssuer = authSettings.Issuer,
                }
            });
            app.UseBasicAuthentication(new BasicAuthenticationOptions
            {
                UserRepository = app.ApplicationServices.GetService<IUserRepository>(),
            });

            app
                .UseStaticFiles()
                .UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(
                        Path.Combine(Directory.GetCurrentDirectory(), @"Content")),
                    RequestPath = new PathString("/assets")
                });

            app.UseCors(builder =>
            {
                builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            });
            app.UseMvc(routes =>
            {
                // SwaggerGen won't find controllers that are routed via this technique.
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });

            app
                .UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "MooovMe API");
                });
        }
    }
}
