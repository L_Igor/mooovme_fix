using LinqToDB.Mapping;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.Dto
{
    [Table("AffiliateDetails")]
    public class AffiliateDetailsDto : IdEntity<long>
    {
        [PrimaryKey]
        public long Id { get; set; }

        [Column, NotNull]
        public SexType Sex { get; set; }

        [Column, NotNull]
        public double MaxDistance { get; set; }

        [Column, Nullable, DataType("varchar(64)")]
        public string PhoneNumber { get; set; }

        [Column, Nullable, DataType("nvarchar(128)")]
        public string State { get; set; }

        [Column, NotNull, DataType("varchar(64)")]
        public string DriverLicense { get; set; }

        [Column, NotNull]
        public AffiliateStatus Status { get; set; }

        [Column, NotNull]
        public long SumRating { get; set; }

        [Column, NotNull]
        public int CountRating { get; set; }

        [Column, NotNull]
        public float Rating { get; set; }

        #region Associations

        [Association(ThisKey = nameof(Id), OtherKey = nameof(UserDto.Id), CanBeNull = true)]
        public UserDto User { get; set; }

        #endregion
    }
}