﻿using LinqToDB.Mapping;

namespace MoooveMe.Api.Models.Dto
{
    [Table("Locations")]
    public class LocationDto : IdEntity<long>
    {
        [PrimaryKey, Identity]
        public long Id { get; set; }

        [Column, NotNull, DataType("nvarchar(1000)")]
        public string FullAddress { get; set; }

        [Column, Nullable, DataType("nvarchar(100)")]
        public string ZipCode { get; set; }

        [Column, Nullable]
        public double? Longitude { get; set; }

        [Column, Nullable]
        public double? Latitude { get; set; }

        [Column, NotNull, DataType("char(2)")]
        public string CountryId { get; set; }

        #region Associations

        [Association(ThisKey = nameof(CountryId), OtherKey = nameof(CountryDto.Id), CanBeNull = false)]
        public CountryDto Country { get; set; }

        #endregion
    }
}