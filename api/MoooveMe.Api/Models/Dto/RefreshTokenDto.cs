﻿using System;
using LinqToDB.Mapping;

namespace MoooveMe.Api.Models.Dto
{
    [Table("RefreshTokens")]
    public class RefreshTokenDto : IdEntity<string>
    {
        [PrimaryKey, NotNull, DataType("varchar(128)")]
        public string Id { get; set; }

        [Column, NotNull]
        public DateTimeOffset Expires { get; set; }

        [Column, NotNull]
        public DateTimeOffset CreatedAt { get; set; }

        [Column, NotNull]
        public long UserId { get; set; }

        #region Associations

        [Association(ThisKey = nameof(UserId), OtherKey = nameof(UserDto.Id), CanBeNull = false)]
        public UserDto User { get; set; }

        #endregion
    }
}
