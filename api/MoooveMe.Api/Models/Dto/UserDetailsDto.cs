﻿using LinqToDB.Mapping;

namespace MoooveMe.Api.Models.Dto
{
    [Table("UserDetails")]
    public class UserDetailsDto : IdEntity<long>
    {
        [PrimaryKey]
        public long Id { get; set; }

        [Column, NotNull]
        public double MaxDistance { get; set; }

        [Column, Nullable, DataType("varchar(64)")]
        public string PhoneNumber { get; set; }

        [Column, Nullable]
        public long? LocationId { get; set; }

        [Column, NotNull]
        public long SumRating { get; set; }

        [Column, NotNull]
        public int CountRating { get; set; }

        [Column, NotNull]
        public float Rating { get; set; }

        #region Associations

        [Association(ThisKey = nameof(Id), OtherKey = nameof(UserDto.Id), CanBeNull = true)]
        public UserDto User { get; set; }

        [Association(ThisKey = nameof(LocationId), OtherKey = nameof(LocationDto.Id), CanBeNull = true)]
        public LocationDto Location { get; set; }

        #endregion
    }
}