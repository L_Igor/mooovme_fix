﻿using LinqToDB.Mapping;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.Dto
{
    [Table("Users")]
    public class UserDto : IdEntity<long>
    {
        [PrimaryKey, Identity]
        public long Id { get; set; }

        [Column, NotNull, DataType("nvarchar(256)")]
        public string Email { get; set; }

        [Column, NotNull, DataType("nvarchar(1024)")]
        public string FullName { get; set; }

        [Column, NotNull, DataType("varchar(512)")]
        public string PasswordHash { get; set; }

        [Column, NotNull, DataType("varchar(128)")]
        public string Salt { get; set; }

        [Column, NotNull]
        public bool IsEmailConfirmed { get; set; }

        [Column, NotNull]
        public UserRoles DefaultUserRole { get; set; }

        #region Associations

        [Association(ThisKey = nameof(Id), OtherKey = nameof(UserDetailsDto.Id), CanBeNull = true)]
        public UserDetailsDto UserDetails { get; set; }

        [Association(ThisKey = nameof(Id), OtherKey = nameof(AffiliateDetailsDto.Id), CanBeNull = true)]
        public AffiliateDetailsDto AffiliateDetails { get; set; }

        #endregion
    }
}