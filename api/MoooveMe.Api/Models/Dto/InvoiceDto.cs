﻿using System;
using LinqToDB.Mapping;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.Dto
{
    [Table("Invoices")]
    public class InvoiceDto : IdEntity<long>
    {
        [PrimaryKey, Identity]
        public long Id { get; set; }

        [Column, Nullable, DataType("varchar(8000)")]
        public string BankInfo { get; set; }

        [Column, NotNull]
        public InvoiceStatus Status { get; set; }

        [Column, NotNull]
        public long JobId { get; set; }

        [Column, NotNull]
        public long PaymentDetailsId { get; set; }

        #region Associations

        [Association(ThisKey = nameof(JobId), OtherKey = nameof(JobDto.Id), CanBeNull = false)]
        public JobDto Job { get; set; }

        [Association(ThisKey = nameof(PaymentDetailsId), OtherKey = nameof(PaymentDetailsDto.Id), CanBeNull = false)]
        public PaymentDetailsDto PaymentDetails { get; set; }

        #endregion
    }
}
