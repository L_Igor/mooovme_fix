﻿using LinqToDB.Mapping;

namespace MoooveMe.Api.Models.Dto
{
    [Table("VehicleTypes")]
    public class VehicleTypeDto : IdEntity<long>
    {
        public VehicleTypeDto()
        {
        }

        public VehicleTypeDto(string name, string desription)
        {
            Name = name;
            Description = desription;
        }

        [PrimaryKey, Identity]
        public long Id { get; set; }

        [Column, NotNull,  DataType("nvarchar(1024)")]
        public string Name { get; set; }

        [Column, NotNull, DataType("nvarchar(4000)")]
        public string Description { get; set; }
    }
}
