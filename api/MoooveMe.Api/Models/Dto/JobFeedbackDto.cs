﻿using System;
using LinqToDB.Mapping;

namespace MoooveMe.Api.Models.Dto
{
    [Table("JobFeedbacks")]
    public class JobFeedbackDto : IdEntity<long>
    {
        [PrimaryKey, Identity]
        public long Id { get; set; }

        [Column, NotNull]
        public byte Rating { get; set; }

        [Column, Nullable, DataType("nvarchar(1000)")]
        public string Comment { get; set; }

        [Column, NotNull]
        public DateTimeOffset CreatedAt { get; set; }

        [Column, NotNull]
        public long UserId { get; set; }

        #region Associations

        [Association(ThisKey = nameof(UserId), OtherKey = nameof(UserDto.Id), CanBeNull = false)]
        public UserDto User { get; set; }

        #endregion
    }
}
