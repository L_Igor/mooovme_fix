﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB.Mapping;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.Dto
{
    [Table("Requests")]
    public class RequestDto : IdEntity<long>
    {
        [PrimaryKey, Identity]
        public long Id { get; set; }

        [Column, Nullable, DataType("nvarchar(256)")]
        public string ContactEmail { get; set; }

        [Column, Nullable, DataType("varchar(64)")]
        public string ContactNumber { get; set; }

        [Column, NotNull, DataType("nvarchar(4000)")]
        public string Description { get; set; }

        [Column, Nullable]
        public long? MoversCount { get; set; }

        [Column, NotNull]
        public RequestStatus Status { get; set; }

        [Column, NotNull]
        public DateTimeOffset CreatedAt { get; set; }

        [Column, NotNull]
        public long DepartureLocationId { get; set; }

        [Column, NotNull]
        public long DestinationLocationId { get; set; }

        [Column, NotNull]
        public long UserId { get; set; }

        [Column, NotNull]
        public long VehicleTypeId { get; set; }

        #region Associations

        [Association(ThisKey = nameof(DepartureLocationId), OtherKey = nameof(LocationDto.Id), CanBeNull = false)]
        public LocationDto DepartureLocation { get; set; }

        [Association(ThisKey = nameof(DestinationLocationId), OtherKey = nameof(LocationDto.Id), CanBeNull = false)]
        public LocationDto DestinationLocation { get; set; }

        [Association(ThisKey = nameof(UserId), OtherKey = nameof(UserDto.Id), CanBeNull = false)]
        public UserDto User { get; set; }

        [Association(ThisKey = nameof(VehicleTypeId), OtherKey = nameof(VehicleTypeDto.Id), CanBeNull = false)]
        public VehicleTypeDto VehicleType { get; set; }

        [Association(ThisKey = nameof(Id), OtherKey = nameof(OfferDto.RequestId), CanBeNull = false)]
        public OfferDto[] Offers { get; set; }
        #endregion
    }
}
