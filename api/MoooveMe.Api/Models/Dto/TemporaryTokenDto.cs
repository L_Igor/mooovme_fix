﻿using System;
using LinqToDB.Mapping;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.Dto
{
    [Table("TemporaryTokens")]
    public class TemporaryTokenDto : IdEntity<string>
    {
        [PrimaryKey, NotNull, DataType("varchar(128)")]
        public string Id { get; set; }

        [Column, NotNull]
        public TokenType Type { get; set; }

        [Column, NotNull]
        public DateTimeOffset Expires { get; set; }

        [Column, NotNull]
        public long UserId { get; set; }

        #region Associations

        [Association(ThisKey = nameof(UserId), OtherKey = nameof(UserDto.Id), CanBeNull = false)]
        public UserDto User { get; set; }

        #endregion
    }
}