﻿using LinqToDB.Mapping;

namespace MoooveMe.Api.Models.Dto
{
    public interface IdEntity<TId>
    {
        TId Id { get; set; }
    }
}
