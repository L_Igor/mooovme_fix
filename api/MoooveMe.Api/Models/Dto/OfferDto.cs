﻿using LinqToDB.Mapping;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.Dto
{
    [Table("Offers")]
    public class OfferDto : IdEntity<long>
    {
        [PrimaryKey, Identity]
        public long Id { get; set; }

        [Column, NotNull, DataType("nvarchar(4000)")]
        public string Description { get; set; }

        [Column, NotNull]
        public OfferStatus Status { get; set; }

        [Column, NotNull]
        public long RequestId { get; set; }

        [Column, NotNull]
        public long UserId { get; set; }

        [Column, NotNull]
        public long PaymentDetailsId { get; set; }

        #region Associations

        [Association(ThisKey = nameof(RequestId), OtherKey = nameof(RequestDto.Id), CanBeNull = false)]
        public RequestDto Request { get; set; }

        [Association(ThisKey = nameof(UserId), OtherKey = nameof(UserDto.Id), CanBeNull = false)]
        public UserDto User { get; set; }

        [Association(ThisKey = nameof(PaymentDetailsId), OtherKey = nameof(PaymentDetailsDto.Id), CanBeNull = false)]
        public PaymentDetailsDto PaymentDetails { get; set; }

        #endregion
    }
}
