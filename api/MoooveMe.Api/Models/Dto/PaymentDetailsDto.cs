﻿using LinqToDB.Mapping;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.Dto
{
    [Table("PaymentDetails")]
    public class PaymentDetailsDto : IdEntity<long>
    {
        [PrimaryKey, Identity]
        public long Id { get; set; }

        [Column, NotNull]
        public PaymentType PaymentType { get; set; }

        [Column, NotNull, DataType("varchar(8000)")]
        public string PaymentDetails { get; set; }
    }
}
