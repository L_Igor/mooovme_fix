﻿using System;
using LinqToDB.Mapping;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.Dto
{
    [Table("Jobs")]
    public class JobDto : IdEntity<long>
    {
        [PrimaryKey, Identity]
        public long Id { get; set; }

        [Column, NotNull]
        public long RequestId { get; set; }

        [Column, NotNull]
        public long OfferId { get; set; }

        [Column, NotNull]
        public JobStatus Status { get; set; }

        [Column, Nullable]
        public long? CustomerFeedbackId { get; set; }

        [Column, Nullable]
        public long? AffiliateFeedbackId { get; set; }

        [Column, Nullable]
        public long? UserIdClosedJob { get; set; }

        [Column, NotNull]
        public DateTimeOffset CreatedAt { get; set; }

        [Column, Nullable]
        public DateTimeOffset? ClosedAt { get; set; }

        #region Associations

        [Association(ThisKey = nameof(RequestId), OtherKey = nameof(RequestDto.Id), CanBeNull = false)]
        public RequestDto Request { get; set; }

        [Association(ThisKey = nameof(OfferId), OtherKey = nameof(OfferDto.Id), CanBeNull = false)]
        public OfferDto Offer { get; set; }

        [Association(ThisKey = nameof(CustomerFeedbackId), OtherKey = nameof(JobFeedbackDto.Id), CanBeNull = true)]
        public JobFeedbackDto CustomerFeedback { get; set; }

        [Association(ThisKey = nameof(AffiliateFeedbackId), OtherKey = nameof(JobFeedbackDto.Id), CanBeNull = true)]
        public JobFeedbackDto AffiliateFeedback { get; set; }

        [Association(ThisKey = nameof(UserIdClosedJob), OtherKey = nameof(UserDto.Id), CanBeNull = true)]
        public UserDto UserClosedJob { get; set; }

        #endregion
    }
}
