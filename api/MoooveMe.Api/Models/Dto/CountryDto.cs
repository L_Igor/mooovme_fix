﻿using LinqToDB.Mapping;

namespace MoooveMe.Api.Models.Dto
{
    [Table("Countries")]
    public class CountryDto : IdEntity<string>
    {
        [PrimaryKey, NotNull, DataType("char(2)")]
        public string Id { get; set; }

        [Column, NotNull, DataType("nvarchar(256)")]
        public string Name { get; set; }
    }
}
