﻿using Newtonsoft.Json;

namespace MoooveMe.Api.Models.Auth
{
    public class PasswordParameters : Parameters
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
