﻿using Newtonsoft.Json;

namespace MoooveMe.Api.Models.Auth
{
    public class TokenResponse
    {
        [JsonProperty("token_type")]
        public string TokenType => "bearer";

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public long? ExpiresIn { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("user_id")]
        public long UserId { get; set; }
    }
}
