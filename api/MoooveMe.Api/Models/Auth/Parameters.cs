﻿using Newtonsoft.Json;
using MoooveMe.Api.Http;

namespace MoooveMe.Api.Models.Auth
{
    [JsonConverter(typeof(ImplementationTypeConverter))]
    [Implementation(PropertyName = "grant_type", Value = "password", MappingType = typeof(PasswordParameters))]
    [Implementation(PropertyName = "grant_type", Value = "refresh_token", MappingType = typeof(RefreshTokenParameters))]
    public class Parameters
    {
        [JsonProperty("grant_type")]
        public string GrantType { get; set; }
    }
}
