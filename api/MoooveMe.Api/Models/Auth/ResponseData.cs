﻿using Newtonsoft.Json;

namespace MoooveMe.Api.Models.Auth
{
    public class ResponseData
    {
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }
    }
}
