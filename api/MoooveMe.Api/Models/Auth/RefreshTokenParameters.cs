﻿using Newtonsoft.Json;

namespace MoooveMe.Api.Models.Auth
{
    public class RefreshTokenParameters : Parameters
    {
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }
}
