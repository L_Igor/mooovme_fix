﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel
{
    public class UpdateLocation
    {
        public string FullAddress { get; set; }

        public string ZipCode { get; set; }

        [RegularExpression("^[A-Z|a-z]{2}$", ErrorMessage = "The {0} must be 2 characters.")]
        public string CountryId { get; set; }
    }
}
