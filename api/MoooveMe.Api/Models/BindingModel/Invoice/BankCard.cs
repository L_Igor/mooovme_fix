﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.Invoice
{
    public class BankCard
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(19, MinimumLength = 13)]
        [RegularExpression("^[0-9]{13,19}$", ErrorMessage = "The {0} must consist of digits and have a length of between 13 and 19 characters.")]
        public string CardNumber { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(4)]
        [RegularExpression("^[0-9]{4}$", ErrorMessage = "The {0} must consist of 4 digits.")]
        public string ExpirationYear { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(2)]
        [RegularExpression("^[0-9]{2}$", ErrorMessage = "The {0} must consist of 2 digits.")]
        public string ExpirationMonth { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(4, MinimumLength = 3)]
        [RegularExpression("^[0-9]{3,4}$", ErrorMessage = "The {0} must consist of 3 or 4 digits.")]
        public string CardCode { get; set; }
    }
}
