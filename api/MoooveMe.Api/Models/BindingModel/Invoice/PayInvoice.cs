﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.Invoice
{
    public class PayInvoice
    {
        [Required]
        public BankCard Card { get; set; }
    }
}
