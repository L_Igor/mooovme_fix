﻿using System.ComponentModel.DataAnnotations;
using MoooveMe.Api.Models.BindingModel.PaymentDetails;

namespace MoooveMe.Api.Models.BindingModel.Invoice
{
    public class CreateInvoice
    {
        [Required(ErrorMessage = "The {0} can not be null.")]
        public CreatePaymentDetails PaymentDetails { get; set; }
    }
}
