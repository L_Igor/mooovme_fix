﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.JobFeedback
{
    public class CreateJobFeedback
    {
        [Required]
        [Range(1, 5, ErrorMessage = "The {0} must be between {1} and {2}.")]
        public byte Rating { get; set; }

        [MaxLength(1000, ErrorMessage = "The {0} can have a maximum length of {1} characters.")]
        public string Comment { get; set; }
    }
}
