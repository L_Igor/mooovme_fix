using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.PaymentDetails
{
    public class CreateFixedPaymentDetails : CreatePaymentDetails
    {
        [Required]
        public decimal Amount { get; set; }
    }
}