using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using MoooveMe.Api.Http;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.BindingModel.PaymentDetails
{
    [JsonConverter(typeof(ImplementationTypeConverter))]
    [Implementation(PropertyName = nameof(Type), Value = nameof(PaymentType.Hourly), MappingType = typeof(CreateHourlyPaymentDetails))]
    [Implementation(PropertyName = nameof(Type), Value = nameof(PaymentType.Fixed), MappingType = typeof(CreateFixedPaymentDetails))]
    public class CreatePaymentDetails
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} type cannot be empty.")]
        public PaymentType Type { get; set; }
    }
}