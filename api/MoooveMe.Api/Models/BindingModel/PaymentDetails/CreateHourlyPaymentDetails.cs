using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.PaymentDetails
{
    public class CreateHourlyPaymentDetails : CreatePaymentDetails
    {
        [Required]
        public decimal Rate { get; set; }

        [Required]
        public double ExpectedHours { get; set; }
    }
}