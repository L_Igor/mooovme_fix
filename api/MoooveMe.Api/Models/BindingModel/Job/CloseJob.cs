﻿using System.ComponentModel.DataAnnotations;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.BindingModel.Job
{
    public class CloseJob
    {
        [Required]
        public JobStatus Status { get; set; }
    }
}
