﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.Job
{
    public class CreateJob
    {
        [Required]
        public long OfferId { get; set; }
    }
}
