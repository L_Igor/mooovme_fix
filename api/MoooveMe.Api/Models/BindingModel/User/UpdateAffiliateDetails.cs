﻿using System.ComponentModel.DataAnnotations;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.BindingModel.User
{
    public class UpdateAffiliateDetails
    {
        [Required(ErrorMessage = "The {0} can not be empty.")]
        public SexType Sex { get; set; }

        [Required(ErrorMessage = "The {0} can not be empty.")]
        public double MaxDistance { get; set; }

        [Phone(ErrorMessage = "Wrong {0} format.")]
        public string PhoneNumber { get; set; }
        
        //[Required(ErrorMessage = "The {0} can not be empty.")]
        public string State { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} can not be empty.")]
        public string DriverLicense { get; set; }
    }
}
