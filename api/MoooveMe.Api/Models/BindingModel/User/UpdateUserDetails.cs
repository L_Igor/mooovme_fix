﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.User
{
    public class UpdateUserDetails
    {
        [Required(ErrorMessage = "The {0} can not be empty.")]
        public double MaxDistance { get; set; }

        [Phone(ErrorMessage = "Wrong {0} format.")]
        public string PhoneNumber { get; set; }

        public UpdateLocation Location { get; set; }
    }
}
