﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.User
{
    public class UpdateUser
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} can not be empty.")]
        public string FullName { get; set; }
    }
}
