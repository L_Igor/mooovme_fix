﻿using System.ComponentModel.DataAnnotations;
using MoooveMe.Api.Models.BindingModel.PaymentDetails;

namespace MoooveMe.Api.Models.BindingModel.Offer
{
    public class CreateOffer
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} can not be empty.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "The {0} can not be null.")]
        public CreatePaymentDetails PaymentDetails { get; set; }
    }
}