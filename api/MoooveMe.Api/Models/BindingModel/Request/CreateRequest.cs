﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.Request
{
    public class CreateRequest
    {
        [EmailAddress(ErrorMessage = "Wrong {0} format.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} cannot be empty.")]
        public string ContactEmail { get; set; }

        [Phone(ErrorMessage = "Wrong {0} format.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} cannot be empty.")]
        public string ContactNumber { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} cannot be empty.")]
        public string Description { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} cannot be empty.")]
        public UpdateLocation DepartureLocation { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} cannot be empty.")]
        public UpdateLocation DestinationLocation { get; set; }

        [Required]
        [Range(0, 100, ErrorMessage = "The {0} must be greater than or equal to {1} and less than or equal to {2}.")]
        public long MoversCount { get; set; }

        [Required]
        public long VehicleTypeId { get; set; }
    }
}