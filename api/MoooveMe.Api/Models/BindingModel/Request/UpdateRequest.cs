﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.Request
{
    public class UpdateRequest
    {
        public long Id { get; set; }

        [EmailAddress(ErrorMessage = "Wrong {0} format.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} cannot be empty.")]
        public string ContactEmail { get; set; }

        [Phone(ErrorMessage = "Wrong {0} format.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} cannot be empty.")]
        public string ContactNumber { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} cannot be empty.")]
        public string Description { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} cannot be empty.")]
        public UpdateLocation DepartureLocation { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} cannot be empty.")]
        public UpdateLocation DestinationLocation { get; set; }

        [Range(1, 100, ErrorMessage = "{0} have to be greater then {1} and lower then {0}.")]
        public long MoversCount { get; set; }

        [Required]
        public long VehicleTypeId { get; set; }
    }
}