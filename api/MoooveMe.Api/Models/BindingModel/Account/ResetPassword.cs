﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.Account
{
    public class ResetPassword
    {
        public string Code { get; set; }

        [EmailAddress(ErrorMessage = "Wrong {0} format.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} can not be empty.")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} can not be empty.")]
        [MinLength(6, ErrorMessage = "The {0} has to have be greater then {1} symbols.")]
        public string Password { get; set; }
    }
}
