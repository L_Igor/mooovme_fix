﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.Account
{
    public class ChangePassword
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} can not be empty.")]
        public string OldPassword { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} can not be empty.")]
        [MinLength(6, ErrorMessage = "The {0} has to have be greater then {1} symbols.")]
        public string NewPassword { get; set; }
    }
}
