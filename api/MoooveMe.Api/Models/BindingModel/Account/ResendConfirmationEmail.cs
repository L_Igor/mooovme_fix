﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.BindingModel.Account
{
    public class ResendConfirmationEmail
    {
        [EmailAddress(ErrorMessage = "Wrong {0} format.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} can not be empty.")]
        public string Email { get; set; }
    }
}
