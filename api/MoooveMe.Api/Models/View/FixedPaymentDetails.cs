namespace MoooveMe.Api.Models.View
{
    public class FixedPaymentDetails : PaymentDetails
    {
        public decimal Amount { get; set; }

        public override decimal TotalAmount
        {
            get => Amount;
        }
    }
}