using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.View
{
    public class PaymentDetails
    {
        public long Id { get; set; }

        public PaymentType Type { get; set; }

        public virtual decimal TotalAmount { get; }
    }
}