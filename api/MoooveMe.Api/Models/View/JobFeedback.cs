﻿using System;

namespace MoooveMe.Api.Models.View
{
    public class JobFeedback
    {
        public long Id { get; set; }

        public byte Rating { get; set; }

        public string Comment { get; set; }

        public User User { get; set; }

        public DateTimeOffset CreatedAt { get; set; }
    }
}


