﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.View
{
    public class UserDetails
    {
        public double MaxDistance { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public Location Location { get; set; }

        public float Rating { get; set; }
    }
}
