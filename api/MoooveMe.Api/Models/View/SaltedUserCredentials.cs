﻿namespace MoooveMe.Api.Models.View
{
    public class SaltedUserCredentials : UserCredentials
    {
        public long Id { get; set; }

        public string Salt { get; set; }
    }
}