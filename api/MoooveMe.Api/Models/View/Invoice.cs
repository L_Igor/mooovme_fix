﻿using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.View
{
    public class Invoice
    {
        public long Id { get; set; }

        public Job Job { get; set; }

        public PaymentDetails PaymentDetails { get; set; }

        public InvoiceStatus Status { get; set; }
    }
}
