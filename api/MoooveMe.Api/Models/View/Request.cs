﻿using System;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.View
{
    public class Request
    {
        public string ContactEmail { get; set; }

        public string ContactNumber { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public Location DepartureLocation { get; set; }

        public string Description { get; set; }

        public Location DestinationLocation { get; set; }

        public long Id { get; set; }

        public long MoversCount { get; set; }

        public long? MyOfferId { get; set; }

        public Offer[] Offers { get; set; }

        public RequestStatus Status { get; set; }

        public User User { get; set; }

        public VehicleType VehicleType { get; set; }
    }
}