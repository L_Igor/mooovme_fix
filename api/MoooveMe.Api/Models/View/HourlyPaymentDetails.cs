namespace MoooveMe.Api.Models.View
{
    public class HourlyPaymentDetails : PaymentDetails
    {
        public decimal Rate { get; set; }

        public double ExpectedHours { get; set; }

        public override decimal TotalAmount
        {
            get => Rate * (decimal)ExpectedHours;
        }
    }
}