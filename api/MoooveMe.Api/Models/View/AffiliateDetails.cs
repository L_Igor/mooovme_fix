﻿using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.View
{
    public class AffiliateDetails
    {
        public SexType Sex { get; set; }

        public double MaxDistance { get; set; }

        public string PhoneNumber { get; set; }

        public string DriverLicense { get; set; }

        public AffiliateStatus Status { get; set; }

        public string State { get; set; }

        public float Rating { get; set; }
    }
}
