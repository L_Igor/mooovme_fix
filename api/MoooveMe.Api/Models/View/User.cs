﻿using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.View
{
    public class User
    {
        public long Id { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public UserRoles DefaultUserRole { get; set; }

        public UserDetails UserDetails { get; set; }

        public AffiliateDetails AffiliateDetails { get; set; }
    }
}
