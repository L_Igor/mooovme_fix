﻿using System;
using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.View
{
    public class Job
    {
        public long Id { get; set; }

        public Request Request { get; set; }

        public Offer Offer { get; set; }

        public JobFeedback CustomerFeedback { get; set; }

        public JobFeedback AffiliateFeedback { get; set; }

        public User UserClosedJob { get; set; }

        public JobStatus Status { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset? ClosedAt { get; set; }
    }
}
