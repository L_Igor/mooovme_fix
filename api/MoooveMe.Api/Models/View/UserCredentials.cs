﻿using System.ComponentModel.DataAnnotations;

namespace MoooveMe.Api.Models.View
{
    public class UserCredentials
    {
        [EmailAddress(ErrorMessage = "Wrong email format")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email can not be empty")]
        public string Email { get; set; }
        
        [Required(AllowEmptyStrings = false, ErrorMessage = "FullName can not be empty")]
        public string FullName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Password can not be empty")]
        [MinLength(6, ErrorMessage = "Password has to have be greater then 6 symbols.")]
        public string Password { get; set; }
    }
}