﻿namespace MoooveMe.Api.Models.View
{
    public class UserInfo
    {
        public long Id { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }
    }
}