﻿using MoooveMe.Api.Models.Enums;

namespace MoooveMe.Api.Models.View
{
    public class Offer
    {
        public long Id { get; set; }

        public string Description { get; set; }

        public PaymentDetails PaymentDetails { get; set; }

        public OfferStatus Status { get; set; }

        public Request Request { get; set; }

        public User User { get; set; }
    }
}