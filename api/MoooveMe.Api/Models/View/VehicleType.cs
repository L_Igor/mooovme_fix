﻿namespace MoooveMe.Api.Models.View
{
    public class VehicleType
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
