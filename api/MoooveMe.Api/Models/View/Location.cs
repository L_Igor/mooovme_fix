﻿namespace MoooveMe.Api.Models.View
{
    public class Location
    {
        public long Id { get; set; }

        public string FullAddress { get; set; }

        public string ZipCode { get; set; }

        public double? Longitude { get; set; }

        public double? Latitude { get; set; }

        public string CountryId { get; set; }
    }
}
