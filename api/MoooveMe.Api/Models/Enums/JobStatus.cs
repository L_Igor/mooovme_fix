﻿namespace MoooveMe.Api.Models.Enums
{
    public enum JobStatus
    {
        Open = 0,
        Completed = 1,
        Inappropriate = 2,
        Undone = 3,
    }
}