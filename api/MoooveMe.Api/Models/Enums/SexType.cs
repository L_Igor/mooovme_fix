﻿namespace MoooveMe.Api.Models.Enums
{
    public enum SexType
    {
        Male = 0,
        Female = 1,
    }
}
