﻿namespace MoooveMe.Api.Models.Enums
{
    public enum UserRoles
    {
        All = 0,
        Customer = 1,
        Affiliate = 2,
    }
}
