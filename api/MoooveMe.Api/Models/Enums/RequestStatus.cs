﻿namespace MoooveMe.Api.Models.Enums
{
    public enum RequestStatus
    {
        Open = 0,
        InProgress = 1,
        Closed = 2,
    }
}