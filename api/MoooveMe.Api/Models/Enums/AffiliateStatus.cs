﻿namespace MoooveMe.Api.Models.Enums
{
    public enum AffiliateStatus
    {
        Checking = 0,
        Rejected = 1,
        Activated = 2,
    }
}