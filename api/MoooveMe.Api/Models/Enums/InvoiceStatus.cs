﻿namespace MoooveMe.Api.Models.Enums
{
    public enum InvoiceStatus
    {
        Paid = 0,
        Unpaid = 1,
    }
}