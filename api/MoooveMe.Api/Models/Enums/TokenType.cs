﻿namespace MoooveMe.Api.Models.Enums
{
    public enum TokenType
    {
        EmailConfirm = 1,
        PasswordRestore = 2,
    }
}