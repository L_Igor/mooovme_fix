﻿namespace MoooveMe.Api.Models.Enums
{
    public enum OfferStatus
    {
        NotDecided = 0,
        Declined = 1,
        Accepted = 2,
    }
}