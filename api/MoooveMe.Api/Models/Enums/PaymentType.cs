﻿namespace MoooveMe.Api.Models.Enums
{
    public enum PaymentType
    {
        Fixed = 0,
        Hourly = 1,
    }
}
