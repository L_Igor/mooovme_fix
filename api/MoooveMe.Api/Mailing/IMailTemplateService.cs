﻿using MoooveMe.Api.Mailing.Models;
using MoooveMe.Api.Mailing.Models.EmailTemplates;

namespace MoooveMe.Api.Mailing
{
    public interface IMailTemplateService
    {
        EmailContent CreateCongirmationEmail(ConfirmationEmail model);

        EmailContent CreateRestorePasswordEmail(RestorePasswordEmail model);

        EmailContent CreateResetPasswordEmail(ResetPasswordEmail model);
    }
}
