﻿using System;

namespace MoooveMe.Api.Mailing
{
    public class MailingSettings
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string Server { get; set; }

        public int Port { get; set; }

        public bool UseSsl { get; set; }
        
        public TimeSpan ConfirmationLifetime { get; set; }
    }
}
