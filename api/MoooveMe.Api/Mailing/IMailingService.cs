﻿using System.Threading.Tasks;
using MoooveMe.Api.Mailing.Models;

namespace MoooveMe.Api.Mailing
{
    public interface IMailingService
    {
        void Send(RecipientInfo recipient, string subject, string body);

        Task SendAsync(RecipientInfo recipient, string subject, string body);
    }
}
