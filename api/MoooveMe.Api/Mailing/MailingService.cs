﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using MoooveMe.Api.Mailing.Models;
using Microsoft.AspNetCore.Hosting;

namespace MoooveMe.Api.Mailing
{
    public class MailingService : IMailingService
    {
        private readonly MailboxAddress _from;
        private readonly MailingSettings _settings;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly IHostingEnvironment _env;

        public MailingService(IOptions<MailingSettings> settings, IOptions<AppSettings> appSettings,
            IHostingEnvironment env)
        {
            _settings = settings.Value;
            _appSettings = appSettings;
            _env = env;
            _from = new MailboxAddress("MooovMe", _settings.Username);
        }

        public void Send(RecipientInfo recipient, string subject, string body)
        {
            if (IsNotSendEmail())
                return;

            var message = GetMessage(recipient, subject, body);

            using (var client = new SmtpClient())
            {
                //todo: For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect(_settings.Server, _settings.Port, _settings.UseSsl);

                // Note: since we don't have an OAuth2 token, disable the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate(_settings.Username, _settings.Password);

                client.Send(message);

                client.Disconnect(true);
            }
        }

        public async Task SendAsync(RecipientInfo recipient, string subject, string body)
        {
            if (IsNotSendEmail())
                return;

            var message = GetMessage(recipient, subject, body);

            using (var client = new SmtpClient())
            {
                //todo: For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                await client.ConnectAsync(_settings.Server, _settings.Port, _settings.UseSsl);

                // Note: since we don't have an OAuth2 token, disable the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                await client.AuthenticateAsync(_settings.Username, _settings.Password);

                await client.SendAsync(message);

                await client.DisconnectAsync(true);
            }
        }

        private MimeMessage GetMessage(RecipientInfo recipient, string subject, string body)
        {
            var message = new MimeMessage();
            message.From.Add(_from);
            message.To.Add(new MailboxAddress(recipient.FullName, recipient.Email));
            message.Subject = subject;
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = body,
            };

            return message;
        }

        private bool IsNotSendEmail()
        {
            return _env.IsDevelopment() || _env.IsEnvironment("Local");
        }
    }
}