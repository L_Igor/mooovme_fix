﻿namespace MoooveMe.Api.Mailing.Models
{
    public class RecipientInfo
    {
        public string Email { get; set; }

        public string FullName { get; set; }
    }
}
