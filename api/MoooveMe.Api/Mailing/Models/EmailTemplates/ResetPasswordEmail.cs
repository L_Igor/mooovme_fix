﻿namespace MoooveMe.Api.Mailing.Models.EmailTemplates
{
    public class ResetPasswordEmail
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}
