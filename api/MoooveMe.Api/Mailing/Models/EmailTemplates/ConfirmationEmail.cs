﻿namespace MoooveMe.Api.Mailing.Models.EmailTemplates
{
    public class ConfirmationEmail
    {
        public string Token { get; set; }

        public string Name { get; set; }
    }
}
