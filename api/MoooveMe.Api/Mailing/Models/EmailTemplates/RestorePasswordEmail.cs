﻿namespace MoooveMe.Api.Mailing.Models.EmailTemplates
{
    public class RestorePasswordEmail
    {
        public string Password { get; set; }

        public string Name { get; set; }
    }
}
