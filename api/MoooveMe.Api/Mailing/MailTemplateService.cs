﻿using System;
using System.IO;
using Microsoft.Extensions.Options;
using MoooveMe.Api.Mailing.Models;
using MoooveMe.Api.Mailing.Models.EmailTemplates;

namespace MoooveMe.Api.Mailing
{
    /// <summary>
    /// TODO: Create template render service, host on Azure and use to get HTML
    /// </summary>
    public class MailTemplateService : IMailTemplateService
    {
        private readonly AppSettings _settings;

        public MailTemplateService(IOptions<AppSettings> settings)
        {
            _settings = settings.Value;
        }

        public EmailContent CreateCongirmationEmail(ConfirmationEmail model)
        {
            var url = UrlCombine(_settings.ApiUrl, $"/account/confirm-email?token={model.Token}");
            var body = File.ReadAllText(Path.Combine("Emails", "ConfirmationEmail.html"));
            body = body
                .Replace("{{UserName}}", model.Name)
                .Replace("{{ConfirmationUrl}}", url); 

            return new EmailContent
            {
                Subject = "Confirmation Email",
                Body = body,
            };
        }

        public EmailContent CreateRestorePasswordEmail(RestorePasswordEmail model)
        {
            var body =
                $"Hello, {model.Name}!<br/>" +
                $"You have been reset the password. To login use the new password.<br/>" +
                $"New password: {model.Password}";

            return new EmailContent
            {
                Subject = "Restore Password",
                Body = body,
            };
        }

        public EmailContent CreateResetPasswordEmail(ResetPasswordEmail model)
        {
            throw new NotImplementedException();
        }

        private static string UrlCombine(string url1, string url2)
        {
            if (url1 == null || url2 == null)
                throw new ArgumentNullException(url1 == null ? "url1" : "url2");

            if (url2.Length == 0)
                return url1;

            if (url1.Length == 0)
                return url2;

            url1 = url1.Trim().TrimEnd('/', '\\');
            url2 = url2.Trim().TrimStart('/', '\\');

            return $"{url1}/{url2}";
        }
    }
}
