using System;
using Microsoft.AspNetCore.Mvc;
using MoooveMe.Api.Http;
using MoooveMe.Api.Models.BindingModel.Invoice;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Services;

namespace MoooveMe.Api.Controllers
{
    [Route("api/[controller]")]
    public class InvoicesController : BaseController
    {
        private readonly IInvoiceService _invoiceService;

        public InvoicesController(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        /// <summary>
        /// Gets invoices of the specified user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="includeRoles">The user role.</param>
        /// <returns>The invoices of the specified user.</returns>
        // GET api/users/{id}/invoices
        [HttpGet("/api/users/{userId:long}/invoices")]
        [ProducesResponseType(typeof(Invoice[]), 200)]
        [ProducesResponseType(403)]
        public IActionResult GetUserRequests(long userId, UserRoles includeRoles = UserRoles.All)
        {
            if (!HasAccess(userId))
                return Forbid();

            var userJobs = _invoiceService.GetUserInvoices(userId, includeRoles);

            return Json(userJobs);
        }

        /// <summary>
        /// Gets the specified invoice.
        /// </summary>
        /// <param name="id">The invoice id.</param>
        /// <returns>The specified invoice.</returns>
        [HttpGet("{id:long}")]
        [ProducesResponseType(typeof(Invoice), 200)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public IActionResult Get(long id)
        {
            var invoice = _invoiceService.Get(id);
            if (invoice == null)
                return NotFound($"The invoice '{id}' is not found.");

            if (!HasAccess(invoice?.Job?.Request?.User?.Id) && !HasAccess(invoice?.Job?.Offer?.User?.Id))
                return Forbid();

            return Json(invoice);
        }

        /// <summary>
        /// Creates a new invoice.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <param name="invoice">The <see cref="CreateInvoice"/> model.</param>
        /// <returns>Created invoice.</returns>
        [HttpPost("/api/jobs/{jobId:long}/invoices")]
        [ProducesResponseType(typeof(Invoice), 200)]
        [ProducesResponseType(400)]
        public IActionResult Post(long jobId, [FromBody] CreateInvoice invoice)
        {
            try
            {
                var createdInvoice = _invoiceService.Create(invoice, jobId, CurrentUser.Id);

                return Ok(createdInvoice);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Performs payment of the specified invoice.
        /// </summary>
        /// <param name="invoiceId">The invoice id.</param>
        /// <param name="model">The <see cref="PayInvoice"/> model.</param>
        /// <returns></returns>
        [HttpPost("{invoiceId}/pay")]
        [ValidateModel]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult PostPay(long invoiceId, [FromBody] PayInvoice model)
        {
            try
            {
                _invoiceService.Pay(invoiceId, model.Card, CurrentUser.Id);

                return Ok();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}