﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using AutoMapper;
using MoooveMe.Api.Helper;
using MoooveMe.Api.Http;
using MoooveMe.Api.Mailing;
using MoooveMe.Api.Mailing.Models;
using MoooveMe.Api.Mailing.Models.EmailTemplates;
using MoooveMe.Api.Models.Auth;
using MoooveMe.Api.Models.BindingModel.User;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;
using MoooveMe.Api.Services;

namespace MoooveMe.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class UsersController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly ITemporaryTokenRepository _tokensRepo;
        private readonly IUserService _userService;
        private readonly IMailingService _mailingService;
        private readonly IMailTemplateService _mailTemplateService;
        private readonly MailingSettings _mailingSettings;

        public UsersController(IMapper mapper, ITemporaryTokenRepository tokensRepo,
            IUserService userService, IMailingService mailingService, IMailTemplateService mailTemplateService,
            IOptions<MailingSettings> mailingSettings)
        {
            _mapper = mapper;
            _tokensRepo = tokensRepo;
            _userService = userService;
            _mailingService = mailingService;
            _mailTemplateService = mailTemplateService;
            _mailingSettings = mailingSettings.Value;
        }

        /// <summary>
        /// Return all users.
        /// </summary>
        /// <returns>All users.</returns>
        // GET api/users
        [Authorize(Roles = "admin")]
        [HttpGet]
        [ProducesResponseType(typeof(UserInfo[]), 200)]
        public IActionResult Get()
        {
            return Json(_userService.GetAll());
        }

        /// <summary>
        /// Gets specified user.
        /// </summary>
        /// <param name="id">The user id.</param>
        /// <returns>User</returns>
        // GET api/users/{id}
        [HttpGet("{id:long}")]
        [ProducesResponseType(typeof(User), 200)]
        [ProducesResponseType(404)]
        public IActionResult Get(long id)
        {
            var user = _userService.GetById(id);
            if (user == null)
                return NotFound();

            return Json(user);
        }

        /// <summary>
        /// Creates a new user.
        /// </summary>
        /// <param name="user">The user credentials.</param>
        /// <returns>Ok if user created</returns>
        // POST api/users
        [AllowAnonymous]
        [HttpPost, ValidateModel]
        [ProducesResponseType(typeof(TokenResponse), 200)]
        [ProducesResponseType(typeof(ResponseData), 400)]
        [ProducesResponseType(409)]
        public IActionResult Post([FromBody] RegisterUser user)
        {
            try
            {
                var existingUser = _userService.GetByEmail(user.Email);
                if (existingUser != null)
                    return StatusCode(409, "The specified user exists.");

                var addedUser = _userService.Create(user);
                var token = new TemporaryTokenDto
                {
                    Id = SecurityHelper.GetRandomString(32),
                    Type = TokenType.EmailConfirm,
                    Expires = DateTimeOffset.UtcNow.Add(_mailingSettings.ConfirmationLifetime),
                    UserId = addedUser.Id,
                };
                _tokensRepo.Add(token);

                var emailContent = _mailTemplateService.CreateCongirmationEmail(new ConfirmationEmail
                {
                    Token = token.Id,
                    Name = addedUser.FullName,
                });

                _mailingService.SendAsync(_mapper.Map<RecipientInfo>(addedUser), emailContent.Subject, emailContent.Body);

                var refreshTokenRepo = HttpContext.RequestServices.GetService(typeof(IRefreshTokenRepository)) as IRefreshTokenRepository;
                var authSettings = HttpContext.RequestServices.GetService(typeof(IOptions<AuthSettings>)) as IOptions<AuthSettings>;
                var tokenController = new TokenController(refreshTokenRepo, _userService, authSettings);

                return tokenController.PostToken(new PasswordParameters
                {
                    GrantType = GrantType.Password,
                    Username = user.Email,
                    Password = user.Password,
                });
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Updates information about the user.
        /// </summary>
        /// <param name="id">The user id.</param>
        /// <param name="user">The user model.</param>
        /// <returns>Ok, if user was updated successfully</returns>
        // PUT api/users/{id}
        [HttpPut("{id:long}")]
        [ValidateModel]
        [ProducesResponseType(200)]
        [ProducesResponseType(403)]
        public IActionResult PutUser(long id, [FromBody] UpdateUser user)
        {
            if (!HasAccess(id))
                return Forbid();

            _userService.UpdateUser(id, user);

            return Ok();
        }

        /// <summary>
        /// Updates additional information about the user.
        /// </summary>
        /// <param name="id">The user id.</param>
        /// <param name="user">The user model.</param>
        /// <returns>Ok, if user was updated successfully</returns>
        // PUT api/users/{id}/details
        [HttpPut("{id:long}/details")]
        [ValidateModel]
        [ProducesResponseType(200)]
        [ProducesResponseType(403)]
        public IActionResult PutUserDetails(long id, [FromBody] UpdateUserDetails user)
        {
            if (!HasAccess(id))
                return Forbid();

            _userService.UpdateUserDetails(id, user);

            return Ok();
        }

        /// <summary>
        /// Updates information about the affiliate.
        /// </summary>
        /// <param name="id">The user id.</param>
        /// <param name="user">The user model.</param>
        /// <returns>Ok, if user was updated successfully</returns>
        // PUT api/users/{id}/affiliate
        [HttpPut("{id:long}/affiliate")]
        [ValidateModel]
        [ProducesResponseType(200)]
        [ProducesResponseType(403)]
        public IActionResult PutAffiliateDetails(long id, [FromBody] UpdateAffiliateDetails user)
        {
            if (!HasAccess(id))
                return Forbid();

            _userService.UpdateAffiliateDetails(id, user);

            return Ok();
        }

        /// <summary>
        /// Gets user rating.
        /// </summary>
        /// <param name="id">The user id.</param>
        /// <param name="role">The user role.</param>
        /// <returns>Returns user rating.</returns>
        [HttpGet("{id:long}/rating/{role}")]
        [ProducesResponseType(typeof(float), 200)]
        public IActionResult GetRating(long id, UserRoles role)
        {
            try
            {
                var rating = _userService.GetUserRating(id, role);

                return Ok(rating);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}