﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using MoooveMe.Api.Http;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;
using MoooveMe.Api.Services;
using MoooveMe.Api.Models.BindingModel.Request;
using MoooveMe.Api.Models.BindingModel.Offer;

namespace MoooveMe.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class RequestsController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IOfferRepository _offersRepo;
        private readonly IRequestService _requestService;
        private readonly IPaymentDetailsRepository _paymentDetailsRepo;
        private readonly IUserService _userService;
        private readonly IOfferService _offerService;

        public RequestsController(IMapper mapper,
            IOfferRepository offersRepo,
            IRequestService requestService,
            IPaymentDetailsRepository paymentDetailsRepo,
            IUserService userService,
            IOfferService offerService)
            : base(requestService)
        {
            _mapper = mapper;
            _offersRepo = offersRepo;
            _paymentDetailsRepo = paymentDetailsRepo;
            _requestService = requestService;
            _userService = userService;
            _offerService = offerService;
        }

        /// <summary>
        /// Get all requests of the specified user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>All requests of the specified user.</returns>
        // GET api/users/{id}/requests
        [HttpGet("/api/users/{userId:long}/requests")]
        [ProducesResponseType(typeof(Request[]), 200)]
        [ProducesResponseType(404)]
        public IActionResult GetUserRequests(long userId)
        {
            if (!HasAccess(userId))
                return Forbid();

            return Json(_requestService.GetUserRequests(userId));
        }

        /// <summary>
        /// Get all nearest requests.
        /// </summary>
        /// <returns>All requests satisfies user settings.</returns>
        // GET api/requests
        [HttpGet]
        [ProducesResponseType(typeof(Request[]), 200)]
        public IActionResult Get()
        {
            var requests = _requestService.GetAll(CurrentUser.Id);
            return Json(requests);
        }

        /// <summary>
        /// Gets the specified request.
        /// </summary>
        /// <param name="id">The request id.</param>
        /// <returns>All users requests.</returns>
        // GET api/requests/{id}
        [HttpGet("{id:long}")]
        [ProducesResponseType(typeof(Request), 200)]
        [ProducesResponseType(404)]
        public IActionResult Get(long id)
        {
            var request = _requestService.Get(id, CurrentUser.Id);
            if (request == null)
                return NotFound();

            return Json(request);
        }

        /// <summary>
        /// Gets all offers of the specified request.
        /// </summary>
        /// <param name="requestId">The request id.</param>
        /// <returns>The request.</returns>
        // GET api/requests/{id}/offers
        [HttpGet("{requestId:long}/offers")]
        [ProducesResponseType(typeof(Offer[]), 200)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public IActionResult GetOffers(long requestId)
        {
            //todo: may be move in OfferService

            var request = _requestService.Get(requestId, CurrentUser.Id);
            if (request == null)
                return NotFound();

            if (!HasAccess(request.User.Id))
                return Forbid();

            var offers = _offerService.GetRequestOffers(requestId);

            return Json(offers);
        }

        /// <summary>
        /// Creates a new request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Ok on success.</returns>
        // POST api/requests
        [HttpPost, ValidateModel]
        [ProducesResponseType(typeof(Request), 200)]
        [ProducesResponseType(404)]
        public IActionResult Post([FromBody] CreateRequest request)
        {
            var user = CurrentUser;
            if (user == null)
                return NotFound();

            var createdRequest = _requestService.Create(request, user.Id);

            return Json(_mapper.Map<Request>(createdRequest));
        }

        /// <summary>
        /// Creates a offer to the specified request. NI
        /// </summary>
        /// <param name="id">The request id.</param>
        /// <param name="offer">The offer.</param>
        /// <returns>Ok on success.</returns>
        // POST api/requests/{id}/offers
        [HttpPost("{id:long}/offers"), ValidateModel]
        [ProducesResponseType(typeof(Offer), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(405)]
        public IActionResult PostOffer(long id, [FromBody] CreateOffer offer)
        {
            //todo: may be move in OfferService

            var request = _requestService.Get(id, CurrentUser.Id);
            if (request == null)
                return NotFound();

            var userId = CurrentUser.Id;

            var affiliateDetails = _userService.GetAffiliateDetails(userId);
            if (affiliateDetails == null || affiliateDetails.Status != AffiliateStatus.Activated)
                return StatusCode(405, "Your affiliate license is not set or not confirmed.");

            if (_offersRepo.IsExistOffer(request.Id, userId))
                return StatusCode(409, "You already sent an offer.");

            var paymentDetailsDto = _paymentDetailsRepo.Add(_mapper.Map<PaymentDetailsDto>(offer.PaymentDetails));

            var offerDto = _mapper.Map<OfferDto>(offer);
            offerDto.RequestId = request.Id;
            offerDto.UserId = CurrentUser.Id;
            offerDto.PaymentDetailsId = paymentDetailsDto.Id;

            var createdOfferDto = _offersRepo.Add(offerDto);
            var createdOffer = _offerService.Get(createdOfferDto.Id);

            return Ok(createdOffer);
        }
    }
}
