﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;

namespace MoooveMe.Api.Controllers
{
    [Route("api/[controller]")]
    public class VehicleTypesController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IVehicleTypeRepository _vehicleTypesRepo;

        public VehicleTypesController(IMapper mapper, IVehicleTypeRepository vehicleTypesRepo)
        {
            _mapper = mapper;
            _vehicleTypesRepo = vehicleTypesRepo;
        }

        /// <summary>
        /// Gets all vehicle types.
        /// </summary>
        /// <returns>All vehicle types.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(VehicleType[]), 200)]
        public IActionResult Get()
        {
            var vehicleTypes = _vehicleTypesRepo.GetAll();

            return Json(_mapper.Map<VehicleType[]>(vehicleTypes));
        }

        /// <summary>
        /// Gets the specified vehicle type.
        /// </summary>
        /// <param name="id">The vehicle type id.</param>
        /// <returns>The specified vehicle type.</returns>
        [HttpGet("{id:long}")]
        [ProducesResponseType(typeof(VehicleType), 200)]
        [ProducesResponseType(404)]
        public IActionResult Get(long id)
        {
            var result = _vehicleTypesRepo.Get(id);
            if (result == null)
                return NotFound();

            return Json(_mapper.Map<VehicleType>(result));
        }

        /// <summary>
        /// Creates a new vehicle type.
        /// </summary>
        /// <param name="type">The vehicle type model.</param>
        /// <returns>OK on success.</returns>
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Post([FromBody] VehicleTypeDto type)
        {
            _vehicleTypesRepo.Add(type);
            return Ok();
        }

        /// <summary>
        /// Updates the specified vehicle type.
        /// </summary>
        /// <param name="id">The vehicle type id.</param>
        /// <param name="type">The vehicle type model.</param>
        /// <returns>OK on success.</returns>
        [Authorize(Roles = "admin")]
        [HttpPut("{id:long}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult Put(long id, [FromBody] VehicleTypeDto type)
        {
            var result = _vehicleTypesRepo.Get(id);
            if (result == null)
                return NotFound();

            _vehicleTypesRepo.Update(id, type);

            return Ok();
        }

        /// <summary>
        /// Deletes the specified vehicle type.
        /// </summary>
        /// <param name="id">The vehilce id.</param>
        /// <returns>OK on success.</returns>
        [Authorize(Roles = "admin")]
        [HttpDelete("{id:long}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult Delete(long id)
        {
            var result = _vehicleTypesRepo.Get(id);
            if (result == null)
                return NotFound();

            _vehicleTypesRepo.Delete(id);

            return Ok();
        }
    }
}