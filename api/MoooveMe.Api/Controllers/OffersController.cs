﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using AutoMapper;
using MoooveMe.Api.Models.BindingModel.Job;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;
using MoooveMe.Api.Services;

namespace MoooveMe.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class OffersController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IOfferRepository _offersRepo;
        private readonly IRequestRepository _requestsRepo;
        private readonly IOfferService _offerService;
        private readonly IJobService _jobService;

        public OffersController(IMapper mapper,
            IOfferRepository offersRepo,
            IRequestRepository requestsRepo,
            IOfferService offerService,
            IJobService jobService)
        {
            _mapper = mapper;
            _offersRepo = offersRepo;
            _requestsRepo = requestsRepo;
            _offerService = offerService;
            _jobService = jobService;
        }

        /// <summary>
        /// Get the all offers of the current user.
        /// </summary>
        /// <returns>All offers of the current user.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(Offer[]), 200)]
        public IActionResult Get()
        {
            var offers = _offerService.GetUserOffers(CurrentUser.Id);

            return Json(offers);
        }

        /// <summary>
        /// Get the specified offer.
        /// </summary>
        /// <param name="id">The offer id.</param>
        /// <returns>Ok on success.</returns>
        [HttpGet("{id:long}")]
        [ProducesResponseType(typeof(Offer), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult Get(long id)
        {
            var offer = _offersRepo.Get(id);
            if (offer == null)
                return NotFound();

            var request = _requestsRepo.Get(offer.RequestId);
            var currentUser = CurrentUser;
            if (offer.UserId != currentUser.Id && request.UserId != currentUser.Id)
                return StatusCode(401, "You haven't rights to access this offer.");

            return Json(_offerService.Get(id));
        }

        /// <summary>
        /// Rejects the specified offer.
        /// </summary>
        /// <param name="offerId">The offer id.</param>
        /// <returns>Ok on success.</returns>
        [HttpPut("{offerId:long}/decline")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult PutDeclineOffer(long offerId)
        {
            var offer = _offersRepo.Get(offerId);
            if (offer == null)
                return NotFound();

            if (offer.Status != OfferStatus.NotDecided)
                return BadRequest($"The offer '{offer.Id}' already has a status.");

            var request = _requestsRepo.Get(offer.RequestId);
            var currentUser = CurrentUser;
            if (request.UserId != currentUser.Id)
                return StatusCode(401, "You haven't rights to change this offer.");

            _offersRepo.SetStatus(offerId, OfferStatus.Declined);

            return Ok();
        }

        /// <summary>
        /// Accepts the specified offer.
        /// </summary>
        /// <param name="offerId">The offer id.</param>
        /// <returns>Created job</returns>
        [HttpPut("{offerId:long}/accept")]
        [ProducesResponseType(typeof(Job[]), 200)]
        [ProducesResponseType(400)]
        public IActionResult PutAcceptOffer(long offerId)
        {
            var job = new CreateJob
            {
                OfferId = offerId,
            };

            try
            {
                var createdJob = _jobService.Create(job, CurrentUser.Id);

                return Ok(createdJob);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
