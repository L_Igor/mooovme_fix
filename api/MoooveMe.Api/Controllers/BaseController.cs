﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using MoooveMe.Api.Helper;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Services;

namespace MoooveMe.Api.Controllers
{
    public abstract class BaseController : Controller
    {
        private IUserService _usersService;
        private IDisposable[] _parameters;

        protected BaseController(params IDisposable[] parameters)
        {
            _parameters = parameters;
        }

        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            _usersService = context.HttpContext.RequestServices.GetService<IUserService>();
        }

        protected bool HasAccess(long? userId)
        {
            var user = CurrentUser;
            return AuthHelper.Admins.Contains(user.Email) || userId == user.Id;
        }

        protected User CurrentUser
        {
            get { return _usersService.GetByEmail(User?.Identity?.Name); }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            foreach (var p in _parameters)
            {
                p.Dispose();
            }
        }
    }
}
