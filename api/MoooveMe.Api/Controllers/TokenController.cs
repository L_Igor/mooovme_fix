using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using MoooveMe.Api.Helper;
using MoooveMe.Api.Models.Auth;
using MoooveMe.Api.Models.Dto;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;
using MoooveMe.Api.Services;

namespace MoooveMe.Api.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private readonly IRefreshTokenRepository _tokensRepo;
        private readonly IUserService _userService;
        private readonly AuthSettings _authSettings;

        public TokenController(IRefreshTokenRepository tokensRepo,
            IUserService userService,
            IOptions<AuthSettings> authSettings)
        {
            _tokensRepo = tokensRepo;
            _userService = userService;
            _authSettings = authSettings.Value;
        }

        /// <summary>
        /// Creates and updates an Access Token
        /// </summary>
        /// <param name="model">The <see cref="Parameters"/> model.</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(TokenResponse), 200)]
        [ProducesResponseType(typeof(ResponseData), 400)]
        [ProducesResponseType(typeof(ResponseData), 401)]
        public IActionResult PostToken([FromBody] Parameters model)
        {
            if (model.GrantType == GrantType.Password)
            {
                return PostCreateToken(model as PasswordParameters);
            }
            else if (model.GrantType == GrantType.RefreshToken)
            {
                return PostRefreshToken(model as RefreshTokenParameters);
            }

            return BadRequest(new ResponseData
            {
                Error = ErrorResponse.UnsupportedGrantType,
                ErrorDescription = $"Grant type '{model.GrantType}' is not supported.",
            });
        }

        /// <summary>
        /// Creates an Access Token
        /// </summary>
        /// <param name="model">The <see cref="PasswordParameters"/> model.</param>
        /// <returns></returns>
        private IActionResult PostCreateToken([FromBody] PasswordParameters model)
        {
            if (model.GrantType != GrantType.Password)
                throw new ArgumentException($"Expected '{GrantType.Password}' value.", nameof(model.GrantType));

            if (String.IsNullOrWhiteSpace(model.Username))
                return BadRequest(new ResponseData
                {
                    Error = ErrorResponse.InvalidRequest,
                    ErrorDescription = $"Request was missing the '{nameof(model.Username)}' parameter.",
                });

            if (String.IsNullOrWhiteSpace(model.Password))
                return BadRequest(new ResponseData
                {
                    Error = ErrorResponse.InvalidRequest,
                    ErrorDescription = $"Request was missing the '{nameof(model.Password)}' parameter.",
                });


            var credentials = _userService.GetCredentials(model.Username);

            if (credentials == null)
                return StatusCode(401, new ResponseData
                {
                    Error = ErrorResponse.InvalidClient,
                    ErrorDescription = "Invalid username.",
                });

            if (AuthHelper.GetPassword(model.Password, credentials.Salt) != credentials.Password)
                return StatusCode(401, new ResponseData
                {
                    Error = ErrorResponse.InvalidGrant,
                    ErrorDescription = "Invalid password.",
                });


            return GetTokenResponse(new UserInfo
            {
                Id = credentials.Id,
                Email = credentials.Email,
            });
        }

        /// <summary>
        /// Updates an Access Token
        /// </summary>
        /// <param name="model">The <see cref="RefreshTokenParameters"/> model.</param>
        /// <returns></returns>
        private IActionResult PostRefreshToken([FromBody] RefreshTokenParameters model)
        {
            if (model.GrantType != GrantType.RefreshToken)
                throw new ArgumentException($"Expected '{GrantType.RefreshToken}' value.", nameof(model.GrantType));

            if (String.IsNullOrWhiteSpace(model.RefreshToken))
                return BadRequest(new ResponseData
                {
                    Error = ErrorResponse.InvalidRequest,
                    ErrorDescription = $"Request was missing the '{nameof(model.RefreshToken)}' parameter.",
                });


            var token = _tokensRepo.Get(model.RefreshToken);

            if (token == null)
                return BadRequest(new ResponseData
                {
                    Error = ErrorResponse.InvalidGrant,
                    ErrorDescription = "Invalid refresh token.",
                });

            if (token.Expires < DateTimeOffset.UtcNow)
                return BadRequest(new ResponseData
                {
                    Error = ErrorResponse.InvalidGrant,
                    ErrorDescription = "Refresh token has expired.",
                });


            token.Expires = DateTimeOffset.MinValue;
            _tokensRepo.Update(token.Id, token);

            var user = _userService.GetById(token.UserId);

            return GetTokenResponse(new UserInfo
            {
                Id = user.Id,
                Email = user.Email,
            });
        }

        private IActionResult GetTokenResponse(UserInfo user)
        {
            var identity = GetIdentity(user);

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                issuer: _authSettings.Issuer,
                audience: _authSettings.Audience,
                claims: identity.Claims,
                notBefore: now,
                expires: now.Add(_authSettings.AccessTokenLifetime),
                signingCredentials: new SigningCredentials(AuthSettings.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
            var token = new RefreshTokenDto
            {
                Id = refreshToken,
                Expires = now.Add(_authSettings.RefreshTokenLifetime),
                CreatedAt = DateTimeOffset.UtcNow,
                UserId = user.Id,
            };
            _tokensRepo.Add(token);

            return Json(new TokenResponse
            {
                AccessToken = encodedJwt,
                ExpiresIn = (long)_authSettings.AccessTokenLifetime.TotalSeconds,
                RefreshToken = refreshToken,
                UserId = user.Id,
            });
        }

        private ClaimsIdentity GetIdentity(UserInfo user)
        {
            var claims = new List<Claim>
            {
                new Claim("id", user.Id.ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, "user"),
            };
            if (AuthHelper.Admins.Contains(user.Email))
                claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, "admin"));

            return new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
        }
    }
}