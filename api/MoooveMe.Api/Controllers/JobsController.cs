using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using MoooveMe.Api.Http;
using MoooveMe.Api.Models.BindingModel.Job;
using MoooveMe.Api.Models.BindingModel.JobFeedback;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Services;

namespace MoooveMe.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class JobsController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IJobService _jobService;
        private readonly IJobFeedbackService _jobFeedbackService;

        public JobsController(IMapper mapper,
            IJobService jobService,
            IJobFeedbackService jobFeedbackService)
        {
            _mapper = mapper;
            _jobService = jobService;
            _jobFeedbackService = jobFeedbackService;
        }

        /// <summary>
        /// Gets all jobs of the specified users.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="includeRoles">The user role.</param>
        /// <returns>All jobs of the specified user.</returns>
        [HttpGet("/api/users/{userId}/jobs")]
        [ProducesResponseType(typeof(Job[]), 200)]
        [ProducesResponseType(403)]
        public IActionResult GetUserJobs(long userId, UserRoles includeRoles = UserRoles.All)
        {
            if (!HasAccess(userId))
                return Forbid();

            var userJobs = _jobService.GetUserJobs(userId, includeRoles);

            return Json(userJobs);
        }

        /// <summary>
        /// Gets the specified job.
        /// </summary>
        /// <param name="id">The job id.</param>
        /// <returns>The specified job.</returns>
        [HttpGet("{id:long}")]
        [ProducesResponseType(typeof(Job), 200)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public IActionResult Get(long id)
        {
            var job = _jobService.Get(id);
            if (job == null)
                return NotFound($"The job '{id}' is not found.");

            if (!HasAccess(job.Request.User.Id) && !HasAccess(job.Offer.User.Id))
                return Forbid();

            return Json(job);
        }

        /// <summary>
        /// Closes the specified job.
        /// </summary>
        /// <param name="id">The job id.</param>
        /// <param name="model">The <see cref="CloseJob"/> model.</param>
        /// <returns></returns>
        [HttpPost("{id:long}/close")]
        [ValidateModel]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public IActionResult PostClose(long id, [FromBody] CloseJob model)
        {
            try
            {
                _jobService.Close(id, model.Status, CurrentUser.Id);

                return NoContent();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Creates job feedback.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <param name="feedback">The <see cref="CreateJobFeedback"/> model.</param>
        /// <returns>Created job feedback.</returns>
        [HttpPost("{jobId:long}/feedbacks")]
        [ValidateModel]
        [ProducesResponseType(typeof(JobFeedback), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult PostFeedback(long jobId, [FromBody] CreateJobFeedback feedback)
        {
            try
            {
                var createdJobFeedback = _jobFeedbackService.Create(feedback, jobId, CurrentUser.Id);

                return Ok(createdJobFeedback);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}