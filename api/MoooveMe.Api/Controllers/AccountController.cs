﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using AutoMapper;
using MoooveMe.Api.Helper;
using MoooveMe.Api.Http;
using MoooveMe.Api.Mailing;
using MoooveMe.Api.Mailing.Models;
using MoooveMe.Api.Mailing.Models.EmailTemplates;
using MoooveMe.Api.Models.BindingModel.Account;
using MoooveMe.Api.Models.Enums;
using MoooveMe.Api.Models.View;
using MoooveMe.Api.Repositories;
using MoooveMe.Api.Services;

namespace MoooveMe.Api.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly ITemporaryTokenRepository _tokensRepo;
        private readonly IUserService _userService;
        private readonly IMailingService _mailingService;
        private readonly IMailTemplateService _mailTemplateService;
        private readonly MailingSettings _mailingSettings;

        public AccountController(IMapper mapper, ITemporaryTokenRepository tokensRepo,
            IUserService userService, IMailingService mailingService, IMailTemplateService mailTemplateService,
            IOptions<MailingSettings> mailingSettings)
        {
            _mapper = mapper;
            _tokensRepo = tokensRepo;
            _userService = userService;
            _mailingService = mailingService;
            _mailTemplateService = mailTemplateService;
            _mailingSettings = mailingSettings.Value;
        }

        /// <summary>
        /// Confirms the email of the user.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        // GET api/account/confirm-email
        [HttpGet("confirm-email")]
        [ProducesResponseType(200)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public IActionResult GetSendConfirmationEmail(string token)
        {
            var user = CurrentUser;
            if (user?.IsEmailConfirmed == true)
                return LocalRedirect("/assets/oops.html");

            var existingToken = _tokensRepo.Get(token, TokenType.EmailConfirm);
            if (existingToken == null || existingToken.Expires < DateTimeOffset.UtcNow)
                return LocalRedirect("/assets/try-again.html");

            existingToken.Expires = DateTimeOffset.MinValue;
            _tokensRepo.Update(existingToken.Id, existingToken);

            _userService.SetEmailConfirmed(existingToken.UserId, true);

            return LocalRedirect("/assets/index.html");
        }

        /// <summary>
        /// Resends the confirmation email.
        /// </summary>
        /// <param name="model">The <see cref="ResendConfirmationEmail"/> model.</param>
        /// <returns></returns>
        // POST api/account/resend-confirmation-email
        [HttpPost("resend-confirmation-email")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult PostResendConfirmationEmail([FromBody] ResendConfirmationEmail model)
        {
            var existingUser = _userService.GetByEmail(model.Email);
            if (existingUser == null)
                return NotFound("The specified user not exists.");

            var existingToken = _tokensRepo.GetByUserId(existingUser.Id, TokenType.EmailConfirm);
            if (existingToken == null)
            {
                var token = new Models.Dto.TemporaryTokenDto
                {
                    Id = SecurityHelper.GetRandomString(32),
                    Type = TokenType.EmailConfirm,
                    Expires = DateTimeOffset.UtcNow.Add(_mailingSettings.ConfirmationLifetime),
                    UserId = existingUser.Id,
                };
                existingToken = _tokensRepo.Add(token);
            }

            var emailContent = _mailTemplateService.CreateCongirmationEmail(new ConfirmationEmail
            {
                Token = existingToken.Id,
                Name = existingUser.FullName,
            });

            _mailingService.SendAsync(_mapper.Map<RecipientInfo>(existingUser), emailContent.Subject, emailContent.Body);

            return Ok();
        }

        /// <summary>
        /// Sends a new password to the mail.
        /// </summary>
        /// <param name="user">The <see cref="RestorePassword"/> model.</param>
        /// <returns></returns>
        // POST api/account/restore-password
        [HttpPost("restore-password")]
        [ValidateModel]
        [ProducesResponseType(200)]
        public IActionResult PostRestorePassword([FromBody] RestorePassword user)
        {
            var existingUser = _userService.GetByEmail(user.Email);
            if (existingUser == null)
                return NotFound("The specified user not exists.");

            var password = SecurityHelper.GetRandomString(8);
            _userService.SetPassword(existingUser.Id, password);

            var emailContent = _mailTemplateService.CreateRestorePasswordEmail(new RestorePasswordEmail
            {
                Password = password,
                Name = existingUser.FullName,
            });

            _mailingService.SendAsync(_mapper.Map<RecipientInfo>(existingUser), emailContent.Subject, emailContent.Body);

            return Ok();
        }

        // POST api/account/forgot-password
        //[HttpPost("forgot-password")]
        //[ValidateModel]
        //[ProducesResponseType(200)]
        //public IActionResult PostForgotPassword([FromBody] ForgotPassword user)
        //{
        //    var existingUser = _userService.GetByEmail(user.Email);
        //    if (existingUser == null)
        //        return NotFound("The specified user not exists");

        //    var token = new TemporaryTokenDto
        //    {
        //        Id = SecurityHelper.GetRandomString(4),
        //        Type = TokenType.PasswordRestore,
        //        Expires = DateTimeOffset.UtcNow.Add(_mailingSettings.ConfirmationLifetime),
        //        UserId = existingUser.Id,
        //    };
        //    _tokensRepo.Add(token);

        //    var emailContent = _mailTemplateService.CreateRestorePasswordEmail(new ResetPasswordEmail
        //    {
        //        Code = token.Id,
        //        Name = existingUser.FullName,
        //    });

        //    _mailingService.Send(_mapper.Map<RecipientInfo>(existingUser), emailContent.Subject, emailContent.Body);

        //    return Ok();
        //}

        // POST api/account/reset-password
        //[HttpPost("reset-password")]
        //[ValidateModel]
        //[ProducesResponseType(200)]
        //public IActionResult PostResetPassword([FromBody] ResetPassword user)
        //{
        //    return Ok();
        //}

        /// <summary>
        /// Updates password of the user.
        /// </summary>
        /// <param name="model">The <see cref="ChangePassword"/> model.</param>
        /// <returns></returns>
        // PUT api/account/password
        [Authorize]
        [HttpPut("password")]
        [ValidateModel]
        [ProducesResponseType(200)]
        [ProducesResponseType(403)]
        public IActionResult PutPassword([FromBody] ChangePassword model)
        {
            var currentUser = CurrentUser;
            if (currentUser == null)
                return Forbid();

            _userService.ChangePassword(currentUser.Id, model.OldPassword, model.NewPassword);

            return Ok();
        }
    }
}