﻿using Newtonsoft.Json;

namespace AuthorizeNetCore.Models
{
    public class TransactionError
    {
        [JsonProperty(PropertyName = "errorCode")]
        public string ErrorCode { get; set; }
        [JsonProperty(PropertyName = "errorText")]
        public string ErrorText { get; set; }
    }
}